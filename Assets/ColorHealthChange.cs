﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorHealthChange : MonoBehaviour {
	
	[SerializeField]
	private Image healthBarImage;
	[SerializeField]
	private Health health;

	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {

		if(health.HealthPoints < 4 ){
			healthBarImage.color = Color.red;
		}



	}
}
