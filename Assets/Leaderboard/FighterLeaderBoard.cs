﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FighterLeaderBoard : MonoBehaviour {

	public List<LBEntry> lBEntries;
	public Transform EntryList;
	public GameObject EntryPrefab;
	public InputField inputfield;

	public RectTransform AcceptUI;

	public RectTransform DenyUI;

	//int totalScore = 0;
    public string playerName;



	public float retriveScoreTime;
	float retreiveScoreTimer;
	bool retrieveScore;

	dreamloLeaderBoard.Score playerSavedScore;

	List<dreamloLeaderBoard.Score> scoreList; 
	enum gameState {
		waiting,
		running,
		enterscore,
		leaderboard
	};
	gameState gs;
	public static FighterLeaderBoard instance;
	dreamloLeaderBoard dl;

	void Awake()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}

	}
	// Use this for initialization
	void Start () {
		// get the reference here...
		this.dl = dreamloLeaderBoard.GetSceneDreamloLeaderboard();

		dl.LoadScores ();
		lBEntries = new List<LBEntry> ();
	}
	
	// Update is called once per frame
	void Update () {
		
		scoreList = dl.ToListHighToLow();


	}


	public void AddScore(){
		dl.AddScore("hovig32",Random.Range(0,100));
	}

	public void AddScore(string PN, int totalscore){
		if (CheckPlayerNameExists()) {
			Debug.Log ("Cant add Score! Name Already Exists!");
			DenyUI.gameObject.SetActive (true);
			AcceptUI.gameObject.SetActive (false);
		} else if(inputfield.text == "" && dl.playerScoreData != null){
			Debug.Log ("Cant add Score! Name Already Exists!");
		}
		else{
			
			dl.AddScore (PN, totalscore);
			Debug.Log ("Score added");
			AcceptUI.gameObject.SetActive (true);
			DenyUI.gameObject.SetActive (false);
		}
	}


	public bool CheckPlayerNameExists(){

		if (dl.playerScoreData != "") {
			return true;
		} else {
			return false;
		}
	}


	public void GetScoreByPlayer(){
		dl.GetScoreByPlayer(inputfield.text);
	}

	public void InputScore(){
		AddScore (inputfield.text, Random.Range(0,100));
	}


	public void ShowList(){
		List<dreamloLeaderBoard.Score> temp;
    	foreach (LBEntry l in lBEntries) {
			Destroy (l.gameObject);
		}
		lBEntries.Clear ();

		int PlayerIndex = 0;
		int lbRange = 2;

		int i = 0;
		foreach (dreamloLeaderBoard.Score score in scoreList) {
			//Debug.Log (score.playerName);
			//Debug.Log (i);
			if (playerName == score.playerName) {

				PlayerIndex = i;
			}

			i++;
		}

		temp = scoreList;
		scoreList = null;



		scoreList = temp.GetRange (PlayerIndex, lbRange+1);

	    scoreList.InsertRange (0, temp.GetRange (PlayerIndex - lbRange, lbRange));

		i = 0;
		foreach (dreamloLeaderBoard.Score score in scoreList) {


			Vector3 entryPosition = EntryList.transform.position + (Vector3.up*(400 - (i*60)));


			GameObject obj = (GameObject) Instantiate(EntryPrefab,entryPosition, EntryList.transform.rotation, EntryList);

			obj.GetComponent<LBEntry>().setScore (score.score);

			obj.GetComponent<LBEntry>().setPlayerName(score.playerName);

			if (playerName == score.playerName) {
				obj.GetComponent<LBEntry> ().SetMainPlayer ();
			
			}
		

			lBEntries.Add(obj.GetComponent<LBEntry>());
			i++;
		}

		int j = -lbRange;

		foreach (LBEntry lb in lBEntries) {

			lb.GetComponent<LBEntry> ().setScoreIndex (j+PlayerIndex+1);

			j++;
		}

	}


	public void ShowPlayerScore(int range){
		foreach (LBEntry l in lBEntries) {
			Destroy (l.gameObject);
		}

		lBEntries.Clear ();

		int i = 0;
		foreach (dreamloLeaderBoard.Score score in scoreList) {


			Vector3 entryPosition = EntryList.transform.position + (Vector3.up*(400 - (i*120)));


			GameObject obj = (GameObject) Instantiate(EntryPrefab,entryPosition, EntryList.transform.rotation, EntryList);

			obj.GetComponent<LBEntry>().setScore (score.score);

			obj.GetComponent<LBEntry>().setPlayerName(score.playerName);
			lBEntries.Add(obj.GetComponent<LBEntry>());
			i++;
		}


	//	scoreList.




	}





}
