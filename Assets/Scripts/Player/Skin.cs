using UnityEngine;
using System.Collections;

namespace UnityStandardAssets._2D
{
public class Skin : MonoBehaviour {

    public Renderer rend;
	public Color mycolor;


	public int type;


	void Start () {
        rend = GetComponent<Renderer>();
		Color color = rend.material.color;
		rend.material.color = color;
	}

	void Update () {
		if (GM.skinsave==type) {
	//		if (!stealth)				   check for stealth here
				rend.enabled = true;
		}
		else
			rend.enabled = false;
    }
}
}


