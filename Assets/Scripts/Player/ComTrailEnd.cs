using UnityEngine;
using System.Collections;

public class ComTrailEnd : MonoBehaviour {


public static ParticleSystem ps;
	void Start () {
    	ps = GetComponent<ParticleSystem>();
		ps.GetComponent<Renderer>().sortingLayerName = "front";
		ps.Stop();

	}

	void Update () {
		if (GM.completed==true) {
			ps.Play();
		}

		if (CloudsEnd.stopped==true) {
			ps.Stop();
		}
    }
}

