﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public enum ExplosionState { Inactive,During,Finished};

	public int damage = 2;
	public float secsBeforeExplosion = 0.0f;
	public float explosionDuration = 0.5f;
	public float AfterMathDuration = 2.0f;
	public LayerMask whatTohit;
	public ExplosionState expState = ExplosionState.Inactive;

	public CircleCollider2D explosionArea;

	public float ExpInaTimer;
	public float ExpDurTimer;
	public float ExpFinTimer;

	//Animator explosionAnim;
	ParticleSystem ps;

	AudioSource explosionSound;

	private SpriteRenderer expSprite;






	// Use this for initialization
	void Start () {
		expSprite = GetComponent<SpriteRenderer> ();
		SwitchExplosionState (ExplosionState.Inactive);
		//explosionAnim = GetComponent<Animator> ();
		ps = GetComponent<ParticleSystem>();
		explosionSound = GetComponent<AudioSource> ();
		//explosionArea.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {

		if (expState == ExplosionState.During) {
			ExpDurTimer -= Time.deltaTime;
		
			if (ExpDurTimer <= 0.0f) {
				EndExplosion ();
			}
		}
	}
		
	public void BeginExplosion(){
		SwitchExplosionState (ExplosionState.During);
		expSprite.enabled = true;
		//explosionAnim.SetBool ("Detonate", true);
		ps.Play();
		explosionSound.Play ();
		//Debug.Log ("explode");
	}

	public void EndExplosion(){
		SwitchExplosionState (ExplosionState.Finished);

		expSprite.enabled = false;
		//explosionAnim.SetBool ("Detonate", false);
	}

	void OnCollisionEnter2D(Collision2D col){
		Health health = col.gameObject.GetComponent<Health> ();

		if (health == null) {
			return;
		} else {
			health.Damage (damage);
		}
	}

	void OnTriggerEnter2D(Collider2D other){

		Health health = other.GetComponent<Health> ();

		if (health == null) {
			return;
		} else {
			health.Damage (damage);
		}
		//Debug.Log(other.name);
	}

	void OnTriggerStay2D(Collider2D other){

		Health health = other.GetComponent<Health> ();

		if (health == null) {
			return;
		} else {
			health.Damage (damage);
			explosionArea.enabled = false;
		}
		//Debug.Log(other.name);

	}

	void SwitchExplosionState(ExplosionState state){


		if (state == ExplosionState.Inactive) {
			
			explosionArea.enabled = false;

		} else if (state == ExplosionState.During) {

			explosionArea.enabled = true;
			ExpDurTimer = explosionDuration;

			
		} else if (state == ExplosionState.Finished) {

			explosionArea.enabled = false;

		}

		expState = state;
		
	}



}
