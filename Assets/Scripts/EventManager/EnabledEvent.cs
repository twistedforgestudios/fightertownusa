﻿using UnityEngine;
using System.Collections;

public class EnabledEvent : MonoBehaviour {

	public string EventTrigger;

	void OnEnable(){
		EventManager.TriggerEvent (EventTrigger);

	}
		
}
