﻿using UnityEngine;
using System.Collections;

public class LayerSetter : MonoBehaviour {

	Renderer render;

	// Use this for initialization
	void Start () {
		render = GetComponent <Renderer> ();

		render.sortingOrder = 50;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
