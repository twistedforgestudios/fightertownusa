﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SavedAllysUI : MonoBehaviour {

	public NumberUI numbers;
	//public GM gameMaster;

	// Use this for initialization
	void Start () {
		numbers.number = GM.alliesSaved;
	}
	
	// Update is called once per frame
	void Update () {
		numbers.number = GM.alliesSaved;
	}
}
