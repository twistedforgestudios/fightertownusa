﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CargoMenu : MonoBehaviour {

	public PlayerGameData playerData;
	public LevelGameData level01Data;
	public LevelGameData level02Data;
	public LevelGameData level03Data;
	public LevelGameData level04Data;
	public LevelGameData level05Data;
	public LevelGameData level06Data;
	public LevelGameData level07Data;
	public LevelGameData level08Data;
	public LevelGameData level09Data;
	public LevelGameData level10Data;

	public NumberUI cargoBoxesUI;


	public Text aircraftText;
	public Text weaponText;

	private int PowerUpLimit = 20;

	public NumberUI FullAutoUI;
	public NumberUI MultishotUI;
	public NumberUI LaserbeamUI;
	public NumberUI ExplodingBulletsUI;


	public NumberUI InvulnerabilityUI;
	public NumberUI SpeedUPUI; 

	public NumberUI ShieldUI;
	public NumberUI InvisibilityUI;

	public Text weaponTxt;
	public Text aircraftTxt;

	public NumberUI cargoLimitUI;




	// Use this for initialization
	void Start () {
	

		cargoLimitUI.number = playerData.CargoBoxUses;


		weaponTxt.text = playerData.StartingWeaponPower.ToString ();

		aircraftTxt.text = playerData.StartingAirPower.ToString ();



	}
	
	// Update is called once per frame
	void Update () {

		cargoBoxesUI.number = playerData.CargoBoxes;

		cargoLimitUI.number = playerData.CargoBoxUses;

		FullAutoUI.number = playerData.FullAutoPowerUps;
		MultishotUI.number = playerData.MultishotPowerUps;
		LaserbeamUI.number = playerData.LaserbeamPowerUps;
		ExplodingBulletsUI.number = playerData.ExplodingBulletsPowerUps;

		InvulnerabilityUI.number = playerData.InvulnerabilityPowerUps;
		SpeedUPUI.number = playerData.SpeedUpPowerUps;
		InvisibilityUI.number = playerData.InvisibilityPowerUps;
		ShieldUI.number = playerData.ShieldPowerUps;
	}




	public void AddWeaponPowerUp(PlayerPowerState.WeaponPower power){


		switch (power) {
		case PlayerPowerState.WeaponPower.FullAuto:
			playerData.FullAutoPowerUps++;
			playerData.FullAutoPowerUps = Mathf.Clamp(playerData.FullAutoPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			playerData.MultishotPowerUps++;
			playerData.MultishotPowerUps = Mathf.Clamp (playerData.MultishotPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			playerData.LaserbeamPowerUps++;
			playerData.LaserbeamPowerUps = Mathf.Clamp (playerData.LaserbeamPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			playerData.ExplodingBulletsPowerUps++;
			playerData.ExplodingBulletsPowerUps = Mathf.Clamp (playerData.ExplodingBulletsPowerUps, 0, PowerUpLimit);
			break;
	
		default:
			break;

		}

	}

	public void AddAircraftPowerUp(PlayerPowerState.AircraftPower power){


		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			playerData.InvulnerabilityPowerUps++;
			playerData.InvulnerabilityPowerUps = Mathf.Clamp (playerData.InvulnerabilityPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			playerData.SpeedUpPowerUps++;
			playerData.SpeedUpPowerUps = Mathf.Clamp (playerData.SpeedUpPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			playerData.ShieldPowerUps++;
			playerData.ShieldPowerUps = Mathf.Clamp (playerData.ShieldPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			playerData.InvisibilityPowerUps++;
			playerData.InvisibilityPowerUps = Mathf.Clamp (playerData.InvisibilityPowerUps, 0, PowerUpLimit);
			break;

		default:
			break;

		}

	}
		

	 public void ApplyWeaponPowerUp(PlayerPowerState.WeaponPower power){

		playerData.StartingWeaponPower = power;
		RemoveWeaponPowerUp (power);

	}

	 public void ApplyAircraftPowerUp(PlayerPowerState.AircraftPower power){


		playerData.StartingAirPower = power;
		RemoveAircraftPowerUp (power);
	}



	public void RemoveWeaponPowerUp(PlayerPowerState.WeaponPower power){


		switch (power) {
		case PlayerPowerState.WeaponPower.FullAuto:
			playerData.FullAutoPowerUps--;
			playerData.FullAutoPowerUps = Mathf.Clamp(playerData.FullAutoPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			playerData.MultishotPowerUps--;
			playerData.MultishotPowerUps = Mathf.Clamp (playerData.MultishotPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			playerData.LaserbeamPowerUps--;
			playerData.LaserbeamPowerUps = Mathf.Clamp (playerData.LaserbeamPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			playerData.ExplodingBulletsPowerUps--;
			playerData.ExplodingBulletsPowerUps = Mathf.Clamp (playerData.ExplodingBulletsPowerUps, 0, PowerUpLimit);
			break;

		default:
			break;

		}

	}

	public void RemoveAircraftPowerUp(PlayerPowerState.AircraftPower power){


		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			playerData.InvulnerabilityPowerUps--;
			playerData.InvulnerabilityPowerUps = Mathf.Clamp (playerData.InvulnerabilityPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			playerData.SpeedUpPowerUps--;
			playerData.SpeedUpPowerUps = Mathf.Clamp (playerData.SpeedUpPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			playerData.ShieldPowerUps--;
			playerData.ShieldPowerUps = Mathf.Clamp (playerData.ShieldPowerUps, 0, PowerUpLimit);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			playerData.InvisibilityPowerUps--;
			playerData.InvisibilityPowerUps = Mathf.Clamp (playerData.InvisibilityPowerUps, 0, PowerUpLimit);
			break;

		default:
			break;

		}

	}


	public void SetPowerUpButtons(){

		if (level01Data.Unlocked) {


		}

		if (level02Data.Unlocked) {


		}

		if (level03Data.Unlocked) {


		}

		if (level04Data.Unlocked) {


		}
		if (level05Data.Unlocked) {


		}
		if (level06Data.Unlocked) {


		}
		if (level07Data.Unlocked) {


		}
		if (level08Data.Unlocked) {


		}
	}

	
	public void SetWeaponPower(PlayerPowerState.WeaponPower power){



		switch (power) {

		case PlayerPowerState.WeaponPower.FullAuto:

			if (playerData.FullAutoPowerUps > 0) {
				playerData.StartingWeaponPower = power;
				weaponTxt.text = power.ToString ();
			}


			break;
		case PlayerPowerState.WeaponPower.Multishot:
			
			if (playerData.FullAutoPowerUps > 0) {
				playerData.StartingWeaponPower = power;
				weaponTxt.text = power.ToString ();
			}

			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			
			playerData.StartingWeaponPower = power;
			weaponTxt.text = power.ToString ();

			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			
			playerData.StartingWeaponPower = power;
			weaponTxt.text = power.ToString ();

			break;
		default:
			break;

		}

	}

	public void SetAircraftPower(PlayerPowerState.AircraftPower power){
		


		switch (power) {
		case PlayerPowerState.AircraftPower.Invulnerability:
			playerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.SpeedUp:
			playerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.Shield:
			playerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			playerData.StartingAirPower = power;
			aircraftTxt.text = power.ToString ();
			break;

		default:
			break;

		}

	}





}
