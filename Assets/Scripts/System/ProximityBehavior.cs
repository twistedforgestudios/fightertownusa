﻿using UnityEngine;
using System.Collections;

public class ProximityBehavior : MonoBehaviour {


	public MonoBehaviour script;
	public string targetTag;
	//public float ActiveTime;
	public bool useTag;
	public bool OnlyOnce;
	//private float activeTimer;
	Collider2D proximityArea;
	//private bool isActive = false;
	// Use this for initialization




	void Start () {
		proximityArea = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {



	}

	void OnTriggerEnter2D(Collider2D col){


		if (useTag) {
			if (col.tag == targetTag) {
				//Debug.Log (col.tag);
				if(script != null)
					script.enabled = true;

				//script.SendMessage ("AcquireTarget", col.transform);
			}
		} else {
			if(script != null)
				script.enabled = true;
		}
//		Debug.Log ("target found");
		//AcquireTarget (col.transform);
	
		
	}

	void OnTriggerExit2D(Collider2D col){
		if (useTag) {
			if (col.tag == targetTag) {

				if(script != null)
					script.enabled = false;

				//script.SendMessage ("AcquireTarget", col.transform);
			}
		} else {

			if(script != null)
				script.enabled = false;
		}

		if (OnlyOnce) {
			proximityArea.enabled = false;
		}

	}

	void OnTriggerStay2D(Collider2D col){
		//script.SendMessage ("AcquireTarget", col.transform);
		//Debug.Log ("staying on target");

		if (useTag) {
			if (col.tag == targetTag) {
				script.enabled = true;
			} else {
				script.enabled = false;
			}
		} else {
			script.enabled = true;
		}
			
	}



	void OnCollisionEnter2D(Collision2D other){

		if (useTag) {
			if (other.gameObject.tag == targetTag) {
				script.enabled = true;
			} else {
				script.enabled = false;
			}
		} else {
			script.enabled = true;
		}
	}
		

	//void OnDisable(){
	//	proximityArea.enabled = false;
	//}




}
