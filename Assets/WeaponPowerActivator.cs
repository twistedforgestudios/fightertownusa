﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPowerActivator : MonoBehaviour {

	public PlayerPowerState playerstate;
	public PlayerGameData playerdata;
	public Button button;
	public int powerUses = 1;

	void Start(){


		if (playerdata.StartingWeaponPower == PlayerPowerState.WeaponPower.Normal) {
			if (button != null)
				button.interactable = false;
		}

	}

	void Update(){
		
	}



	public void ActivateWeaponPower(){

		if (powerUses > 0) {
			powerUses--;

			if (button != null)
				button.interactable = false;


			switch (playerdata.StartingWeaponPower) {

			case PlayerPowerState.WeaponPower.FullAuto:
				playerstate.FullAuto (30f);
				break;
			case PlayerPowerState.WeaponPower.Multishot:
				playerstate.Multishot (30f);
				break;
			case PlayerPowerState.WeaponPower.Laserbeam:
				playerstate.LaserBeam (30f);
				break;
			case PlayerPowerState.WeaponPower.ExplodingBullets:
				playerstate.ExplodingBullets (30f);
				break;
			default:
				break;
			}

			playerdata.StartingWeaponPower = PlayerPowerState.WeaponPower.Normal;

		}
	}

	public  void EnableWeaponPowerBtn(){
		
	}

}
