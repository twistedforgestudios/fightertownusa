using UnityEngine;
using System.Collections;

public class HelpBackToTop : MonoBehaviour {

    public Renderer rend;
	public int type;

	void OnMouseDown () {
		Help.scrollpad=0;
	}


	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Help.active==true)
			rend.enabled = true;
		else
			rend.enabled = false;

	
	}
}
