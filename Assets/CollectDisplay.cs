﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectDisplay : MonoBehaviour {

	public string CargoText;
	public string ShipUpgradeText;

	public string FullAutoText;
	public string MultishotText;
	public string LazerText;
	public string ExplodingBulletsText;
	public string InvisibilityText;
	public string InvulnerabilityText;
	public string SheildText;
	public string SpeedText;

	public string YellowDiamond;
	public Text text;
	public Animator collectAnim;
	// Use this for initialization
	void Start () {
		text.text = "";
	}
	
	// Update is called once per frame
	void Update () {




	}

	public void SetCargoBoxText(){
		text.text = CargoText;
	}

	public void SetShipUpgradeText(){
		text.text = ShipUpgradeText;
	}
		
	public void SetFullAutoText(){
		text.text = FullAutoText;
	}

	public void SetMultishotText(){
		text.text = MultishotText;
	}

	public void SetLazerBeamText(){
		text.text = LazerText;
	}
		
	public void SetExplodingShotText(){
		text.text = ExplodingBulletsText;
	}
		
	public void SetInvulnerabilityText(){
		text.text = InvulnerabilityText;
	}
		
	public void SetSheildText(){
		text.text = SheildText;
	}

	public void SetSpeedText(){
		text.text = SpeedText;
	}

	public void SetInvisibilityText(){
		text.text = InvisibilityText;
	}

	public void SetYellowDiamondText(){
		text.text = YellowDiamond;
	}

	public void PlayCargoAnim(){
		collectAnim.SetTrigger ("CargoBoxCollect");
		//Debug.Log ("Colletecd diplay");
	}

}
