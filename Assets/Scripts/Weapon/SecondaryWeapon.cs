﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SecondaryWeapon : MonoBehaviour {
	public  bool Controlled = true;
	public float fireRate = 0;
	public float Damage = 10;
	public float prepShootTime = 0;
	public float shootTime = 0;
	public GameObject curSecWeapon;

	public bool ShotFired = false;

	public GameObject[] secWeapons;
	public float Delayscale;
	//private Dictionary<string,GameObject> weapons;

	float timeToSpawnEffect = 0;
	public float effectSpawnRate;

	float timeToFire = 0;
	float preShootTimer;
	float shootTimer;
	bool preShootActive = false;
	bool shootActive = false;

	public bool UsesSecondaryAmmo = true;

	// Use this for initialization
	void Start () {
	

		preShootTimer = prepShootTime;
		shootTimer = shootTime;
		/*
		weapons = new Dictionary<string,GameObject> ();

		foreach (GameObject w in secWeapons) {

			if(weapons != null)
				weapons.Add (w.name,w);
		}
*/
		//curSecWeapon = secWeapons [0];

	}
	
	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.W)){
			GM.button_pressed = 1;
		}
		#endif

		#if UNITY_STANDALONE_WIN
		if(Input.GetKeyDown(KeyCode.W)){
			GM.button_pressed = 1;
		}
		#endif

		if (Controlled) {
			if (fireRate == 0 && GM.secondary_weapon_uses > 0) {
				//  if (GM.button_pressed_type==3) {		 //bob
				if (GM.button_pressed == 1) {		 //bob
					//FireSecondaryWeapon();
					preShootActive = true;

					GM.button_pressed = 0;	//bob
				} else {
					ShotFired = false;
				}

			} else {
				if (Time.time > timeToFire) {	  //bob

					if (GM.button_pressed == 1) {		 //bob
						//FireSecondaryWeapon();
						GM.button_pressed_type = 0;	  //bob
						FireSecondaryWeapon ();
						GM.button_pressed = 0;	//bob
					} else {
						ShotFired = false;
					}

					timeToFire = Time.time + 1 / fireRate;
			

					//Debug.Log ("Shoot");

				}
			}

		}


		//Represents Time before a shot
		if(preShootActive){
			
			preShootTimer -= Time.deltaTime;


			if(preShootTimer <= 0){
				ShotFired = true;

				preShootActive = false;
				preShootTimer = prepShootTime;
				shootActive = true;
			}
		}

		//Represents time during a shot
		if(shootActive){
			shootTimer -= Time.deltaTime;

			if(shootTimer <= 0){
				shootActive = false;
				shootTimer = shootTime;
				FireSecondaryWeapon();
				ShotFired = false;
			}

		}

		//ShotFired = false;
		/*
		if(Input.GetButtonDown("Jump")){
			GM.sec_weapon_current=GM.sec_weapon_current^1;

			SwitchSecondaryWeapon(secWeapons[GM.sec_weapon_current].name);
		}*/


		//

	}

	public void FireSecondaryWeapon(){
		if (Time.time >= timeToSpawnEffect) {

			if (UsesSecondaryAmmo) {
				GM.secondary_weapon_uses--;
				Mathf.Clamp (GM.secondary_weapon_uses, 0, 50);
			}

			//Debug.Log ("Firing");
			if(curSecWeapon != null && curSecWeapon.activeSelf)
				curSecWeapon.SendMessage ("Shoot");

			timeToSpawnEffect = Time.time + 1/effectSpawnRate * Delayscale;
		}
	}


	public void FireSecondaryWeaponAlt(){
		preShootActive = true;


	}





	public void SwitchSecondaryWeapon(string w){


	//	if (weapons [w] == null)
	//		return;

	//	curSecWeapon = weapons[w];
                                                	
	}
}
