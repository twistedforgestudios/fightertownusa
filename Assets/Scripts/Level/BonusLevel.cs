﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BonusLevel : MonoBehaviour {


	public Sprite levelUnfinished;
	public Sprite LevelFinished;
	public Sprite oneDiamondComplete;
	public Sprite twoDiamondComplete;
	public Sprite threeDiamondComplete;


	public LevelGameData lD1;
	public LevelGameData lD2;
	public LevelGameData lD3;
	public LevelGameData lD4;
	public LevelGameData lD5;

	public int  NumofYDCollected;

	public LevelGameData bonusStage;
	private Button button;
	private Image levelIcon;
	public Image starsIcon;
	public RectTransform DisplayPrompt;

	public StartLevel startButton;
	// Use this for initialization
	void Start () {
		NumofYDCollected = lD1.YellowDiamondsColleted + lD2.YellowDiamondsColleted + lD3.YellowDiamondsColleted + lD4.YellowDiamondsColleted + lD5.YellowDiamondsColleted;
		button = GetComponent<Button> ();
		levelIcon = GetComponent<Image> ();

		if (NumofYDCollected >= 15) {
			bonusStage.Unlocked = true;
		} else {
			bonusStage.Unlocked = false;
		}


		if (!bonusStage.Unlocked) {
			levelIcon.sprite = levelUnfinished;
			button.interactable = false;
		}
		/*else if (bonusStage.Completed) {

			if (bonusStage.YellowDiamondsColleted == 0)
				levelIcon.sprite = LevelFinished;
			else if (bonusStage.YellowDiamondsColleted == 1)
				levelIcon.sprite = oneDiamondComplete;
			else if (bonusStage.YellowDiamondsColleted == 2)
				levelIcon.sprite = twoDiamondComplete;
			else if (bonusStage.YellowDiamondsColleted == 3)
				levelIcon.sprite = threeDiamondComplete;

		} */
		else {
			levelIcon.sprite = levelUnfinished;
		}


	}
	
	// Update is called once per frame
	void Update () {
	
	}



	public void SelectLevel(){

		if (startButton.CheckIfEnoughFuelCells()) {

			startButton.GetComponent<Button> ().interactable = true;
			DisplayPrompt.gameObject.SetActive (false);
		} else {
			startButton.GetComponent<Button> ().interactable = false;
			DisplayPrompt.gameObject.SetActive (true);
		}


	}




}
