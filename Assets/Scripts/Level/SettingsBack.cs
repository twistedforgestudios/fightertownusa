using UnityEngine;
using System.Collections;

public class SettingsBack : MonoBehaviour {

	public float screenwoffset;
	public float screenhoffset;
	public float heightoffset;
	public float widthoffset;
    public float offset;


//	private Vector3 coords;

	public GUIStyle customButton;
	public SpriteRenderer sprite;
	public RectTransform rectTrans;

	void Start () {
		screenwoffset=Screen.width;
		screenhoffset=Screen.height;   
	    heightoffset=387;
		widthoffset=765;
    	offset=screenhoffset / heightoffset;

		sprite = GetComponent<SpriteRenderer> ();

	}

	void OnMouseUpAsButton () {
    	Settings.active=true;
		print("back");
    	Credits.active=false;
    	Help.active=false;
	}


	void Update () {

		if (Settings.active) {
			sprite.enabled = false;
			rectTrans.gameObject.SetActive (false);
		} else {
			sprite.enabled = true;
			rectTrans.gameObject.SetActive (true);
		}
	}
}
