﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelCellTimer : MonoBehaviour {
	
	public PlayerGameData playerData;
	public bool CargoReady;

	public NumberUI secondsUI;
	public NumberUI minutesUI;

	public float TimePerCell = 60f;

	public float TimeLimit;
	public bool timesUp;
	public string eventName;
	public string message;

	float timer;
	int seconds;
	int  minutes;

	public int fuelCellLimit;

	// Use this for initialization
	void Start () {

		timer = TimeLimit;

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


		if (timesUp) {
			

		} else {


		}

	}
	
	// Update is called once per frame
	void Update () {

		if (!timesUp) {
			timer -= Time.deltaTime;

			if (timer <= 0.0f) {
				timesUp = true;

				AcquireFuelCell ();

				//we restart the timer after the fuel cell is acquired

				RestartTimer (TimePerCell);

				if (eventName != "")
					EventManager.TriggerEvent (eventName);

				if (message != "")
					SendMessage (message);

			}
		}


		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


	}

	public void RestartTimer(){
		timer = TimeLimit;
	}

	public void RestartTimer(float time){
		TimeLimit = time;
		timer = TimeLimit;


		timesUp = false;
	}

	public void UseFuelCell(){
		playerData.DecrementFuelCell ();
		Debug.Log ("Used A fuel Cell");
	}


	public void  AcquireFuelCell(){

		playerData.IncrementFuelCell ();
		Debug.Log ("Acquired A fuel Cell");
	}


	//every 1 minute different we acquire a single fuel cell
	public void ReduceTime(float seconds){
		int cells;
		float remainaderTime;

		//number of fuel cells  = seconds past/ 60 
		cells = (int)(seconds/TimePerCell);
		remainaderTime = seconds % TimePerCell;

		playerData.IncrementFuelCell (cells);

		if (cells > 0) {
			
			TimeLimit = TimePerCell - remainaderTime;
		} else {

			TimeLimit -= remainaderTime;
		}


	}

	public float GetTimeLeft(){
		return timer;
	}




}
