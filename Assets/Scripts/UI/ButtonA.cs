using UnityEngine;
using System.Collections;

public class ButtonA : MonoBehaviour {

	public GameObject[] objects;
	public int i;
	public int window;
	public int touches;
	public float touchespos1x;
	public float touchespos1y;
	public float touchespos2x;
	public float touchespos2y;

	public bool touchscreenbottom;

	public GUIStyle customButton;
		
	private Vector3 coords;


	public int heightoffset;
	public int widthoffset;



	void OnMouseDrag () {
            #if UNITY_EDITOR
				GM.button_pressed =2;
			#endif
            #if UNITY_STANDALONE_WIN
		    	GM.button_pressed =2;
			#endif 		
	}


private void OnGUI () {
	customButton.fontSize = Mathf.RoundToInt (10f * Screen.height / (heightoffset * 1.0f));
 //   GUI.Label(new Rect(100, 140, 1, 300), "t1y "+touchespos1y, customButton);
	/*
	GUI.Label(new Rect(100, 100, 1, 300), "t "+touches, customButton);
	GUI.Label(new Rect(100, 120, 1, 300), "t1x "+touchespos1x, customButton);
	GUI.Label(new Rect(100, 160, 1, 300), "t2x "+touchespos2x, customButton);
	GUI.Label(new Rect(100, 180, 1, 300), "t2y "+touchespos2y, customButton);
 //   GUI.Label(new Rect(100, 200, 1, 300), "coordx "+coords.x, customButton);
 //   GUI.Label(new Rect(100, 220, 1, 300), "coordy "+coords.y, customButton);

	GUI.Label(new Rect(100, 200, 1, 300), "coord_dx "+ButtonDown.x, customButton);
	GUI.Label(new Rect(100, 220, 1, 300), "coord_dy "+ButtonDown.y, customButton);

	GUI.Label(new Rect(100, 240, 1, 300), "coord_dx "+ButtonUp.x, customButton);
	GUI.Label(new Rect(100, 260, 1, 300), "coord_dy "+ButtonUp.y, customButton);

 //   GUI.Label(new Rect(100, 320, 1, 300), "type "+GM.button_pressed_type, customButton);
 */

}

	// Use this for initialization
	void Start () {
		objects = GameObject.FindGameObjectsWithTag("a_button");

		coords = Camera.main.WorldToScreenPoint(transform.position);	    

		//window=20;

		//heightoffset=387;
		//widthoffset=765;

		touchscreenbottom=false;


	}
	
	// Update is called once per frame
	void Update () {
		
		int nbTouches = Input.touchCount;

		if(nbTouches > 0)
		{
	    	//print(nbTouches + " touch(es) detected");

			touches=nbTouches;

			touchespos1x=0;
			touchespos1y=0;
			touchespos2x=0;
			touchespos2y=0;

			touchscreenbottom=false;

			GM.button_pressed_type=0;



			for (int i = 0; i < nbTouches; i++)
			{
				Touch touch = Input.GetTouch(i);

			//	if (touch.phase == TouchPhase.Began) {
					if (i == 0) {
						touchespos1x = touch.position.x;
						touchespos1y = touch.position.y;
					}

					if (i == 1) {
						touchespos2x = touch.position.x;
						touchespos2y = touch.position.y;
					}

					/*
				if (touchespos1y>(Screen.height/2)) {
					GM.button_pressed_type = 1;
				}

				if (touchespos2y>(Screen.height/2)) {
					GM.button_pressed_type = 1;
				}
				if (touchespos1y<(Screen.height/2)) {
					if (GM.button_pressed_type!=1)
						GM.button_pressed_type = 2;
				}
				if (touchespos2y<(Screen.height/2)) {
					if (GM.button_pressed_type!=1)
						GM.button_pressed_type = 2;
				}
				*/

					if (touchespos1x > (coords.x - window) && touchespos1x < (coords.x + window)) {
						if (touchespos1y > (coords.y - window) && touchespos1y < (coords.y + window)) {
							GM.button_pressed = 2;
						}
					}

					if (touchespos2x > (coords.x - window) && touchespos2x < (coords.x + window)) {
						if (touchespos2y > (coords.y - window) && touchespos2y < (coords.y + window)) {
							GM.button_pressed = 2;												    
						}
					}
		
					if (touchespos1x > (ButtonB.x - window) && touchespos1x < (ButtonB.x + window)) {
						if (touchespos1y > (ButtonB.y - window) && touchespos1y < (ButtonB.y + window)) {
							GM.button_pressed_type = 3;
							GM.button_pressed = 1;
						}
					}

					if (touchespos2x > (ButtonB.x - window) && touchespos2x < (ButtonB.x + window)) {
						if (touchespos2y > (ButtonB.y - window) && touchespos2y < (ButtonB.y + window)) {
							GM.button_pressed_type = 3;
							GM.button_pressed = 1;
						}
					}


					if (touchespos1x > (ButtonDown.x - window) && touchespos1x < (ButtonDown.x + window)) {
						if (touchespos1y > (ButtonDown.y - window) && touchespos1y < (ButtonDown.y + window)) {
							GM.button_pressed_type = 2;
						}
					}

					if (touchespos2x > (ButtonDown.x - window) && touchespos2x < (ButtonDown.x + window)) {
						if (touchespos2y > (ButtonDown.y - window) && touchespos2y < (ButtonDown.y + window)) {
							GM.button_pressed_type = 2;
						}
					}

					if (touchespos1x > (ButtonUp.x - window) && touchespos1x < (ButtonUp.x + window)) {
						if (touchespos1y > (ButtonUp.y - window) && touchespos1y < (ButtonUp.y + window)) {
							GM.button_pressed_type = 1;
						}
					}

					if (touchespos2x > (ButtonUp.x - window) && touchespos2x < (ButtonUp.x + window)) {
						if (touchespos2y > (ButtonUp.y - window) && touchespos2y < (ButtonUp.y + window)) {
							GM.button_pressed_type = 1;
						}
					}

				//}

			}
		}
		else {
			touches=0;
		}
		/*

        if (PlayerPowerState.curWeaponPower==PlayerPowerState.WeaponPower.Normal)
        	GM.primary_weapon_current=0;
        if (PlayerPowerState.curWeaponPower==PlayerPowerState.WeaponPower.Multishot)
        	GM.primary_weapon_current=1;
  //  	print("curWeaponPower "+GM.primary_weapon_current);

		//for (int i=0;i!=2;i++) {  
		//	objects[i].SetActive(false);
		//}

		//objects[GM.primary_weapon_current].SetActive(true);*/
	}
}
