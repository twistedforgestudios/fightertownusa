﻿using UnityEngine;
using System.Collections;

public class ToolTipManager : MonoBehaviour {




	// Use this for initialization
	void Start () {
	
		EventManager.StartListening ("PowerUpCollected", ShowPowerUpAcquired);
		EventManager.StartListening ("YellowDiamonCollected", ShowPowerUpAcquired);
		EventManager.StartListening ("CargoBoxCollected", ShowPowerUpAcquired);

	}


	// Update is called once per frame
	void Update () {

	}


	public void ShowPowerUpAcquired(){
		
	}

	public void ShowYellowDiamondAcquired(){
		
	}

	public void ShowCargoBoxAcquired(){
		
	}

}
