using UnityEngine;
using System.Collections;


public class MM : MonoBehaviour {
	static int soundon;
	static public int soundplay;
	static public int musicplay;
	static public int musicstop;
	static public int musicnewsong;
//	static int audioend;
	static public int delay;
	static float audio1Volume;
//	static float audio1Volumereset;

	static public AudioClip[] musicAudio = new AudioClip[10];

	static public AudioClip[] soundAudio = new AudioClip[10];

	AudioSource music;

	static public bool musicvolumechange;
	static public bool soundvolumechange;
	static public float musicvolumechangevalue;
	static public float soundvolumechangevalue;

void Awake () {
		musicplay=-1;
		musicAudio[0] = Resources.Load("Audio/Give Away") as AudioClip;
		//musicAudio[1] = Resources.Load("Audio/causeway") as AudioClip;
		//musicAudio[2] = Resources.Load("Audio/Give Away") as AudioClip;

		soundAudio[0] = Resources.Load("Audio/Laser_Shoot2") as AudioClip;
		soundAudio[1] = Resources.Load("Audio/Laser_Shoot2") as AudioClip;
}

void Start () {
	soundplay=-1;
	musicstop=0;
//	audioend=0;
//	audio1Volumereset=0;

	audio1Volume=1.0f;
//	audio1Volumereset=1.0f;

	musicnewsong=0;
	music = GetComponent<AudioSource> ();

	delay=0;


	musicvolumechangevalue = PlayerPrefs.GetFloat("musicvolumechangevalue");
	musicnewsong = PlayerPrefs.GetInt("musicnewsong");
	
	soundvolumechangevalue = PlayerPrefs.GetFloat("soundvolumechangevalue");

}

void Update () {


	if (musicplay!=-1) {

		musicvolumechangevalue = PlayerPrefs.GetFloat("musicvolumechangevalue");




		if (musicplay==0) {
			music.clip = musicAudio[musicnewsong];
		}
		else
			music.clip = musicAudio[musicplay];

		audio1Volume=0.5f;
		musicplay=-1;
    	musicstop=2;
		music.volume=audio1Volume;
		music.Play ();
	}

	if (musicstop==1) {
		fadeOut(.5f);
	}
	if (musicstop==2) {
		fadeIn();
	}

	if (musicstop==3) {
		fadeOutNew(2f,musicnewsong);
	}

	if (soundplay!=-1) {
		SoundPlay(soundplay);
		soundplay=-1;
	}

	if (musicvolumechange) {
		musicvolumechange=false;
		audio1Volume = musicvolumechangevalue;
		music.volume=audio1Volume;
		print("audio1Volume "+audio1Volume);
	}

	if (soundvolumechange) {
		soundplay=1;
		soundvolumechange=false;
	}

} 

void SoundPlay(int clip) {
    audio1Volume = soundvolumechangevalue;	 
    AudioSource.PlayClipAtPoint (soundAudio[clip], Camera.main.transform.position,audio1Volume);
}

void fadeOut(float speed) {
    if(audio1Volume > 0)
    {
        audio1Volume -= speed * Time.deltaTime;
        music.volume = audio1Volume;
    }  
	else {
        music.volume = 0;
		musicstop=0;
		music.Stop();
	}
}

void fadeOutNew(float speed, int song) {
    if(audio1Volume > 0)
    {
        audio1Volume -= speed * Time.deltaTime;
        music.volume = audio1Volume;
    }  
	else {
        music.volume = 0;
		musicstop=0;
		music.Stop();
		musicplay=song;
	}
}




void fadeIn() {
    if(audio1Volume < musicvolumechangevalue)
    {
        audio1Volume += 0.15f * Time.deltaTime;
    }  
	else {
		audio1Volume = musicvolumechangevalue;
		musicstop=0;

	}

	music.volume = audio1Volume;
}

}


