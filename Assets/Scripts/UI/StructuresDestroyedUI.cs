﻿using UnityEngine;
using System.Collections;

public class StructuresDestroyedUI : MonoBehaviour {
	public NumberUI numbers;
	// Use this for initialization
	void Start () {
		numbers.number = GM.strucutures_destroyed;
	}
	
	// Update is called once per frame
	void Update () {
		numbers.number = GM.strucutures_destroyed;
	}
}
