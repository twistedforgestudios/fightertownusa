using UnityEngine;
using System.Collections;


public class Smoke : MonoBehaviour {


public static ParticleSystem ps;
	void Start () {
		ps = GetComponent<ParticleSystem>();
        ps.GetComponent<Renderer>().sortingLayerName = "front";
	}

	void Update () {
		if (GM.smokeflag==true){
			transform.position = GM.planepos;
            Vector3 temp = GM.planepos; // copy to an auxiliary variable...
			temp.y-=6;
			transform.position=temp;
    		ps.Play();
			GM.smokeflag=false;
		}

	
	}
}
