﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets._2D;
using UnityStandardAssets.CrossPlatformInput;

public class SpeedBoost : MonoBehaviour {

	public float speedIncrease;
	public ParticleSystem leftComtrailParticle;
	public ParticleSystem rightComtrailParticle;
	public float speedIncreaseTime;

	public enum SpeedState{ Turning,Waiting, Speeding};

	public SpeedState speedState = SpeedState.Waiting;

	PlatformerCharacter2D player;
	float speedIncreaseTimer;

	// Use this for initialization
	void Start () {
		speedIncreaseTimer = speedIncreaseTime;
		player = GetComponent<PlatformerCharacter2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	
		if (CrossPlatformInputManager.GetAxis ("Vertical") != 0) {

			if (leftComtrailParticle != null && leftComtrailParticle.isStopped )
				leftComtrailParticle.Play ();
			    
			if (rightComtrailParticle != null&& rightComtrailParticle.isStopped  )
				rightComtrailParticle.Play ();
			



			if (speedState == SpeedState.Turning) {



			} else if (speedState == SpeedState.Waiting) {
				speedIncreaseTimer = speedIncreaseTime;
				speedState = SpeedState.Turning;

				
			} else if (speedState == SpeedState.Speeding) {
				player.m_MaxSpeed -= speedIncrease;
				speedIncreaseTimer = speedIncreaseTime;
				speedState = SpeedState.Turning;


				
			}


		} else {

			if (speedState == SpeedState.Turning) {
				speedState = SpeedState.Waiting;

				if (leftComtrailParticle != null && leftComtrailParticle.isPlaying)
					leftComtrailParticle.Stop ();


				if (rightComtrailParticle != null && rightComtrailParticle.isPlaying)
					rightComtrailParticle.Stop();


			} else if (speedState == SpeedState.Waiting) {
				speedIncreaseTimer -= Time.deltaTime;

				if (speedIncreaseTimer <= 0f) {
					speedIncreaseTimer = speedIncreaseTime;
					player.m_MaxSpeed += speedIncrease;
					speedState = SpeedState.Speeding;

					if (leftComtrailParticle != null  && leftComtrailParticle.isStopped )
						leftComtrailParticle.Play ();



					if (rightComtrailParticle != null && rightComtrailParticle.isStopped)
						rightComtrailParticle.Play ();
					

				}

			} else if (speedState == SpeedState.Speeding) {
				
			}
		}



	}
}
