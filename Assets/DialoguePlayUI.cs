﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePlayUI : MonoBehaviour {


	public GM levelMananger;
	public RectTransform[] tips;
	public AudioSource[] clips;
	public float TutorialAppearTime;
	public float TutorialAppearDelay;

	public bool tutorialStarted;
	public bool stoppedTutorial;
	[SerializeField]
	private float tutorialTimer;
	[SerializeField]
	private float delayTimer;
	[SerializeField]
	private int currentTutorialTip = 0;

	// Use this for initialization
	void Start () {
		tutorialTimer = TutorialAppearTime;
		delayTimer = TutorialAppearDelay;
	}

	// Update is called once per frame
	void Update () {

		if (!tutorialStarted && !stoppedTutorial) {
			//Debug.Log("continueing tutorial");
			tutorialTimer -= Time.deltaTime;

			if (tutorialTimer <= 0.0f) {
				tutorialStarted = true;

				if (currentTutorialTip >= 1) {
					tips[currentTutorialTip-1].gameObject.SetActive (false);
				}


				if (currentTutorialTip <= tips.Length+1) {


					if (currentTutorialTip < tips.Length) {
						Debug.Log(currentTutorialTip);
						tips [currentTutorialTip].gameObject.SetActive (true);

						if( clips[currentTutorialTip].clip != null){
							delayTimer = clips[currentTutorialTip].clip.length + TutorialAppearDelay;
						}
						else{
							delayTimer =  TutorialAppearDelay;
						}
					}

					currentTutorialTip++;
				}



			}
		}


		if (tutorialStarted && !stoppedTutorial) {

			delayTimer -= Time.deltaTime;

			if (delayTimer <= 0.0f) {
				tutorialStarted = false;
				//delayTimer = TutorialAppearDelay;

				if (tips.Length == currentTutorialTip) {
					stoppedTutorial = true;
					tutorialStarted = true;
					Debug.Log("tutorialStopped");
					foreach(RectTransform r in tips){
						r.gameObject.SetActive(false);
					}
				}
			}
		}

	}
}
