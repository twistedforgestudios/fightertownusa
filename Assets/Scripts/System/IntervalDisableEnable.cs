﻿using UnityEngine;
using System.Collections;

public class IntervalDisableEnable : MonoBehaviour {


	public GameObject obj;
	public float intervalTime;
	bool isActive;



	// Use this for initialization
	void Start () {
		Invoke ("DisableObject", intervalTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void DisableObject(){
		obj.SetActive (false);
		Invoke ("EnableObject", intervalTime);
	}

	public void EnableObject(){
		obj.SetActive (true);
		Invoke ("DisableObject", intervalTime);


	}

}
