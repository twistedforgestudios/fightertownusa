﻿using UnityEngine;
using System.Collections;

public class CargoBoxCollectable : MonoBehaviour {

	public PlayerGameData playerData;
	public SpriteRenderer cargoSprite;
	public AudioSource CollectSound;
	private Collider2D collider2d;
	// Use this for initialization
	void Start () {
		CollectSound = GetComponent<AudioSource> ();
		cargoSprite = GetComponent<SpriteRenderer> ();
		collider2d = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){

	//	PlayerPowerState pps = col.gameObject.GetComponent<PlayerPowerState> ();
		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();

		collider2d.enabled = false;
		cd.SetCargoBoxText ();
		cd.PlayCargoAnim ();
		Debug.Log ("Cargo box");

		playerData.CargoBoxes++;
		EventManager.TriggerEvent ("CargoBoxCollected");
		cargoSprite.enabled = false;
		CollectSound.Play ();
	}
}
