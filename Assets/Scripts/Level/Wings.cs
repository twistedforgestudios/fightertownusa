using UnityEngine;
using System.Collections;

public class Wings : MonoBehaviour {

    public GameObject[] wings;
    public GameObject[] stars;
    public GameObject[] starsbright;

	public float startx=-5.4f;
	public float startx1=-5.7f+100-.71f;
	public float startx2;
	public float startx3;
	public float starty=2.48f;
	public float starty1=2.7f;
	public float multy;
	public int display;
	static public bool update;

    public Renderer rend1;
    public Renderer rend2;
    public Renderer rend3;


	void Start () {

		wings = GameObject.FindGameObjectsWithTag("wings");
    	stars = GameObject.FindGameObjectsWithTag("star_pale");
    	starsbright = GameObject.FindGameObjectsWithTag("star_bright");

		display=0;
		multy=.70f;

		update=false;
	}

	void UpdatePage (int i, int num, int type) {
		rend1 =starsbright[i].GetComponent<Renderer>();
		rend2 =starsbright[i+1].GetComponent<Renderer>();
		rend3 =starsbright[i+2].GetComponent<Renderer>();
		if (type==0) {
			if (num>=50) {
				rend1.enabled = true;
			}
			if (num>=200) {
				rend2.enabled = true;
			}
			if (num>=500) {
				rend3.enabled = true;
			}
		}
		if (type==1) {
			if (num>=1) {
				rend1.enabled = true;
			}
			if (num>=2) {
				rend2.enabled = true;
			}
			if (num>=3) {
				rend3.enabled = true;
			}
		}

		if (type==2) {
			if (num>=2) {
				rend1.enabled = true;
			}
			if (num>=4) {
				rend2.enabled = true;
			}
			if (num>=6) {
				rend3.enabled = true;
			}
		}

		if (type==3) {
			if (num>=8) {
				rend1.enabled = true;
			}
			if (num>=16) {
				rend2.enabled = true;
			}
			if (num>=24) {
				rend3.enabled = true;
			}
		}

		if (type==4) {
			if (num>=1) {
				rend1.enabled = true;
			}
			if (num>=2) {
				rend2.enabled = true;
				rend3.enabled = true;
			}
		}




	}

	void Update () {
		int i,j,k;
		float l;

		if (update) {
			UpdatePage(0,AchievementMap.enemyunits_Air,0);
			UpdatePage(1*3,AchievementMap.secondary_kills,0);
			UpdatePage(2*3,AchievementMap.stages,1);
			UpdatePage(3*3,AchievementMap.bonuscompleted,4);
			UpdatePage(4*3,AchievementMap.secondaryreloads,0);
			UpdatePage(5*3,AchievementMap.powerups_count,0);
			UpdatePage(6*3,AchievementMap.enemyunits_Terrain,0);
			UpdatePage(7*3,AchievementMap.allies_protected,0);
			UpdatePage(8*3,AchievementMap.achievements,3);
			update=false;

		}
		if (display==0) {
			display++;

			for (i=0;i!=27;i++) {  
		        rend1 =starsbright[i].GetComponent<Renderer>();
		        rend1.enabled = false;
			}

	    	for (i=0;i!=9;i++) {  
	    		wings[i].SetActive(true);
	        	wings[i].transform.Translate(startx,starty-(i*multy),0, Space.World);
	    	}
			i=0;
			for (k=0;k!=9;k++) {  
				for (j=0;j!=3;j++) {  
					if (j==1)
						l=.06f;
					else 
						l=0;
	    	      starsbright[i].transform.Translate(startx1+(j*.3f),starty1-(k*multy)+l,0, Space.World);
			      stars[i].transform.Translate(startx1+(j*.3f),starty1-(k*multy)+l,0, Space.World);
					i++;
				}
			}
		}
    }
}


