using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SettingsExit : MonoBehaviour {

    public Renderer rend;

	void OnMouseUp () {
		if (Settings.active==true) {
			GM.Exiting=true;
			GM.scene="CampaignMap";
		}
	}


	// Use this for initialization
	void Start () {

		rend = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Settings.active==true) {
			rend.enabled = true;
		}
		else {

			rend.enabled = false;
		}
	}
}
