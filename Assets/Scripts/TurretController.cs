﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour {

	public GameObject EmpUI;
	public float timeToSpawnEffect;
	public float fireRate;

	public ObjectPool bulletPool;
	public ObjectPool empPool;

	Transform firePoint;

	public float EmpTime;
	bool empReady = true;
	float EmpTimer;

	public float gunTime;
	bool gunReady = true;
	float gunTimer;


	// Use this for initialization
	void Start () {
		bulletPool = GetComponent<ObjectPool> ();
		firePoint = transform.FindChild ("FirePoint");

		gunTimer = gunTime;
		EmpTimer = EmpTime;

	}
	
	// Update is called once per frame
	void Update () {

		if (!empReady) {
			EmpTimer -= Time.deltaTime;

			if (EmpTimer <= 0f) {
				EmpUI.SetActive (false);
				empReady = true;
				EmpTimer = EmpTime;
			}

		}


		if (!gunReady) {
			gunTimer -= Time.deltaTime;

			if (gunTimer <= 0f) {
				gunReady = true;
				gunTimer = gunTime;
			}

		}


		if(GM.button_pressed==2){
			Shoot ();
			GM.button_pressed = 0;
		}

		if(GM.button_pressed==1){
			ShootEmp();
			EmpUI.SetActive (true);
			GM.button_pressed=0;
		}
	}

	public void Shoot () {

		if(gunReady)
			Effect ();

		   gunReady = false;
	}

	public void ShootEmp() {

		if (empReady) {
			EmpEffect ();

			empReady = false;
		}
	}



	void Effect () {
		GameObject obj = bulletPool.GetPooledObject();

		if (obj == null) {
			return;
		}
		//obj.transform.parent = this.transform;

		obj.transform.position = firePoint.position;
		obj.transform.rotation = firePoint.rotation;
		obj.SetActive (true);


		foreach (Transform child in obj.transform) {
			child.gameObject.SetActive (true);
		}


	}

	void EmpEffect () {
		GameObject obj = empPool.GetPooledObject();

		if (obj == null) {
			return;
		}
		//obj.transform.parent = this.transform;

		obj.transform.position = firePoint.position;
		obj.transform.rotation = firePoint.rotation;
		obj.SetActive (true);


		foreach (Transform child in obj.transform) {
			child.gameObject.SetActive (true);
		}


	}






}
