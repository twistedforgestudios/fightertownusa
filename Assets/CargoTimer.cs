﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CargoTimer : MonoBehaviour {

	public PlayerGameData playerData;
	public bool CargoReady;

	public NumberUI secondsUI;
	public NumberUI minutesUI;


	public float TimeLimit;
	public bool timesUp;
	public string eventName;
	public string message;

	float timer;
	int seconds;
	int  minutes;

	public int cargoLimit;

	// Use this for initialization

	void OnEnable(){


	}

	void Start () {
		timer = TimeLimit;

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


		if (timesUp) {
			CargoReady = true;

		} else {
			CargoReady = false;

		}
	
	}

	// Update is called once per frame
	void Update () {



		if(!timesUp){
			timer -= Time.deltaTime;

			if(timer <=0.0f){
				timesUp = true;
				CargoReady = true;
				//cargoButton.interactable = true;


				if(eventName != "")
					EventManager.TriggerEvent(eventName);

				if (message != "")
					SendMessage (message);

			}
		}

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;


		if (timesUp) {
			CargoReady = true;
			secondsUI.gameObject.SetActive (false);
			//Debug.Log("Timer off");
		} else {
			CargoReady = false;
			secondsUI.gameObject.SetActive (true);
			//Debug.Log("Timer on");
		}


		playerData.TimeUntilCargoReplenish = timer;

	}

	public float GetTimeLeft(){
		return timer;
	}

	void DisableTimer(){
		timesUp = true;
	}




	void OnDisable(){
		timesUp = true;
		timer = TimeLimit;
	}


	public void RestartTimer(){
		timer = TimeLimit;

	}

	public void ReplenishCargoUses(){
		playerData.CargoBoxUses = cargoLimit;
	}


	public void RestartTimer(float time){
		TimeLimit = time;
		timer = TimeLimit;


		timesUp = false;
	}


	public void StartCargoTimer(float time){
	     
			RestartTimer (time);

	}



	public void ReduceTime(float seconds){
		TimeLimit -= seconds;
		//Debug.Log("Time reduced by " + seconds.ToString());

		if (TimeLimit <= 0) {
			TimeLimit = 0;
			CargoReady = true;
		}
	}

	public void UseCargoBox(){

		if (timesUp) {

			playerData.CargoBoxUses--;
			Debug.Log ("Cargo Box used");
			playerData.CargoBoxes--;

			playerData.CargoBoxes = Mathf.Clamp(playerData.CargoBoxes, 0,20) ;

			playerData.CargoBoxUses = Mathf.Clamp(playerData.CargoBoxUses, 0,20) ;


			if (playerData.CargoBoxUses <= 0) {
				CargoReady = false;
				StartCargoTimer (120f);

			}

		}
	}
		

}
