using UnityEngine;
using System.Collections;

public class Explode1 : MonoBehaviour {

public static ParticleSystem ps2;
	void Start () {
		ps2 = GetComponent<ParticleSystem>();
		ps2.GetComponent<Renderer>().sortingLayerName = "front";
	}
	
	// Update is called once per frame
	void Update () {
		if (GM.explodetype==0) {
			if (GM.explodeflag==true){
				transform.position = GM.planepos;
				Vector3 temp = GM.planepos; // copy to an auxiliary variable...
		    	temp.y-=3;
				transform.position=temp;
				ps2.Play();
				GM.explodeflag=false;
			}
		}
	}
}
