﻿using UnityEngine;
using System.Collections;

public class ProximityObject : MonoBehaviour {

	public GameObject obj;
	public string targetTag;
	public bool useTag;
	public float activeTime;

	public bool OnlyOnce;

	float activeTimer;
	bool isActive;
	Collider2D col2D;
	public bool active;
	// Use this for initialization
	void Start () {
		col2D = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D col){

		if (useTag) {
			if (col.tag == targetTag) {
				obj.SetActive(active);
			}
		} else {
			obj.SetActive(active);
		}

		if (OnlyOnce) {
			col2D.enabled = false;
		}

	}


	void OnCollisionEnter2D(Collision2D other){
//		Debug.Log ("Collided");
		if (useTag) {
			if (other.gameObject.tag == targetTag) {
				obj.SetActive (active);
			}
		} else {
			obj.SetActive (active);
		}

	}



}
