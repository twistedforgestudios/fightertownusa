﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof(AudioSource))]
public class EventSound : MonoBehaviour {

	private AudioSource sound;
	public string eventName;
	// Use this for initialization
	void Start () {
		sound = GetComponent<AudioSource> ();


		if(sound !=null && !sound.isPlaying)
			EventManager.StartListening (eventName,PlaySound);

	}


	void PlaySound(){

		if(!sound.isPlaying){
			sound.Play ();
		}
	}

	public void Pause(){
		sound.Pause();
	}

	public void Play(){
		sound.Play ();
	}

	
	
}
