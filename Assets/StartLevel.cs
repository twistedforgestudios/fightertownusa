﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartLevel : MonoBehaviour {
	public PlayerGameData playerdata;
	public Button startButton;
	public GameObject levelManager;

	// Use this for initialization
	void Start () {

		startButton.onClick.AddListener (() => levelManager.SendMessage ("LoadLevelAsync"));

	}
	
	// Update is called once per frame
	void Update () {




	}


	public bool CheckIfEnoughFuelCells(){

		if (playerdata.NumOfFuelCells > 0) {
			return true;
		}

		return false;
	}


}
