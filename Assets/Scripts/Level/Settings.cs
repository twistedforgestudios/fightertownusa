using UnityEngine;
using System.Collections;


public class Settings : MonoBehaviour {

	public float screenwoffset;
	public float screenhoffset;
	public float heightoffset;
	public float widthoffset;
    public float offset;


	private Vector3 coords;

	public GUIStyle customButton;
	public GUIStyle customButton0;
	public GUIStyle customButton1;
	public GUIStyle customButton2;

	public GUIStyle ThumbSlider;

    public float hSliderValue1;
    public float hSliderValue2;
    public float hSliderValuelast1;
    public float hSliderValuelast2;
    public float musicvolumechangevaluelast;
    public float soundvolumechangevaluelast;


	static public bool musicstart;

	static public bool active;



    private string[] music = {"Give Away", "Causeway", "Give Away"};//add the rest
    int n,i,wichsong;
    private Vector2 scrollViewVector = Vector2.zero;


	void Start () {

		MM.musicvolumechangevalue = PlayerPrefs.GetFloat("musicvolumechangevalue");
		MM.musicnewsong = PlayerPrefs.GetInt("musicnewsong");
		MM.soundvolumechangevalue = PlayerPrefs.GetFloat("soundvolumechangevalue");
		soundvolumechangevaluelast=MM.soundvolumechangevalue;

		MM.musicvolumechange=true;
		MM.soundvolumechange=false;
		musicvolumechangevaluelast=-1;

		hSliderValue1=MM.musicvolumechangevalue*10;
		hSliderValue2=MM.soundvolumechangevalue*10;

		screenwoffset=Screen.width;
		screenhoffset=Screen.height;  
		heightoffset=387;
		widthoffset=765;
		offset=screenhoffset / heightoffset;

		coords = Camera.main.WorldToScreenPoint(transform.position);
		Fade.fadeinstart=true;

		hSliderValuelast1=-1;
		hSliderValuelast2=-1;
    	musicstart=false;
		wichsong=0;
		
		active=true;
    	//active=false;
    	Help.active=false;
   // 	Help.active=true;
    	Credits.active=false;
   // 	Credits.active=true;

  //  	MM.musicstop=1;

	}


private void OnGUI () {

	if (active) {
		customButton.fontSize = Mathf.RoundToInt (20f * offset);
		customButton0.fontSize = Mathf.RoundToInt (14f * offset);
		customButton1.fontSize = Mathf.RoundToInt (14f * offset);
		customButton2.fontSize = Mathf.RoundToInt (14f * offset);

		//GUI.Label(new Rect(coords.x-130*offset, screenhoffset-coords.y-(160*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), "Fighter Town USA", customButton);
		//GUI.Label(new Rect(coords.x-130*offset, screenhoffset-coords.y-(140*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), "Stage 1", customButton);


		if(GUI.Button(new Rect(coords.x-80 * offset,90 * offset,25 * offset,25 * offset), "")) {
			if(n==0)
				n=1;
			else 
				n=0;        
		}
		if(n==1){
			/*	
		  scrollViewVector = GUI.BeginScrollView (new Rect (coords.x-190 * offset, 90 * offset, 100 * offset, 115 * offset), scrollViewVector, new Rect (0, 0, 300, 500));

		  for(i=0;i<3;i++) {
			 // if (GUI.Button(new Rect(coords.x-180 * offset,i*25 +100 * offset,100 * offset,25 * offset), "")){
			  if (GUI.Button(new Rect(0,i*25 * offset,300 * offset,25 * offset), "")){
				  n=0;
				  wichsong=i; 

				  MM.musicstop=3;
				  MM.musicnewsong=wichsong;

				  PlayerPrefs.SetInt("musicnewsong", MM.musicnewsong);	 

			  }              
			  GUI.Label(new Rect(5 * offset,i*25 * offset,300 * offset,25 * offset), music[i],customButton1);           
		  }
		  GUI.EndScrollView();   
*/
		}
		else
			GUI.Label(new Rect(coords.x-180 * offset,98 * offset,300 * offset,125 * offset), music[MM.musicnewsong],customButton2);

		GUI.Label(new Rect(coords.x-200*offset, screenhoffset-coords.y-(90*offset), 1, 300*offset), "Music Selection", customButton0);
		GUI.Label(new Rect(coords.x-200*offset, screenhoffset-coords.y-(40*offset), 1, 300*offset), "Music Volume", customButton0);
		GUI.Label(new Rect(coords.x-200*offset, screenhoffset-coords.y-(0*offset), 1, 300*offset), "Sounds Volume", customButton0);


			hSliderValue1 = GUI.HorizontalSlider (new Rect (coords.x-80*offset, screenhoffset-coords.y-(35*offset), 200*offset,10*offset), (int)hSliderValue1, 0.0f, 10.0f,customButton,ThumbSlider);
			hSliderValue2 = GUI.HorizontalSlider (new Rect (coords.x-80*offset, screenhoffset-coords.y+(5*offset), 200*offset, 10*offset), (int)hSliderValue2, 0.0f, 10.0f,customButton,ThumbSlider);

		if (hSliderValuelast1!=hSliderValue1) {
			MM.musicvolumechange=true;
			MM.musicvolumechangevalue=hSliderValue1/10;
			hSliderValuelast1=hSliderValue1;

		}
		if (hSliderValuelast2!=hSliderValue2) {
			MM.soundvolumechangevalue=hSliderValue2/10;
			hSliderValuelast2=hSliderValue2;
		}
	}

}
	void Update () {

		if (musicstart) {
			musicstart=false;
			MM.musicplay=0;
		}
		if (MM.musicvolumechangevalue!=musicvolumechangevaluelast) {
			PlayerPrefs.SetFloat("musicvolumechangevalue", MM.musicvolumechangevalue);	 
			musicvolumechangevaluelast=MM.musicvolumechangevalue;
			hSliderValue1=MM.musicvolumechangevalue*10;
		}
		if (MM.soundvolumechangevalue!=soundvolumechangevaluelast) {
			MM.soundvolumechange=true;
			PlayerPrefs.SetFloat("soundvolumechangevalue", MM.soundvolumechangevalue);	 
			soundvolumechangevaluelast=MM.soundvolumechangevalue;
	    	hSliderValue2=MM.soundvolumechangevalue*10;
		}

	}
}
