﻿using UnityEngine;
using System.Collections;

public class TimedEvent : MonoBehaviour {

	public float secondsTillEvent;

	public string eventToTrigger;

	public float timeLeft;

	public bool TimerActive = true;

	// Use this for initialization
	void Start () {
		timeLeft = secondsTillEvent;
	}

	void Update(){

		if (TimerActive) {
			timeLeft -= Time.deltaTime;


			if (timeLeft <= 0.0f) {
				TimerActive = false;
				enabled = false;
				EventManager.TriggerEvent (eventToTrigger);
			}
		}


	}

	public void ResetTimer(){
		timeLeft = secondsTillEvent;
	}

	public void StopTimer(){
		TimerActive = false;
	}

	public void StartTimer(){
		TimerActive = true;
	}

}
