﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CampaignLevelButtonUI : MonoBehaviour {

	public Sprite levelUnfinished;
	public Sprite LevelFinished;
	public Sprite noDiamonComplete;
	public Sprite oneDiamondComplete;
	public Sprite twoDiamondComplete;
	public Sprite threeDiamondComplete;

	public RectTransform DisplayPrompt;

	public LevelGameData levelData;

	public StartLevel startButton;


	private Button button;
	private Image levelIcon;
	public Image StarsIcon;
	// Use this for initialization
	void Start () {
		button = GetComponent<Button> ();
		levelIcon = GetComponent<Image> ();

		if (!levelData.Unlocked) {
			levelIcon.sprite = levelUnfinished;
			StarsIcon.sprite = noDiamonComplete;
			button.interactable = false;
		}
		else if (levelData.Completed) {


			levelIcon.sprite = LevelFinished;
		
			if (StarsIcon != null) {
				if (levelData.YellowDiamondsColleted == 0) {
					StarsIcon.sprite = noDiamonComplete;
				} else if (levelData.YellowDiamondsColleted == 1) {
					StarsIcon.sprite = oneDiamondComplete;
				} else if (levelData.YellowDiamondsColleted == 2) {
					StarsIcon.sprite = twoDiamondComplete;
				} else if (levelData.YellowDiamondsColleted == 3) {
					StarsIcon.sprite = threeDiamondComplete;
				}
			}

		} 
		else {
			levelIcon.sprite = levelUnfinished;

			if(StarsIcon != null)
				StarsIcon.sprite = noDiamonComplete;
		}

	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void SelectLevel(){

		if (startButton.CheckIfEnoughFuelCells()) {

			startButton.GetComponent<Button> ().interactable = true;
			DisplayPrompt.gameObject.SetActive (false);
		} else {
			startButton.GetComponent<Button> ().interactable = false;
			DisplayPrompt.gameObject.SetActive (true);
		}


	}




}
