using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
		public float speed;
		private Vector3 newVector;

        private void LateUpdate()
        {
		   AddPosition ();
          // transform.position = target.position + offset;
			transform.position += (newVector * Time.deltaTime * speed);
        }


		private void AddPosition(){

			newVector = target.position - transform.position;
			

		}

		public void SlowDown(){
			speed -= 0.01f;
		}

    }
}
