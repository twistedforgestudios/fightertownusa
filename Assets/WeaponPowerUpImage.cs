﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPowerUpImage : MonoBehaviour {


	public PlayerGameData playerdata;

	public Sprite[] weaponsPowerupSprites;
	private Image weaponPowerImage;
	// Use this for initialization
	void Start () {
		weaponPowerImage = GetComponent<Image> ();

		switch (playerdata.StartingWeaponPower) {

		case PlayerPowerState.WeaponPower.FullAuto:
			weaponPowerImage.sprite = weaponsPowerupSprites [0];
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			weaponPowerImage.sprite = weaponsPowerupSprites [1];
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			weaponPowerImage.sprite = weaponsPowerupSprites [2];
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			weaponPowerImage.sprite = weaponsPowerupSprites [3];
			break;
		default:
			break;
		}



	}
	
	// Update is called once per frame
	void Update () {
	





	}



	public  void SetCurWeaponImage(){

		switch (playerdata.StartingWeaponPower) {

		case PlayerPowerState.WeaponPower.FullAuto:
			weaponPowerImage.sprite = weaponsPowerupSprites [0];
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			weaponPowerImage.sprite = weaponsPowerupSprites [1];
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			weaponPowerImage.sprite = weaponsPowerupSprites [2];
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			weaponPowerImage.sprite = weaponsPowerupSprites [3];
			break;
		default:
			break;
		}




	}


}
