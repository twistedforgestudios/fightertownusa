﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FighterPromoCode : MonoBehaviour {

	public PlayerGameData playerData;

	public dreamloPromoCode promocode;

	public InputField  codeInput;


	public string okEventName;
	public string errorEventName;

	public string userEntryPromocode;


	public RectTransform RewardPrompt;

	public RectTransform errorPrompt;

	public string promocodeValue;



	// Use this for initialization
	void Start () {
	

		EventManager.StartListening (okEventName, RewardPromoPowerUP);
		EventManager.StartListening (errorEventName, ErrorPromoPowerUP);

	}
	
	// Update is called once per frame
	void Update () {
	

		userEntryPromocode = codeInput.text;
	}

	public void EnterPromoCode(){

		promocode.RedeemCode (codeInput.text);
	}

	public void RewardPromoPowerUP(){

		if (promocode.value == promocodeValue) {
			Debug.Log ("You got Cool Item 1");
			RewardPrompt.gameObject.SetActive (true);
		} else {
			Debug.Log ("There was a problem");
			errorPrompt.gameObject.SetActive (true);
		}


	}

	public void ErrorPromoPowerUP(){


			Debug.Log ("There was a problem");
		errorPrompt.gameObject.SetActive (true);


	}





}
