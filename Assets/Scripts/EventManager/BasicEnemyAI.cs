﻿using UnityEngine;
using System.Collections;

public class BasicEnemyAI : MonoBehaviour {

	public BasicGun gun;
	public Animator HealthUIAnim;
	public FaceTowardObject faceTowardScr;
	public ForwardMovement ForwardMoveScr;
	public WaveMovement waveMoveScr;
	//public ProximityObject
	private bool dead = false;
	public bool goalEnemy;
	public float shootTimer;
	AudioSource deathsound;
	private Animator anim;
	private Collider2D col;
	private ParticleSystem ps;
	//private Emi
	private Health health;

	public bool autoShoot;
	public bool SeesEnemy = false;

	public bool KilledBySecondary;
	public SpriteRenderer Head;

	void OnEnable(){
		dead = false;
		col = GetComponent<Collider2D> ();
		col.enabled = true;


		//if (gun.gameObject != null) {
		//	gun.gameObject.SetActive (false);
		//}


		if(autoShoot)
			InvokeRepeating("Shoot", shootTimer,shootTimer);

		ps = GetComponent<ParticleSystem> ();
		ps.Stop ();

		if (waveMoveScr != null) {
			waveMoveScr.enabled = true;
		}

	}



	// Use this for initialization
	void Start () {
		
		health = GetComponent<Health> ();

		anim = GetComponent<Animator> ();
		deathsound = GetComponent<AudioSource> ();
		col = GetComponent<Collider2D> ();
		ps = GetComponent<ParticleSystem> ();


		//ps.Simulate(ps.duration);
		if (faceTowardScr != null)
			faceTowardScr.enabled = true;
		if (ForwardMoveScr!= null)
			ForwardMoveScr.enabled = true;
		if (waveMoveScr != null) {
			waveMoveScr.enabled = true;
		}

		col.enabled = true;

		if(autoShoot)
			InvokeRepeating("Shoot", 1.0f,1.0f);



	}

	public bool ContainsParam(Animator _Anim, string _ParamName)
	{
		foreach (AnimatorControllerParameter param in _Anim.parameters)
		{
			if (param.name == _ParamName) return true;
		}
		return false;
	}

	// Update is called once per frame
	void Update () {

		if(SeesEnemy){
			if(anim != null){
				if(ContainsParam(anim,"SeesEnemy"))
					anim.SetBool("SeesEnemy",true);

			}
		}

		if (!health.isAlive && !dead) {

			if (KilledBySecondary) {
				EventManager.TriggerEvent ("SecondaryKill");
			}


			if (faceTowardScr != null)
				faceTowardScr.enabled = false;
			
			if (ForwardMoveScr!= null)
				ForwardMoveScr.enabled = false;
			
			if (waveMoveScr != null) {
				waveMoveScr.enabled = false;
			}

			CancelInvoke ();
			if (goalEnemy) {
				if (LayerMask.LayerToName (gameObject.layer) == "enemy") {
					EventManager.TriggerEvent ("KilledAirEnemy");
				} else if (LayerMask.LayerToName (gameObject.layer) == "groundEnemy") {
					EventManager.TriggerEvent ("KilledGroundEnemy");
				}
			}
			StartCoroutine ("DeathAnimateCo");
			dead = true;
			col.enabled = false;

			if(Head != null){
				Head.enabled = false;
			}

			if (transform.childCount >= 0) {
				foreach (Transform t in transform.GetComponentInChildren<Transform>()) {
					t.gameObject.SetActive (true);
				}
			}



		}
	}

	void Shoot(){
		if (gun != null) {
				gun.Shoot ();
		}
	}
		
	void OnDisable(){
		
		CancelInvoke ();
		//if(anim != null)
		//	anim.SetBool("Dead", false);
		
	}


	void DeathAnimate(){
	//	if(anim != null)
	//		anim.SetBool("Dead", true);
		
		if (!deathsound.isPlaying) {
			deathsound.Play ();
		}
	}


	IEnumerator DeathAnimateCo(){
		//if(anim != null)
		//	anim.SetBool("Dead", true);
		//ps.Simulate(0.5f,false,true);

		ps.Play ();

		//Debug.Log ("Dead");
		if (!deathsound.isPlaying) {
			deathsound.Play ();
		}
//		SendMessage ("SpawnItem");
		yield return new WaitForSeconds(deathsound.clip.length);
	}

	void OnTriggerEnter2D(Collider2D col){
		//Debug.Log("Sees Enemy");
		InvokeRepeating("Shoot", shootTimer,shootTimer);
		SeesEnemy = true;


		if (col.gameObject.layer == LayerMask.NameToLayer("playerbullet")){
			if(HealthUIAnim !=null)
				HealthUIAnim.SetTrigger ("Damage");
		}


		if (col.tag == "SecondaryWeapon") {
			KilledBySecondary = true;
		} else {
			KilledBySecondary = false;
		}

	}


}
