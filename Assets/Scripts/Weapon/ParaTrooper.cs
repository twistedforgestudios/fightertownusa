﻿using UnityEngine;
using System.Collections;

public class ParaTrooper : MonoBehaviour {

	public bool isFalling;
	public bool isGrounded;
	public bool isDead;
	//private SpriteRenderer sprite;
	private Rigidbody2D rgd;
	private Health health;

	void OnEnable(){
		//sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		rgd.isKinematic = false;
		isDead = false;
	}



	// Use this for initialization
	void Start () {
		//sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		health = GetComponent<Health> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!health.isAlive) {
			rgd.isKinematic = false;
		}

	}



	void OnTriggerEnter2D(Collider2D other){

		rgd.isKinematic = true;

	}


	void OnCollisionEnter2D(Collision2D col){


			rgd.isKinematic = true;


	}



	void OnDisable(){
		rgd.isKinematic = false;
	}


	void OnParatrooperDeath(){
		EventManager.TriggerEvent ("ScientistDied");
		Debug.Log ("Sceintist Dies");
		isDead = true;
		rgd.isKinematic = false;
	}

}
