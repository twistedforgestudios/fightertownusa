﻿using UnityEngine;
using System.Collections;

public class ItemSpawner : MonoBehaviour {

	public bool UseRandom;

	public GameObject[] Item;



	public int[] itemChance;

	public int randomNum;

	public int chanceOfNull;

	public int index;

	int range;
	int temp;

//	int[] weightArray;

	GameObject setItem;

	// Use this for initialization
	void Start () {
	//	weightArray = new int[Item.Length];
		setItem = Item [index];
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void RandomSelection(){
		
		//add weights
		foreach(int i  in itemChance) {
			range += i;
		}

		randomNum = Random.Range (0, range);

		temp = 0;

		for (int i = 0; i < itemChance.Length; i++) {
			if (randomNum <= (temp + itemChance [i])) {
				//set item to spawn
				setItem = Item[i];
				break;
			} else {
				temp += itemChance [i];
			}

		}

	}



	public void SpawnItem(){

		if(UseRandom)
			RandomSelection ();

		if(setItem != null)
			Instantiate (setItem, transform.position, transform.rotation);
	

	}
}
