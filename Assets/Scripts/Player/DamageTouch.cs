﻿using UnityEngine;
using System.Collections;

public class DamageTouch : MonoBehaviour {


	public int Damage;
	public string EventTriggerName;
	public bool Inside;

	void Start () {
		//disableTimer = DisableTime;

	}
	
	// Update is called once per frame
	void Update () {
	
	}



	void OnTriggerEnter2D(Collider2D other){

		Health health = other.gameObject.GetComponent<Health> ();

		if (health == null ) {
			return;
		} else {
			//if (!GM.intunnel)
				health.Damage (Damage);
			//else {
			if (EventTriggerName != null) {
				EventManager.TriggerEvent (EventTriggerName);
			}
			//	if (Inside) {
			//		health.Damage (Damage);
			//	}

			//}

		}
	}


	void OnTriggerStay2D(Collider2D other){

		Health health = other.gameObject.GetComponent<Health> ();

		if (health == null ) {
			return;
		} else {
			//if (!GM.intunnel)
			health.Damage (Damage);
			//else {

			//	if (Inside) {
			//		health.Damage (Damage);
			//	}

			//}

		}
	}



}
