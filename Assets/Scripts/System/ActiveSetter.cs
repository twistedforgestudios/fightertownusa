﻿using UnityEngine;
using System.Collections;

public class ActiveSetter : MonoBehaviour {


	public GameObject gObject;

	public bool setBool;

	public void SetObject(){
		//Debug.Log ("Setting...");
		if (gObject != null)
			gObject.SetActive (setBool);
	}

	public void SetOtherObject(){
		//Debug.Log ("Setting...");
		if (gObject != null)
			gObject.SetActive (setBool);
	}
}
