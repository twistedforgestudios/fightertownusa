﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	private ObjectPool enemyPool;

	public bool unactive;
	public bool randomSpawning;

	public float timeTilSpawn;
	public float spawnDelay;
	public int randomDistance;

	//public bool inheritRotation;

	void OnEnable(){

		enemyPool = GetComponent<ObjectPool> ();


		if (randomSpawning)
			InvokeRepeating ("RandomlySpawnEnemy", timeTilSpawn, spawnDelay);
		else
			InvokeRepeating ("SpawnEnemy", timeTilSpawn, spawnDelay);
		
	}

	// Use this for initialization
	void Start () {
		enemyPool = GetComponent<ObjectPool> ();


		CancelInvoke ();
		if (randomSpawning)
			InvokeRepeating ("RandomlySpawnEnemy", timeTilSpawn, spawnDelay);
		else
			InvokeRepeating ("SpawnEnemy", timeTilSpawn, spawnDelay);

		if (unactive)
			CancelInvoke ();

	}

	// Update is called once per frame
	void Update () {
		
	}

	void SpawnEnemy(){

		GameObject obj = enemyPool.GetPooledObject ();

		if (obj == null)
			return;
		
		obj.transform.position = this.transform.position;
		//obj.transform.rotation = this.transform.rotation;
		obj.SetActive (true);
	}


	void RandomlySpawnEnemy(){
		
		GameObject obj = enemyPool.GetPooledObject ();

		if (obj == null)
			return;

		int x = Random.Range (-1, 2) * randomDistance;
		int y = Random.Range (-1, 2) * randomDistance;

		obj.transform.position = this.transform.position + new Vector3 (x, y);;

		//if(inheritRotation)
		//	obj.transform.rotation = this.transform.rotation;
		
		obj.SetActive (true);



	}

	void OnDisable(){
		CancelInvoke ();
	}



}
