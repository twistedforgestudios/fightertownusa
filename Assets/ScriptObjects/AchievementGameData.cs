﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "Game/AchievementData", order = 1)]
public class AchievementGameData : ScriptableObject {

	public enum AchievementType { DestroyAircraft, DestroyGroundTroops, AcquirePiece,CollectPower, CollectSecondary,ProtectUnits,CollectAcheivements }


	public AchievementType acheivementType;
	public string Title;
	public bool Acquired;
	public int Reward;
	public int Tier;
	public int Requirements;

}
