﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondaryFireTrigger : MonoBehaviour {

	public SecondaryWeapon secWeapon;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void OnTriggerEnter2D(Collider2D col){

		secWeapon = col.transform.FindChild ("SecondaryWeaponAlt").GetComponent<SecondaryWeapon> ();

		secWeapon.FireSecondaryWeaponAlt ();

	}


	void OnCollisionEnter2D(Collision2D other){




	}



}
