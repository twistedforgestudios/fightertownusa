﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Mission01Explain : MonoBehaviour {

	public RectTransform missionblurp;
	public GM gameManager;
	public bool activated;



	void OnEnable(){
		if (!activated) {
			missionblurp.gameObject.SetActive (true);
			gameManager.Pause ();
			activated = true;
		}

	}

}
