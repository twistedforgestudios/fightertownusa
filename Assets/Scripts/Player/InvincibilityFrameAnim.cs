﻿using UnityEngine;
using System.Collections;

public class InvincibilityFrameAnim : MonoBehaviour {

	public Health health;

	public Color inviciColor;

	public SpriteRenderer render;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (health.invulnerble) {
			render.color = inviciColor;
		} else {
			render.color = Color.white;
		}
	}
}
