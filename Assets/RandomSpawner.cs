﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawner : MonoBehaviour {

	[SerializeField]
	private ObjectPool[] goalPool;

	public bool unactive;
	public bool randomSpawning;

	public float timeTilSpawn;
	public float spawnDelay;
	public int randomDistance;
	public PushPosition push;

	void OnEnable(){


		if (randomSpawning)
			InvokeRepeating ("RandomlySpawnGoal", timeTilSpawn, spawnDelay);
		else
			InvokeRepeating ("SpawnGoal", timeTilSpawn, spawnDelay);

	}


	// Use this for initialization
	void Start () {

		CancelInvoke ();
		if (randomSpawning)
			InvokeRepeating ("RandomlySpawnGoal", timeTilSpawn, spawnDelay);
		else
			InvokeRepeating ("SpawnGoal", timeTilSpawn, spawnDelay);

		if (unactive)
			CancelInvoke ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	void SpawnGoal(){

		int r = Random.Range (0, goalPool.Length);


		GameObject obj = goalPool[r].GetPooledObject ();

		Debug.Log ("Spawned pool #" + r.ToString ());



		if (obj == null)
			return;

		obj.transform.position = this.transform.position;
		//obj.transform.rotation = this.transform.rotation;
		obj.SetActive (true);
	}


	void RandomlySpawnGoal(){

		GameObject obj = goalPool[Random.Range(0,goalPool.Length)].GetPooledObject ();

		if (obj == null)
			return;

		int x = Random.Range (-1, 2) * randomDistance;
		int y = Random.Range (-1, 2) * randomDistance;

		obj.transform.position = this.transform.position + new Vector3 (x, y);;

		//if(inheritRotation)
		//	obj.transform.rotation = this.transform.rotation;

		obj.SetActive (true);



	}


	void OnDisable(){
		CancelInvoke ();
	}




}
