﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePlay : MonoBehaviour {

	public AudioSource[] dialogueList;

	public float TutorialAppearTime;
	public float TutorialAppearDelay;

	public bool dialogueStarted;
	public bool dialoguePaused;
	public bool dialogueEnded;
	public bool currentClipFinished = true;

	[SerializeField]
	private float tutorialTimer;
	[SerializeField]
	private float delayTimer;
	[SerializeField]
	private int currentDialogue = 0;

	private AudioSource currentplayingSound;

	// Use this for initialization
	void Start () {
		tutorialTimer = TutorialAppearTime;
		delayTimer = TutorialAppearDelay;


	}
	
	// Update is called once per frame
	void Update () {


		if (!dialogueStarted && !dialoguePaused) {

			tutorialTimer -= Time.deltaTime;

			if (tutorialTimer <= 0.0f) {
				dialogueStarted = true;


				if (currentDialogue > 1) {
					dialogueList [currentDialogue - 1].Pause ();
				}


				if (currentDialogue < dialogueList.Length+1) {

					if(currentDialogue < dialogueList.Length){
						currentClipFinished = false;
						dialogueList[currentDialogue].Play();
						currentplayingSound = dialogueList [currentDialogue];

						if(dialogueList[currentDialogue].clip != null){
							delayTimer  = currentplayingSound.clip.length + TutorialAppearDelay;
						}
						else{
							delayTimer  = TutorialAppearDelay;
						}

					}

					currentDialogue++;
				}


				if (dialogueList.Length < currentDialogue) {
					dialogueEnded = true;

				}

			}
		}


		if (dialogueStarted && !dialoguePaused) {

			delayTimer -= Time.deltaTime;

			if(TutorialAppearDelay > delayTimer){
				currentClipFinished = true;
			}


			if (delayTimer <= 0.0f) {
				dialogueStarted = false;
			

				if (dialogueList.Length == currentDialogue) {
					dialogueStarted = true;
					dialogueEnded = true;
					dialoguePaused = true;
					Debug.Log("tutorialStopped");

				}


			}
		}




	}



	public void PauseCurrentSound(){

		if (currentplayingSound != null) {
			currentplayingSound.Pause ();
			dialoguePaused = true;
		}
	

	}

	public void UnpauseCurrentSound(){

		if (currentplayingSound != null && !dialogueEnded && !currentClipFinished)
				currentplayingSound.Play();


			dialoguePaused = false;
		}

}
