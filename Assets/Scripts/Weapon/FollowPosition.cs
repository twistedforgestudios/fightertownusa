﻿using UnityEngine;
using System.Collections;

public class FollowPosition : MonoBehaviour {

	public Transform followTransform;
	public Vector3 offset;
	// Use this for initialization


	public bool ignoreX;
	public bool ignoreY;
	public bool ignoreZ;

	Vector3 followPos;
	Vector3 myPos;

	Transform myTransform;

	void Start () {
		myTransform = transform;

	}
	
	// Update is called once per frame
	void Update () {

		if (ignoreX && ignoreY && ignoreZ) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX && ignoreY) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, myTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreX && ignoreZ) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, followTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreY && ignoreZ) {
			myTransform.position = new Vector3 (followTransform.position.x + offset.x, myTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else if (ignoreX) {
			myTransform.position = new Vector3 (myTransform.position.x + offset.x, followTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreY) {
			myTransform.position = new Vector3 (followTransform.position.x + offset.x, myTransform.position.y + offset.y, followTransform.position.z + offset.z);
		} else if (ignoreZ) {
			myTransform.position = new Vector3 (followTransform.position.x + offset.x, followTransform.position.y + offset.y, myTransform.position.z + offset.z);
		} else {
			myTransform.position = new Vector3 (followTransform.position.x + offset.x, followTransform.position.y + offset.y, followTransform.position.z + offset.z);
		}






			
	}
}
