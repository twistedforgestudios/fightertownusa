using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollowNew : MonoBehaviour
    {
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_LastPosition;
        private Vector3 offset;
        private Vector3 test;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;

		public float backwards =0f;

        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
        }


        private void Update()
        {
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

			GM.planepos=target.position;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;
			//Debug.Log (xMoveDelta.ToString ());

            if (updateLookAheadTarget)
            {
				m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta) * Time.deltaTime;
				//Debug.Log ("updatelook ahead");
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
				//Debug.Log ("updatelook false");
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;

		    aheadTargetPos.x=aheadTargetPos.x+backwards;

            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);

            Vector3 temp = newPos; // copy to an auxiliary variable...

	    	if (PlatformerCharacter2D.cameraon==false) {
				temp.y = m_LastTargetPosition.y; // modify the component you want in the variable...
				transform.position = temp;
			}
			else {
				transform.position = temp;
				m_LastTargetPosition = target.position;
			}

        }

    }
}
