using UnityEngine;
using System.Collections;

public class Emp : MonoBehaviour {


	public static bool start;

	public static ParticleSystem ps;



	void Start () {
    	ps = GetComponent<ParticleSystem>();
		ps.GetComponent<Renderer>().sortingLayerName = "front";
		start=false;
	}

	void Update () {

		if (start) {
			transform.position = GM.planepos;
			Vector3 temp = GM.planepos; // copy to an auxiliary variable...
			temp.y-=10;
			temp.x+=50;
			transform.position=temp;
			start=false;
			ps.Play();
			EmpSprite.start=true;
		}
    }
}

