﻿using UnityEngine;
using System.Collections;

public class BGM : MonoBehaviour {

	AudioSource source;

	int oldmusic;
	public static BGM instance;
	void Awake()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
	}
		
	// Use this for initialization
	void Start () {
		Settings.musicstart=true;
		source = GetComponent<AudioSource> ();
		oldmusic = MM.musicnewsong;
		source.clip = MM.musicAudio [MM.musicnewsong];
		source.volume = MM.musicvolumechangevalue;
		source.Play ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (MM.musicnewsong != oldmusic) {
			source.clip = MM.musicAudio [MM.musicnewsong];
			oldmusic = MM.musicnewsong;
			source.volume = MM.musicvolumechangevalue;
			source.Play ();
		}

		if (MM.musicvolumechange) {
			source.volume = MM.musicvolumechangevalue;
		}


	}

	public void Play(){
		source.Play();

	}

	public void Pause(){

		source.Pause();
	}




}
