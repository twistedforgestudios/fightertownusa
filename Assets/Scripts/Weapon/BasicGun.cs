﻿using UnityEngine;
using System.Collections;

public class BasicGun : MonoBehaviour {

	float timeToSpawnEffect = 0;
	public float fireRate = 10;
	public bool tripleShootMode = false; 

	Transform firePoint;

	ObjectPool bulletPool;


	void Awake () {
		firePoint = transform.FindChild ("FirePoint");
		if (firePoint == null) {
			Debug.LogError ("No firePoint? WHAT?!");
		}
		bulletPool = GetComponent<ObjectPool> ();
	}

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		
	}
	public void Shoot () {
//		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		if (Time.time >= timeToSpawnEffect) {
			
			if (tripleShootMode) {
				FireTripleShot ();
			} else {
				Effect ();
			}
			timeToSpawnEffect = Time.time + 1/fireRate;

		}
	}
	void Effect () {
		GameObject obj = bulletPool.GetPooledObject();

		if (obj == null) {
			return;
		}

		obj.transform.position = firePoint.position;
		obj.transform.rotation = firePoint.rotation;
		obj.SetActive (true);


		foreach (Transform child in obj.transform) {
			child.gameObject.SetActive (true);
		}

		//if(obj.transform.GetChild(1) != null)
		//	obj.transform.GetChild(1).gameObject.SetActive (true);

	}

	void FireTripleShot(){

		//Debug.Log ("Firing Triple shot");
		GameObject obj1 = bulletPool.GetPooledObject ();

		if (obj1 == null) {
			return;
		}
		obj1.transform.position = firePoint.position;
		obj1.transform.rotation = firePoint.rotation;
		obj1.SetActive (true);

		GameObject obj2 = bulletPool.GetPooledObject ();

		if (obj2 == null) {
			return;
		}
		obj2.transform.position = firePoint.position + this.transform.TransformDirection(Vector3.up);
		obj2.transform.rotation = firePoint.rotation;
		obj2.SetActive (true);

		GameObject obj3 = bulletPool.GetPooledObject ();

		if (obj3 == null) {
			return;
		}

		obj3.transform.position = firePoint.position + this.transform.TransformDirection(Vector3.down);  ;
		obj3.transform.rotation = firePoint.rotation ;
		obj3.SetActive (true);

	}

}
