﻿using UnityEngine;
using System.Collections;


public class EventMessage : MonoBehaviour {


	public string Message;
	public string EventToListen;
	public string EventToTrigger;

	// Use this for initialization
	void Start () {
	
		if(EventToListen != "")
			EventManager.StartListening (EventToListen,TriggerMessage);

	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void TriggerMessage(){

		if(Message != "")
			this.SendMessage (Message);

		if (EventToTrigger != "") 
			EventManager.TriggerEvent (EventToTrigger);
		

	}
}
