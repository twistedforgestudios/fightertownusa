﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeMenu : MonoBehaviour {
	public PlayerGameData playerData;

	public enum Upgrades { Armor,Wings, Ammo, Heat };

	public Text armorLevel;
	public Text wingLevel;
	public Text ammoLevel;
	public Text heatLevel;
	public Text upgradesAmt;


	public Text AmountRequiredArmor;
	public Text AmountRequiredAmmo;
	public Text AmountRequiredHeat;
	public Text AmountRequiredWing;

	public Button armorUpgradeBtn;
	public Button ammoUpgradeBtn;
	public Button heatUpgradeBtn;
	public Button wingUpgradeBtn;

	public int[] levelRequirments;


	// Use this for initialization
	void Start () {

		armorUpgradeBtn.onClick.AddListener (() => Upgrade(Upgrades.Armor));
		ammoUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Ammo));
		heatUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Heat));
		wingUpgradeBtn.onClick.AddListener (() => Upgrade (Upgrades.Wings));

		AmountRequiredArmor.text = levelRequirments [playerData.ArmorLevel - 1].ToString ();
		AmountRequiredAmmo.text = levelRequirments [playerData.AmmoLevel - 1].ToString ();
		AmountRequiredHeat.text = levelRequirments [playerData.HeatLevel - 1].ToString ();
		AmountRequiredWing.text = levelRequirments [playerData.WingLevel - 1].ToString ();

		armorLevel.text = playerData.ArmorLevel.ToString ();
		ammoLevel.text = playerData.AmmoLevel.ToString ();
		heatLevel.text = playerData.HeatLevel.ToString ();
		wingLevel.text = playerData.WingLevel.ToString ();


		upgradesAmt.text = playerData.shipUpgrades.ToString ();


	}
	
	// Update is called once per frame
	void Update () {
		//armorLevel.text = playerData.ArmorLevel.ToString ();
		//ammoLevel.text = playerData.AmmoLevel.ToString ();
		//heatLevel.text = playerData.HeatLevel.ToString ();
		//wingLevel.text = playerData.WingLevel.ToString ();


		//upgradesAmt.text = playerData.shipUpgrades.ToString ();

	}


	public void Upgrade( Upgrades upgrade){


		switch (upgrade) {


		case Upgrades.Armor:

			if (playerData.shipUpgrades >= levelRequirments [playerData.ArmorLevel-1]) {
				playerData.shipUpgrades -= levelRequirments [playerData.ArmorLevel-1];
				playerData.ArmorLevel++;
			}


			break;
		case Upgrades.Wings:
			if (playerData.shipUpgrades >= levelRequirments [playerData.WingLevel-1]) {
				playerData.shipUpgrades -= levelRequirments [playerData.WingLevel-1];
				playerData.WingLevel++;
			}


			break;
		case Upgrades.Ammo:

			if (playerData.shipUpgrades >= levelRequirments [playerData.AmmoLevel-1]) {
				playerData.shipUpgrades -= levelRequirments [playerData.AmmoLevel-1];
				playerData.AmmoLevel++;
			}


			break;
		case Upgrades.Heat:

			if (playerData.shipUpgrades >= levelRequirments [playerData.HeatLevel-1]) {
				playerData.shipUpgrades -= levelRequirments [playerData.HeatLevel-1];
				playerData.HeatLevel++;
			}



			break;
		
		default:
			break;



		}
		armorLevel.text = playerData.ArmorLevel.ToString ();
		ammoLevel.text = playerData.AmmoLevel.ToString ();
		heatLevel.text = playerData.HeatLevel.ToString ();
		wingLevel.text = playerData.WingLevel.ToString ();


		upgradesAmt.text = playerData.shipUpgrades.ToString ();



		AmountRequiredArmor.text = levelRequirments [playerData.ArmorLevel - 1].ToString ();
		AmountRequiredAmmo.text = levelRequirments [playerData.AmmoLevel - 1].ToString ();
		AmountRequiredHeat.text = levelRequirments [playerData.HeatLevel - 1].ToString ();
		AmountRequiredWing.text = levelRequirments [playerData.WingLevel - 1].ToString ();

	}




}
