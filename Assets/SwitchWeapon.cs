﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchWeapon : MonoBehaviour {

	public SecondaryWeapon secWeapon;
	public GameObject weapon1;
	public GameObject Weapon2;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void SetWeaponOne(){
		secWeapon.curSecWeapon = weapon1;
		secWeapon.fireRate = 0;

		secWeapon.prepShootTime = 2;
		secWeapon.shootTime = 2;
	

	}

	public void SetWeaponTwo(){
		secWeapon.curSecWeapon = Weapon2;

		secWeapon.fireRate = 120;

		secWeapon.prepShootTime = 0;
		secWeapon.shootTime = 0.1f;

	}






}
