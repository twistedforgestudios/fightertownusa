using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SettingsLegal : MonoBehaviour {

	public float screenwoffset;
	public float screenhoffset;
	public float heightoffset;
	public float widthoffset;
    public float offset;

    public Renderer rend;

	public Text text;
	private Vector3 coords;

	public GUIStyle customButton;

	void Start () {
		rend = GetComponent<Renderer>();
		
		screenwoffset=Screen.width;
		screenhoffset=Screen.height;   
		heightoffset=387;
		widthoffset=765;
		offset=screenhoffset / heightoffset;

		coords = Camera.main.WorldToScreenPoint(transform.position);
	}


private void OnGUI () {

	customButton.fontSize = Mathf.RoundToInt (13f * offset);
	//if (Settings.active==true)
		//GUI.Label(new Rect(coords.x-(60*offset), coords.y+(78*offset), 100*offset, 100*offset), "Legal", customButton);
}
	void Update () {
		if (Settings.active == true) {
			rend.enabled = true;
			text.enabled = true;
		} else {
			rend.enabled = false;
			text.enabled = false;
		}
	}
}
