﻿using UnityEngine;
using System.Collections;

public class TanksDestroyedUI : MonoBehaviour {
	public NumberUI numbers;

	// Use this for initialization
	void Start () {
		numbers.number = GM.enemyunits_Terrain;
	}
	
	// Update is called once per frame
	void Update () {
		numbers.number = GM.enemyunits_Terrain;
	}
}
