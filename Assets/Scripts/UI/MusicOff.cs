using UnityEngine;
using System.Collections;

public class MusicOff : MonoBehaviour {

    public Renderer rend;


	void OnMouseUpAsButton () {
		MM.musicvolumechange=true;
		MM.musicvolumechangevalue=0;
	}

	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Settings.active==true)
			rend.enabled = true;
		else
			rend.enabled = false;

	
	}
}
