﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FuelDisplayUI : MonoBehaviour {

	public NumberUI fuelNumberUI;
	public PlayerGameData gameData;

	public Sprite fuel0;
	public Sprite fuel1;
	public Sprite fuel2;
	public Sprite fuel3;
	public Sprite fuel4;
	public Sprite fuel5;
	public Sprite fuel6;
	public Sprite fuel7;
	public Sprite fuel8;


	public Image fuelMeter;

	// Use this for initialization
	void Start () {

		//fuelNumberUI.number = gameData.NumOfFuelCells;


		switch (gameData.NumOfFuelCells) {

		case 8:
			fuelMeter.overrideSprite = fuel8;
			break;
		case 7:
			fuelMeter.overrideSprite = fuel7;
			break;
		case 6:
			fuelMeter.overrideSprite = fuel6;
			break;
		case 5:
			fuelMeter.overrideSprite = fuel5;
			break;
		case 4:
			fuelMeter.overrideSprite = fuel4;
			break;
		case 3:
			fuelMeter.overrideSprite = fuel3;
			break;
		case 2:
			fuelMeter.overrideSprite = fuel2;
			break;
		case 1:
			fuelMeter.overrideSprite = fuel1;
			break;
		case 0:
			fuelMeter.overrideSprite = fuel0;
			break;
		default:
			break;
		}




	}
	
	// Update is called once per frame
	void Update () {
		//fuelNumberUI.number = gameData.NumOfFuelCells;



		switch (gameData.NumOfFuelCells) {

		case 8:
			fuelMeter.overrideSprite = fuel8;
			break;
		case 7:
			fuelMeter.overrideSprite = fuel7;
			break;
		case 6:
			fuelMeter.overrideSprite = fuel6;
			break;
		case 5:
			fuelMeter.overrideSprite = fuel5;
			break;
		case 4:
			fuelMeter.overrideSprite = fuel4;
			break;
		case 3:
			fuelMeter.overrideSprite = fuel3;
			break;
		case 2:
			fuelMeter.overrideSprite = fuel2;
			break;
		case 1:
			fuelMeter.overrideSprite = fuel1;
			break;
		case 0:
			fuelMeter.overrideSprite = fuel0;
			break;
		default:
			break;
		}

	}


		
}
