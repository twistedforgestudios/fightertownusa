﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FollowArrowUI : MonoBehaviour {
	
	public Transform otherTransform;
	public Transform playerTransform;

	public float maxY;
	public float minY;

	public float maxX;
	public float minX;

	private Vector3 targetPosition;
	//private Camera cam;
	private SpriteRenderer otherRender;
	private Image image;

	public TimedEvent timeEvent;

	// Use this for initialization
	void Start () {
		//cam = Camera.main;
		otherRender = otherTransform.GetComponent<SpriteRenderer> ();
		image = GetComponent<Image> ();
	//	timeEvent.StopTimer ();
	}
	
	// Update is called once per frame
	void Update () {
			
		if (otherRender != null) {
			if (otherRender.isVisible) {
				image.enabled = false;
				timeEvent.StopTimer ();
				timeEvent.ResetTimer ();
				timeEvent.gameObject.SetActive (false);
			} else {
				image.enabled = true;
				timeEvent.gameObject.SetActive (true);
				timeEvent.StartTimer ();

			}
		}
	}

}
