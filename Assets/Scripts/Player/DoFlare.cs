using UnityEngine;
using System.Collections;

public class DoFlare : MonoBehaviour {


	public static bool flare;
	static public float flarecount;
	Vector3 temp;



	public static ParticleSystem ps;


	void Start () {
    	ps = GetComponent<ParticleSystem>();
		ps.GetComponent<Renderer>().sortingLayerName = "front";
	}

	void Update () {
    	if (flare) {
    		ps.Play();
    		flare=false;
			flarecount=50;
			temp = GM.planepos; // copy to an auxiliary variable...
    	}
		if (flarecount!=0) {
			flarecount--;
			temp.y+=.1f;
			temp.x+=.6f;
			transform.position=temp;
		}

    }
}

