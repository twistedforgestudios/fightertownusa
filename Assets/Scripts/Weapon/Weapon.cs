using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	

	public float fireRate = 0;
	public float Damage = 10;
	public int clear;
	public LayerMask whatToHit;

	float timeToSpawnEffect = 0;
	public float effectSpawnRate = 10;

	//added flag so bullet can shoot multiple shots
	public bool tripleShootMode = false; 
	public bool explodeShootMode = false;

	public bool laserShootMode = false;

	public GameObject lazerBullet;
	public bool heatEnabled = true;
	float timeToFire = 0;
	Transform firePoint;

	//added object pool class to handle bullets
	//ObjectPool bulletPool;

	public ObjectPool basicBulletPool;

	public ObjectPool explodingBulletPool;

	//Heat guage script to handle when the Gun can fire
	HeatGuage heatGuage;


    AudioSource fireSound;

	void Awake () {
		firePoint = transform.FindChild ("FirePoint");
		if (firePoint == null) {
			Debug.LogError ("No firePoint? WHAT?!");
		}

		//ObjectPool componet must be in the same game object as this weapon
		//bulletPool = GetComponent<ObjectPool> ();

		//HeatGuage Script must be in the same game object as this weapon;
		heatGuage = GetComponent<HeatGuage>();



   	fireSound = GetComponent<AudioSource> ();
	}

	// Use this for initialization
	void Start () {
		clear=0;
	}
	
	// Update is called once per frame
	void Update () {

		#if UNITY_EDITOR
		if(Input.GetKeyDown(KeyCode.E)){
			GM.button_pressed = 2;
		}
		#endif

		#if UNITY_STANDALONE_WIN
		if(Input.GetKeyDown(KeyCode.E)){
			GM.button_pressed = 2;
		}
		#endif




		if (!laserShootMode) {

		    	if (GM.button_pressed ==2) {
			 //   	print("shooting"+Time.time);
		    		GM.button_pressed=0;
					if (fireRate == 0) {
						Shoot ();
					} else {
						if (Time.time > timeToFire) {
							timeToFire = Time.time + 1 / fireRate;
							Shoot ();
						}
					}
	    		}
		//	}
		} 
	}


	void Shoot () {

   // 	print("shoot");

//		Vector2 mousePosition = new Vector2 (Camera.main.ScreenToWorldPoint (Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
//		Vector2 firePointPosition = new Vector2 (firePoint.position.x, firePoint.position.y);
		
	//	RaycastHit2D hit = Physics2D.Raycast (firePointPosition, mousePosition-firePointPosition, 100, whatToHit);
		if (Time.time >= timeToSpawnEffect) {


			//add heat to the heat gauge
			if(heatEnabled)
				heatGuage.IncreaseHeat();

			if (!heatGuage.overheated) {
   			fireSound.Play ();
				//added condition that if triple shoot flag is on then perform triple shoot
				if (tripleShootMode) {
					FireTripleShot ();

				} else {
					Effect ();
				}
			}




			timeToSpawnEffect = Time.time + 1/effectSpawnRate;
		}
	//	Debug.DrawLine (firePointPosition, (mousePosition-firePointPosition)*100, Color.cyan);
	//	if (hit.collider != null) {
	 //   	Debug.DrawLine (firePointPosition, hit.point, Color.red);
	 //   	Debug.Log ("We hit " + hit.collider.name + " and did " + Damage + " damage.");

			//GM.enemyunits_Air--;;
	//	}
	}
	//changed Effect so it works with the objectPool
	void Effect () {

		GameObject obj;
		if (explodeShootMode) {
			 obj = explodingBulletPool.GetPooledObject ();
		} else {
			
			obj = basicBulletPool.GetPooledObject ();
		}


		if (obj == null)
			return;
		
		Rigidbody2D rgd = obj.GetComponent<Rigidbody2D> ();


		obj.transform.position = firePoint.position;
		obj.transform.rotation = firePoint.rotation;
		rgd.velocity.Set (0.0f,0.0f);		 //fixes bad angle problem
		obj.SetActive (true);

	}

	//added a function for triple shoot
	void FireTripleShot(){
		GameObject obj1 = basicBulletPool.GetPooledObject ();
		if (obj1 == null)
			return;

		obj1.transform.position = firePoint.position;
		obj1.transform.rotation = firePoint.rotation;
		obj1.SetActive (true);

		GameObject obj2 = basicBulletPool.GetPooledObject ();
		if (obj2 == null)
			return;

		obj2.transform.position = firePoint.position + this.transform.TransformDirection(Vector3.up);
		obj2.transform.rotation = firePoint.rotation;
		obj2.SetActive (true);

		GameObject obj3 = basicBulletPool.GetPooledObject ();
		if (obj3 == null)
			return;

		obj3.transform.position = firePoint.position + this.transform.TransformDirection(Vector3.down);  ;
		obj3.transform.rotation = firePoint.rotation ;
		obj3.SetActive (true);

	}

}
