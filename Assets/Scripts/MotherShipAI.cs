﻿using UnityEngine;
using System.Collections;

public class MotherShipAI : MonoBehaviour {
	public Animator BossHealthAnim;
	ForwardMovement movescript;
	Health health;
	public float reverseTime;
	bool reversing;
	float reverseTimer;
	public int damageTakenbyEmp;

	Collider2D shipCollider;

	void Start () {
		movescript = GetComponent<ForwardMovement> ();
		reverseTimer = reverseTime;
		health = GetComponent<Health> ();
		shipCollider = GetComponent<Collider2D> ();
	}
	

	void Update () {
		if (reversing) {
			reverseTimer -= Time.deltaTime;

			if (reverseTimer <= 0f) {
				reversing = false;
				reverseTimer = reverseTime - 2f;
				movescript.direction.x *= -0.3f;
				shipCollider.enabled = true;
			}
		}
	}
		
	void OnTriggerEnter2D(Collider2D col){
		
		if (col.gameObject.tag == "Emp") {

			if (health.isAlive) {


				if (health.HealthPoints > damageTakenbyEmp) {
					reversing = true;
					movescript.direction.x *= -5f;
				}
			} 


			health.Damage (damageTakenbyEmp);
			BossHealthAnim.SetTrigger ("Damage");
			shipCollider.enabled = false;
			EventManager.TriggerEvent ("DamageMotherShip");

		}


	}


}
