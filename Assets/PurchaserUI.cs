﻿using UnityEngine;
using UnityEngine.Purchasing;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PurchaserUI : MonoBehaviour, IStoreListener {

	public PlayerGameData playerData;
	private static IStoreController m_StoreController;
	private static IExtensionProvider m_StoreExtensionProvider;

	public static string fuelCellConsumableProductID = "fuelcell_1";

	public static string FighterBlueProductID = "fighter_blue";

	public static string CamoGreenProductID = "camo_green";

	public static string ArticLeopardProductID = "artic_leopard";

	public static string BlackTigerProductID = "black_tiger";

	public static string DesertEagleProductID = "desert_eagle";

	public static string FlyingBeagleProductID = "flying_beagle";


	//public static string kProductIDNonComsumable = "nonConsumable";
	//public static string kProductIDSubscription = "subsciption";

	//private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

	//private static string kProductNameGooglePlaySunscription = "com.unity3d.subscription.original";



	private GUIStyle guiStyle = new GUIStyle(); //create a new variable
	private string DebugString = "";

	// Use this for initialization
	void Start () {
		if (m_StoreController == null) 
		{
			//Begin to configure our connection to Purchasing
			//InitializePurchasing();
		}
	
	}

	public void InitializePurchasing(){

		if(IsInitialized())
		{
				return;
		}
		DebugString += string.Format ("Conecting to the purchaser..") +  System.Environment.NewLine;

			//var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

			//builder.AddProduct(fuelCellConsumableProductID, ProductType.Consumable);

	     	//builder.AddProduct (FighterBlueProductID, ProductType.NonConsumable);
		   // builder.AddProduct (CamoGreenProductID, ProductType.NonConsumable);
		   // builder.AddProduct (ArticLeopardProductID, ProductType.NonConsumable);
		   // builder.AddProduct (BlackTigerProductID, ProductType.NonConsumable);
		   // builder.AddProduct (DesertEagleProductID, ProductType.NonConsumable);
		   // builder.AddProduct (FlyingBeagleProductID, ProductType.NonConsumable);

	
			//UnityPurchasing.Initialize(this,builder);

	}

	private bool IsInitialized(){
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}
		



	public void BuyTenFuelCells(){
		BuyProductID (fuelCellConsumableProductID);
	}


	public void BuyFighterBlue(){
		BuyProductID (fuelCellConsumableProductID);
	}

	public void BuyCamoGreen(){
		BuyProductID (CamoGreenProductID);
	}

	public void BuyArticLeopard(){
		BuyProductID (ArticLeopardProductID);
	}

	public void BuyBlackTiger(){
		BuyProductID (BlackTigerProductID);
	}

	public void BuyDesertEagle(){
		BuyProductID (DesertEagleProductID);
	}

	public void BuyFlyingBeagle(){
		BuyProductID (FlyingBeagleProductID);
	}



	public bool CheckPreviousPurchases(string productId){



		if (IsInitialized ()) {
			Product product = m_StoreController.products.WithID (productId);


			if (product != null && product.hasReceipt) {
				return true;
			}

		}

		return false;

	}


	void BuyProductID(string productId)
	{
		if (IsInitialized ()) {

			Product product = m_StoreController.products.WithID (productId);


			if (product != null && product.availableToPurchase) {
				Debug.Log (string.Format ("Purchasing product asychronously: '{0}'", product.definition.id));
				DebugString += string.Format ("Purchasing product asychronously: '{0}'", product.definition.id) +  System.Environment.NewLine;
				m_StoreController.InitiatePurchase (product);
			}
			else {
				Debug.Log ("BuyProductId:Fail. Not purchasing product, either is not found or is not available for purchase");
				DebugString += "BuyProductId:Fail. Not purchasing product, either is not found or is not available for purchase" +  System.Environment.NewLine;
			}
		}



	}

	public void RestorePurchases()
	{
		if (!IsInitialized ()) {
			Debug.Log ("Restore Purchase FAIL. Not Initialized");
			DebugString += "Restore Purchase FAIL. Not Initialized" +  System.Environment.NewLine;
			return;
		}


		if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {

			Debug.Log (" RestorePurchases Started..");
			DebugString += " RestorePurchases Started.." + "/n";
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();

			apple.RestoreTransactions ((result) => {

				Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
				DebugString += "RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore." +  System.Environment.NewLine;
			});
		} 
		else 
		{
			Debug.Log ("RestorePurchases FAIL. Not supported on this Platform. Current = " + Application.platform);
			DebugString += "RestorePurchases FAIL. Not supported on this Platform. Current = " + Application.platform + System.Environment.NewLine;
		}


		}
		
	// --  IStoreListener

	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log ("OnInitialized: PASS");
		DebugString += "OnInitialized: PASS" + System.Environment.NewLine;
		m_StoreController = controller;

		m_StoreExtensionProvider = extensions;
	}


	public void OnInitializeFailed(InitializationFailureReason error){
		Debug.Log ("OnInitializedFailed InitializationFailureReason:" + error);
		DebugString += "OnInitializedFailed InitializationFailureReason:" + error +  System.Environment.NewLine;
	}


	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args){

		if (string.Equals (args.purchasedProduct.definition.id, fuelCellConsumableProductID, System.StringComparison.Ordinal))
		{
			//m_StoreController.
			//args.purchasedProduct.
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;
			//playerData.IncrementFuelCell(10);

		}
		else if (string.Equals (args.purchasedProduct.definition.id, FighterBlueProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;
		} 
		else if (string.Equals (args.purchasedProduct.definition.id, CamoGreenProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;



		} 
		else if (string.Equals (args.purchasedProduct.definition.id, ArticLeopardProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;



		} 
		else if (string.Equals (args.purchasedProduct.definition.id, BlackTigerProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;



		} 
		else if (string.Equals (args.purchasedProduct.definition.id, DesertEagleProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;



		} 
		else if (string.Equals (args.purchasedProduct.definition.id, FlyingBeagleProductID, System.StringComparison.Ordinal))
		{
			//Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			//DebugString += string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;



		} 
		else 
		{
			Debug.Log (string.Format ("ProcessPurchase: FAIL. Unrecognized Product: '{0}'", args.purchasedProduct.definition.id));
			DebugString += string.Format ("ProcessPurchase: FAIL. Unrecognized Product: '{0}'", args.purchasedProduct.definition.id) + System.Environment.NewLine;
		}
		return PurchaseProcessingResult.Complete;
	}



	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason){
		Debug.Log (string.Format ("ProcessPurchase: FAIL. Product: '{0}' , PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
		DebugString += string.Format ("ProcessPurchase: FAIL. Product: '{0}' , PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason) + System.Environment.NewLine;
	}

	void OnGUI(){

		guiStyle.fontSize = 40;

		GUI.Label ( new Rect (500, 500, 1000, 1000), DebugString,guiStyle);
	}



}
