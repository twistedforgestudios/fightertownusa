﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetryLevelButton : MonoBehaviour {
	public PlayerGameData playerData;
	public GM game;
	public RectTransform displayPrompt;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void CheckFuelCells(){


		if (playerData.NumOfFuelCells > 0) {
			game.RestartLevel ();
			playerData.DecrementFuelCell ();
			displayPrompt.gameObject.SetActive (false);
		} else {

			displayPrompt.gameObject.SetActive (true);

		}



	}
}
