﻿using UnityEngine;
using System.Collections;

public class LevelUnlocker : MonoBehaviour {

	public LevelGameData nextLevel;
	public int nextLevelId;
	public void UnlockNextLevel(){

		if(nextLevel != null)
			nextLevel.Unlocked = true;

		GameDataManager.instance.SaveLevel (nextLevelId);
	

	}

}
