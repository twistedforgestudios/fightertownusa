using UnityEngine;
using System.Collections;
public class HeatEnd : MonoBehaviour {


	static public bool show;		
	static public bool stopped;		
    public Renderer rend;

	void Start () {
        rend = GetComponent<Renderer>();
		rend.enabled = false;
	}

	void Update () {
		if (CloudsEnd.show==true) {
			rend.enabled = true;
            rend.sortingLayerName = "front";
		}
    }
}

