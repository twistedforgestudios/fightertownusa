﻿using UnityEngine;
using System.Collections;

public class EnemiesDestroyedUI : MonoBehaviour {
	
	public NumberUI numbers;

	// Use this for initialization
	void Start () {
		numbers.number = GM.enemyunits_Air;
	}
	
	// Update is called once per frame
	void Update () {
		numbers.number = GM.enemyunits_Air;
	}
}
