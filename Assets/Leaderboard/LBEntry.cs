﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LBEntry : MonoBehaviour {

	public Text ScoreEntry;
	public Text NameEntry;
	public Text ScoreIndex;

	public void setScore(int score){
		ScoreEntry.text = score.ToString ();
	}


	public void setPlayerName(string name){
		NameEntry.text = name;
	}

	public void setScoreIndex(int index){
		ScoreIndex.text = index.ToString ();
	}

	public void SetMainPlayer(){
		NameEntry.color = Color.red;
	}




}
