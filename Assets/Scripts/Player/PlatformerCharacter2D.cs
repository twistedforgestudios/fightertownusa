using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    public class PlatformerCharacter2D : MonoBehaviour
    {
		//public PlayerGameData playerData;
        public float m_MaxSpeed = 20f;                    // The fastest the player can travel in the x axis.
		public bool ReverseScale;
        static public Animator m_Anim;            // Reference to the player's animator component.
        private Rigidbody2D m_Rigidbody2D;
		public bool UseAltMove = false;
	//	public int acRotation = 15;
		static public int acRotation = 3;
		static public float acRotationvalue;
		public float rotationSpeed = 100f;
		static public float mh = 0;
		static public float mv = 0;
		public float speed = 0;
		static public float speeddec = 0;
		public bool dead;		
		public bool exit;		
		public bool bouncing;		

		public bool doroll = false;
		public bool dorolldemo;
		public bool dorolldemostarted;
		static public bool cameraon = true;
		public int rolltimer;
		public int deadcount;
		public int flashcount;
		public int delayroll;

		public bool toohigh=false;
		public int toohightimer;
		static public bool falling=false;
		public bool stopped=false;
		public bool stealth=false;
		public int stealthtimer;
		public bool fallleft;
		public bool spinning;
		public int presstime;
		public bool ReverseDirection = false;
        public Color mycolor;
		private SpriteRenderer spriteRender;

		private bool isMovingVert;
		private bool isRotatingAngle;
		private bool isBoth;

		public bool IsMoving{
			get {return isMovingVert;}
			set { isMovingVert = value; }
		}
		public bool IsRotating{
			get {return isRotatingAngle;}
			set {isRotatingAngle = value;}
		}

		public bool IsBoth{
			get {return isBoth;}
			set {isBoth = value;}
		}


        private void Awake()
        {
            // Setting up references.
            m_Anim = GetComponent<Animator>();
            m_Rigidbody2D = GetComponent<Rigidbody2D>();
			spriteRender = GetComponent<SpriteRenderer> ();
			mh=1;
			mv=0;

			speeddec=0;
			//spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];
        }

        private void Exit() {
			GM.Exiting=true;
			GM.completed=false;
			GM.scene="-1";
	    	stopped=true;
			Health health = m_Rigidbody2D.GetComponent<Health> ();
			health.invulnerble=true;
			CloudsEnd.endlevel=true;
			Fade.fadeingOut = true;
			m_Anim.SetBool("spin", false);
			PlaneEnd.rolltimer=10;

		}

        private void Start()
		{
			dead=false;
			stopped=false;
			spinning=false;
			acRotation=3;
			cameraon=true;
			falling=false;

			ReverseScale = ControlTypeOption.ReverseStyle;


	    	rolltimer=50;
	    	dorolldemo=false;
	    	Exhaust.exhaust_start=true;

			mycolor=transform.GetComponent<Renderer>().material.color;

			if (PlaneEnd.show==true)
				PlaneEnd.kill=true;


			GM.intunnel=false;

			dorolldemostarted=false;


			//spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];


			//PlatformerCharacter2D.m_Anim.Play("plane", -1, (.5f*GM.skinsave));
			//PlatformerCharacter2D.m_Anim.speed=0;


      //     Physics.IgnoreCollision(ground.GetComponent<Collider>(), GetComponent<Collider>());


		}
		  
        public string spriteSheetName;

		void LateUpdate () {


			if(spriteRender.sprite == null)
				spriteRender.sprite = GameDataManager.instance.Skins [GM.skinsave];


			/*


			var subSprites = Resources.LoadAll<Sprite>("planes/"+spriteSheetName);

            foreach (var renderer in GetComponentsInChildren<SpriteRenderer>())
             {
             string spriteName = renderer.sprite.name;

		     print("spriteName "+spriteName);
             var newSprite = Array.Find(subSprites, item => item.name == spriteName);
 
             if (newSprite) {
				 print("newSprite "+newSprite);
                 renderer.sprite = newSprite;
			 }
            }

			*/


		}

		private void OnTriggerEnter2D(Collider2D other) {
			/*
			if (other.tag == "purple") {		
				//print("purple enter");
	    		//cameraon=false;
			}
			if (other.tag == "ground") {		
				//print("ground");

				if (!GM.intunnel) {
					GM.explodetype=1;
				}
				GM.hitground=true;
			}

			if (other.tag == "entry") {		
				//print("entry");
				if (GM.intunnel)
					GM.intunnel=false;
				else
					GM.intunnel=true;
			}
*/
			if (other.tag == "sky") {		
				//print("sky");
				toohigh=true;
				toohightimer=150;
				Exhaust.exhaust_end=true;
			}

		}
		private void OnTriggerExit2D(Collider2D other) {
			if (other.tag == "purple") {		
				//print("purple exit");
	    		cameraon=true;
			}

			/*
			if (other.tag == "sky") {	
				if (falling==false && spinning==false) {
				//	print("sky exit");
					toohigh=false;
					transform.GetComponent<Renderer>().material.color = mycolor;
					toohightimer=150;
					Exhaust.exhaust_start=true;
				}
			}*/


		}

        private void Flash() {							 
			flashcount++;
			if (flashcount==20) {
				flashcount=0;
			}
			if (flashcount>10)
				transform.GetComponent<Renderer>().material.color = Color.red;
			else
				transform.GetComponent<Renderer>().material.color = mycolor;
		}

		public void DoAltRotate() {

			if (spinning == false && !bouncing) {

				#if UNITY_STANDALONE_WIN


				//on key up check if position is reversed

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

				GM.button_pressed_type = 0;

				transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
				GM.button_pressed_type = 0;

				transform.Rotate (0, 0, (-1 * acRotation));
				}
				#endif 	
				/*
				#if UNITY_EDITOR

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
					
				#endif
				*/

				#if UNITY_ANDROID

			
				//Debug.Log(IsRotating);
				//Debug.Log( CrossPlatformInputManager.GetAxis ("Vertical"));

				if (GM.button_pressed_type == 1){
					GM.button_pressed_type = 0;
					transform.Rotate (0, 0, 15);
				}else if (GM.button_pressed_type == 2){
					GM.button_pressed_type = 0;
					transform.Rotate (0, 0, -15);
				}
					

				#endif







				Vector3 eulerAngles = transform.rotation.eulerAngles;

				acRotationvalue	= eulerAngles.z;

				if (acRotationvalue >=0 && acRotationvalue <91) {
					mh=1-(.0111f*acRotationvalue);
					mv=.0111f*acRotationvalue;
					transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=false;
				}

				if (acRotationvalue >90 && acRotationvalue <181) {
					mh=-(acRotationvalue-90)*.0111f;
					mv=1-(.0111f*(acRotationvalue-90));

					if (ReverseScale) 
						transform.localScale = new Vector3(1f, -1f, 1f);
					else
						transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=true;

				}

				if (acRotationvalue >180 && acRotationvalue <271) {
					mh=-(1-(.0111f*(acRotationvalue-180)));
					mv=-(.0111f*(acRotationvalue-180));


					if (ReverseScale) 
						transform.localScale = new Vector3(1f, -1f, 1f);
					else
						transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=true;
				}


				if (acRotationvalue >270 && acRotationvalue <360) {
					mh=(acRotationvalue-270)*.0111f;
					mv=-(1-(.0111f*(acRotationvalue-270)));
					transform.localScale = new Vector3(1f, 1f, 1f);

					fallleft=false;
				}
			}
		

		}

		//ignore
        public void DoRotate() {
			
			if (spinning==false && !bouncing) {

				#if UNITY_STANDALONE_WIN


				//on key up check if position is reversed

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
				#endif 	
				/*
				#if UNITY_EDITOR

				if (Input.GetKey (KeyCode.UpArrow) || GM.button_pressed_type == 1) {

					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, acRotation);

				} else if (Input.GetKey (KeyCode.DownArrow) || GM.button_pressed_type == 2) {
					GM.button_pressed_type = 0;

					transform.Rotate (0, 0, (-1 * acRotation));
				}
					
				#endif
				*/

				#if UNITY_ANDROID

				float increment = 0;

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) >= 0)
					increment = CrossPlatformInputManager.GetAxis ("Vertical");

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) < 0)
					increment = CrossPlatformInputManager.GetAxis ("Vertical");


				if(ReverseDirection){
					acRotation = (int)transform.rotation.z;
					m_Rigidbody2D.angularVelocity = acRotation + increment * rotationSpeed;

				}
				else {
					acRotation = (int)transform.rotation.z;
					//transform.Rotate (0, 0, acRotation + increment * rotationSpeed);
					m_Rigidbody2D.angularVelocity = acRotation + increment * rotationSpeed;
				}
				
				#endif

				#if UNITY_IPHONE

				float increment = 0;

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) >= 0)
				increment = CrossPlatformInputManager.GetAxis ("Vertical");

				if(Mathf.Sign(CrossPlatformInputManager.GetAxis ("Vertical")) < 0)
				increment = CrossPlatformInputManager.GetAxis ("Vertical");


				if(ReverseDirection){
				acRotation = (int)transform.rotation.z;
				transform.Rotate (0, 0, acRotation +  increment * -2.5f);

				}
				else {
				acRotation = (int)transform.rotation.z;
				transform.Rotate (0, 0, acRotation + increment * 2.5f);
				}

				#endif





			}


			Vector3 eulerAngles = transform.rotation.eulerAngles;

			acRotationvalue	= eulerAngles.z;

			if (acRotationvalue >=0 && acRotationvalue <91) {
				mh=1-(.0111f*acRotationvalue);
				mv=.0111f*acRotationvalue;
				transform.localScale = new Vector3(1f, 1f, 1f);
			
				fallleft=false;
			}

			if (acRotationvalue >90 && acRotationvalue <181) {
				mh=-(acRotationvalue-90)*.0111f;
				mv=1-(.0111f*(acRotationvalue-90));

				if (ReverseScale) 
					transform.localScale = new Vector3(1f, -1f, 1f);
				else
					transform.localScale = new Vector3(1f, 1f, 1f);
				
				fallleft=true;

			}

			if (acRotationvalue >180 && acRotationvalue <271) {
				mh=-(1-(.0111f*(acRotationvalue-180)));
				mv=-(.0111f*(acRotationvalue-180));


				if (ReverseScale) 
					transform.localScale = new Vector3(1f, -1f, 1f);
				else
					transform.localScale = new Vector3(1f, 1f, 1f);
			
				fallleft=true;
			}


			if (acRotationvalue >270 && acRotationvalue <360) {
				mh=(acRotationvalue-270)*.0111f;
				mv=-(1-(.0111f*(acRotationvalue-270)));
				transform.localScale = new Vector3(1f, 1f, 1f);
		
				fallleft=false;
			}
		}

		public void DoMovement(){


		
			m_Rigidbody2D.velocity	+= (Vector2.up * CrossPlatformInputManager.GetAxis ("Vertical") * 20f);
		}




		public void CheckReversePosition(){
			if (transform.localScale.y == -1) {
				ReverseDirection = true;
			} else {
				ReverseDirection = false;
			}

		}




        private void FixedUpdate()
        {
			Move();

	    	if (CloudsEnd.show==true)
	    		this.gameObject.SetActive (false);


			if (toohigh) {
				toohightimer--;
				Flash();
				if (toohightimer==0) {
					//print("fall");
                    m_Rigidbody2D.gravityScale=200;
                    m_Rigidbody2D.isKinematic=false;
					falling=true;
					toohigh=false;
					//EventManager.TriggerEvent("Falling");
					Health health = m_Rigidbody2D.GetComponent<Health> ();
					health.HealthPoints = 1;
					//Debug.Log("TriggeringFalling");
				}

			}

			/*


			if (IsBoth) {
				DoRotate ();
				DoMovement ();
			}
			else if (IsRotating) {
				DoRotate ();
			} 
			else if (IsMoving) {
				DoMovement ();
			}
		*/

			if (UseAltMove) {
				DoAltRotate ();
			} else {

				DoRotate ();
			}


			if (falling) {
				if (fallleft==true) {
					transform.Rotate(0, 0, (10));
					if (acRotationvalue>260)
						falling=false;
				}
				else {
					transform.Rotate(0, 0, (-10));
					if (acRotationvalue < 280 && acRotationvalue >180)
						falling=false;
				}
				if (falling==false) {
					spinning=true;
					//m_Anim.SetBool("spin", true);
					PlatformerCharacter2D.m_Anim.speed=2;

				}

			}
			/*
			if (dorolldemo) {



				if (rolltimer!=0) {
					rolltimer--;
					if (rolltimer==0) {
						m_Anim.Play("roll", -1, .5f*GM.skinsave);
			    		dorolldemo=false;
						Exhaust.roll=true;
						rolltimer=150;
						dorolldemostarted=true;

						PlatformerCharacter2D.m_Anim.speed=2;

					}
				}
			}

			if (dorolldemostarted) {

                int animationIndex = ((int)(m_Anim.GetCurrentAnimatorStateInfo(0).normalizedTime * (48))) % 48;

				if (animationIndex>(24*(GM.skinsave+1))) {
		    		m_Anim.Play("plane", -1, .5f*GM.skinsave);
		        	dorolldemostarted=false;

					PlatformerCharacter2D.m_Anim.speed=0;

				}
			}
			if (delayroll==0) {
				if (Input.GetKey(KeyCode.R)) {

					print("r");
			    	dorolldemo=true;
					rolltimer=1;
					delayroll=10;
				}  
				if (Input.GetKey(KeyCode.Z)) {
					m_Anim.SetTrigger("stealth_roll");
					Exhaust.roll=true;
					delayroll=10;
				}				   

			}
			else
				delayroll--;
			*/
			if (presstime!=0)
				presstime--;


			if (Input.GetKey(KeyCode.E)) {
				if (presstime==0) {
					presstime=10;
					Emp.start=true;
//					print("e");
				}
			}				   


			if (Input.GetKey(KeyCode.F)) {
		    //	DoFlare.flare=true;
			}			



			if (Input.GetKey(KeyCode.S)) {
				stealthtimer=200;
		    	stealth=true;
				m_Anim.SetBool("stealth", true);
			}				   

			/*
			if (stealth) {
				if (stealthtimer!=0) {
					stealthtimer--;
				}
				else {
					stealth=false;
					m_Anim.SetBool("stealth", false);
				}
			}*/

			if (dead==false) {

				Health health = m_Rigidbody2D.GetComponent<Health> ();


		 //   	if (health.HealthPoints<5) {
		 //   		Flash();
		 //   	}

				
				if (health.isAlive==false) {

					GM.explodeflag=true;
					dead=true;
					print("dead "+dead);
					deadcount=40;
					m_Rigidbody2D.gravityScale=0;
					falling=false;
					m_Anim.SetBool("spin", false);
				}
			}
			else {
				if (speeddec<m_MaxSpeed) {
					speeddec+=1f;
					if (deadcount!=0) {
						deadcount--;
						if (deadcount==0) {
							this.gameObject.SetActive (false);
						}
					}
				}
			}

		}


		public void Move()
        {
			PlayerPowerState powerState = GetComponent<PlayerPowerState> ();

			speed=m_MaxSpeed-speeddec;

			speed *= powerState.playerSpeedMulti;

			if (GM.completed) {
				Exit();
				//Debug.Log ("Exiting");
			}
	  //  	if (stopped)
	  //  		speed=0;
			if (falling)
				speed=0;


			m_Rigidbody2D.velocity = new Vector2(mh*speed, mv*speed);

        }

    }
}
