﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class StartMenu : MonoBehaviour {


	public float startSecs;
	public string CampaignMap;
	public string Options;

	public Animator stBtnAnim;
	public Animator psBtnAnim;
	public Animator opBtnAnim;


	public Animation clip;

	public GameObject stButton;
	public GameObject psButton;
	public GameObject opButton;

	public Animator GameMapAnim;

	AsyncOperation async;
	AudioSource gmStSound;

	float transitionTimer;
	bool startTimer = false;

	// Use this for initialization
	void Start () {
		transitionTimer = startSecs;
		GameDataManager.instance.Load ();



		//check how much time has passed scince the game has launched
		//GameDataManager.instance

		gmStSound = GetComponent<AudioSource> ();

	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)){
			QuitGame ();
		}

		if (startTimer) {
			transitionTimer -= Time.deltaTime;


			if (transitionTimer <= 0) {
				//StartGame ();
				async.allowSceneActivation = true;
			}
		}


	}
	 
	public void LoadGame(){
		startTimer = true;
		stBtnAnim.SetBool ("Clicked", true);
		gmStSound.Play ();

		if (psButton != null) {
			psButton.SetActive (false);
		}

		if (opButton != null) {
			opButton.SetActive (false);
		}


		GameMapAnim.SetBool ("StartedGame", true);
	}


	public void LoadStore(){
		startTimer = true;
		psBtnAnim.SetBool ("Clicked", true);
		gmStSound.Play ();
	}

	public void LoadOptions(){
		startTimer = true;
		opBtnAnim.SetBool ("Clicked", true);
		gmStSound.Play ();
	}




	public void StartGame(){
		SceneManager.LoadScene (CampaignMap);
	}


	public IEnumerator LoadCampaignMapAsync(){
		gmStSound.Play ();
		clip.Play ();
		startTimer = true;
		Debug.Log ("Loading Level");
		async = SceneManager.LoadSceneAsync(CampaignMap);
		async.allowSceneActivation = false;
		yield return async;
		Debug.Log ("Loading Complete");
	}

	//public void PauseGame(){
	//	Application.
	//}

	public void QuitGame(){
		Application.Quit();
	}
}
