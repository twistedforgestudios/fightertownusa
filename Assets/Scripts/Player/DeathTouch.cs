﻿using UnityEngine;
using System.Collections;

public class DeathTouch : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D other){

		Health health = other.gameObject.GetComponent<Health> ();

		if (health == null) {
			return;
		} else {
			if (GM.hitground) {
				GM.hitground=false;
				if (!GM.intunnel)
					health.Dying ();
			}
			else
				health.Dying ();
		}
	}


}
