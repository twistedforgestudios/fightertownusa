using System;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class RunWay : MonoBehaviour
    {

		public bool runway;
		static public bool runwayflag;
		public bool goup;
		static public int runwaytimer;
		static public int runwayrotatetimer;
		static public int runwayrotatetimerdelay;
		static public int runwaygodowntimer;

		public Renderer rend1;

		private Vector3 change;

        private void Awake()
        {
        }

        private void Start()
		{
			runwaytimer=120;
			runwaygodowntimer=40;
			runwayrotatetimerdelay=0;
			runwayrotatetimer=45;
			goup=true;

	    	runwayflag=runway;

			if (runway) {
				PlatformerCharacter2D.speeddec=20;
				//PlatformerCharacter2D.m_Anim.speed=0;
			}
			else {
		    	PlatformerCharacter2D.speeddec=0;
		    	//PlatformerCharacter2D.m_Anim.Play("plane", -1, (.5f*GM.skinsave));
		    	//PlatformerCharacter2D.m_Anim.speed=0;
			}
			MM.musicplay=0;



		}




        private void FixedUpdate()
        {
			if (runway) {
				GM.button_pressed_type=0;

				if (runwaytimer>1) {
					runwaytimer--;
				    PlatformerCharacter2D.mv=.0f;
				}
				else {
			    	PlatformerCharacter2D.acRotation=1;
					if (runwaytimer==1) {
						runwaytimer=0;
						//PlatformerCharacter2D.m_Anim.speed=1;
						PlatformerCharacter2D.mv=.6f;
					}
					if (runwayrotatetimerdelay!=0) 
						runwayrotatetimerdelay--;
					else {
						if (goup) {
							if (runwayrotatetimer!=0) {
								GM.button_pressed_type=1;
								runwayrotatetimer--;
							}
							else {
								runwayrotatetimerdelay=80;
								runwayrotatetimer=44;
								goup=false;

						    	PlatformerCharacter2D.m_Anim.speed=0;
							}
						}
						else {
							GM.button_pressed_type=2;
							

							if (runwayrotatetimer != 0) {
								runwayrotatetimer--;

							}
							else {
						    	//PlatformerCharacter2D.m_Anim.Play("plane", -1, .5f);
						    	//PlatformerCharacter2D.m_Anim.speed=0;

								runwayflag=runway;
								runway=false;
							}
						}
					}
				}

		    	PlatformerCharacter2D.mh=1.4f;

				if (PlatformerCharacter2D.speeddec!=0) {
			    	PlatformerCharacter2D.speeddec-=.10f;
					if (PlatformerCharacter2D.speeddec<0) {
						PlatformerCharacter2D.speeddec=0;
					}
				}

			}
		}
    }
}
