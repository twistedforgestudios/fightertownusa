﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public enum PowerUps { FullAuto, Multishot, Laserbeam, ExplodingBullets,
	                       Invulnerability, SpeedUp, Invisibility, Sheild};
	public PowerUps power;

	public float powerTime;
	private SpriteRenderer sprite;
	private Collider2D collider2d;
	// Use this for initialization
	void Start () {
		sprite = GetComponent<SpriteRenderer> ();
		collider2d = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D other){

		//this.gameObject.SetActive (false);
	    

	}

	void OnTriggerEnter2D(Collider2D col){
		

		PlayerPowerState pps = col.gameObject.GetComponent<PlayerPowerState> ();
		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();
		if (pps == null && cd == null) {
			return;
		} else {
			sprite.enabled = false;
			collider2d.enabled = false;
			EventManager.TriggerEvent ("PowerUpCollected");

			switch (power) {
			case PowerUps.FullAuto:
				pps.FullAuto (powerTime);
				cd.SetFullAutoText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.Multishot:
				pps.Multishot (powerTime);
				cd.SetMultishotText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.Laserbeam:
				pps.LaserBeam (powerTime);
				cd.SetLazerBeamText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.ExplodingBullets:
				pps.ExplodingBullets (powerTime);
				cd.SetExplodingShotText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.Invulnerability:
				pps.Invulnerabiliy (powerTime);
				cd.SetInvulnerabilityText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.SpeedUp:
				pps.Speedup (powerTime);
				cd.SetSpeedText ();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.Invisibility:
				pps.Invisibility (powerTime);
				cd.SetInvisibilityText();
				cd.PlayCargoAnim ();
				break;
			case PowerUps.Sheild:
				pps.Sheild (powerTime);
				cd.SetSheildText ();
				cd.PlayCargoAnim ();
				break;
			default:
				break;
			}

		}
	}
}
