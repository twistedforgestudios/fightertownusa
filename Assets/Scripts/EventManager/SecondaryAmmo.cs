﻿using UnityEngine;
using System.Collections;

public class SecondaryAmmo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		EventManager.TriggerEvent ("AcquireSecondaryAmmo");
		gameObject.SetActive (false);
	}
}
