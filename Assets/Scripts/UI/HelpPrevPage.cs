using UnityEngine;
using System.Collections;

public class HelpPrevPage : MonoBehaviour {

    public Renderer rend;
	public bool active;


	void OnMouseDown () {
		if (active) {
			Help.page--;
			if (Help.page==0)
				Help.newi=0;
			else
				Help.newi=Help.newisave[Help.page-1];

			Help.scrollpad=0;
		}
	}


	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();

	}
	
	// Update is called once per frame
	void Update () {
		if (Help.active==true && Help.page>0) {
			rend.enabled = true;
			active=true;
		}
		else {
			rend.enabled = false;
			active=false;
		}
	}
}
