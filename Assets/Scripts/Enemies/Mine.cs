﻿using UnityEngine;
using System.Collections;

public class Mine : MonoBehaviour {


	public Explosion explosion;

	public SpriteRenderer sprite;

	public  CircleCollider2D col2d;

	// Use this for initialization
	void Start () {
	//	sprite = GetComponent<SpriteRenderer> ();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D col){
		sprite.enabled = false;
		col2d.enabled = false;
		explosion.BeginExplosion ();

	}



}
