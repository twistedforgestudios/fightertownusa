﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinLabel : MonoBehaviour {
	public PlayerGameData playerData;
	public Text label;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {


		if (playerData.CurrentSkin == 0) {
			label.text = "Fighter Gray";
		}
		else if (playerData.CurrentSkin == 1) {
			label.text = "Fighter Blue";
		}
		else if (playerData.CurrentSkin == 2) {
			label.text = "Camo Green";
		}
		else if (playerData.CurrentSkin == 3) {
			label.text = "Artic Leopard";
		}
		else if (playerData.CurrentSkin == 4) {
			label.text = "Black Tiger";
		}
		else if (playerData.CurrentSkin == 5) {
			label.text = "Desert Eagle";
		}
		else if (playerData.CurrentSkin == 6) {
			label.text = "Flying Beagle";
		}



	}
}
