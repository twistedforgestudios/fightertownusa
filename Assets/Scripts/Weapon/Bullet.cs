﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public Vector2 movementVector;
	public float speed;
	private Rigidbody2D rgbd;
	public int damage;
	public float lifetime;
	public bool persistant;





	void OnEnable(){

		rgbd = GetComponent<Rigidbody2D>();

		movementVector = transform.TransformDirection (Vector2.right);
		rgbd.velocity = movementVector * speed;

		Invoke ("Destroy", lifetime);
	}

	void OnTriggerEnter2D(Collider2D other){



		if(!persistant)
			Destroy ();

		rgbd.velocity = Vector2.zero;
		//Debug.Log("Collided" + gameObject.name.ToString());

		Health targetHealth = other.transform.GetComponent<Health> ();
		if (targetHealth == null) {
		} else 
		{

			if (damage <= 0)
				targetHealth.Damage ();
			else
				targetHealth.Damage (damage);
		}

	}

	void OnCollisionEnter2D(Collision2D other){



		if(!persistant)
			Destroy ();

		rgbd.velocity = Vector2.zero;
		//Debug.Log("Collided" + gameObject.name.ToString());

		Health targetHealth = other.transform.GetComponent<Health> ();
		if (targetHealth == null) {
		} else 
		{

			if (damage <= 0)
				targetHealth.Damage ();
			else
				targetHealth.Damage (damage);
		}

	}







	void Destroy(){
		movementVector = transform.TransformDirection (Vector2.right);

		this.gameObject.SetActive (false);

	}


	void OnDisable(){
		CancelInvoke ();
	}
}
