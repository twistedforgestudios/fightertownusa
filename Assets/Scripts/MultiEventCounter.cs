﻿using UnityEngine;
using System.Collections;

public class MultiEventCounter : MonoBehaviour {


	public int eventCounterLimit;

	public string[] EventsToListen;
	public string MessageToSend;
	public string eventToTrigger;

	int eventCounter;




	// Use this for initialization
	void Start () {
	

		foreach( string s in EventsToListen){
			EventManager.StartListening (s, IncrementEventCounter);
		}

		eventCounter = 0;
	}
		

	void IncrementEventCounter(){

		eventCounter++;

		if (eventCounter >= eventCounterLimit) {


			foreach (string s in EventsToListen) {
				EventManager.StopListening (s, IncrementEventCounter);
			}

			if (MessageToSend != "")
				SendMessage (MessageToSend);	

			if (eventToTrigger != "")
				EventManager.TriggerEvent (eventToTrigger);
		}

	}



}
