﻿using UnityEngine;
using System.Collections;

public class HealthNumberUI : MonoBehaviour {
	public Health health;
	public NumberUI numberUi;

	// Use this for initialization
	void Start () {
		numberUi.number = health.HealthPoints;

	}
	
	// Update is called once per frame
	void Update () {
		numberUi.number = health.HealthPoints;
	}
}
