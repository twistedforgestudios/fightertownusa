﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class NumberUI : MonoBehaviour {
	
	public Image firstDigitImage;
	public Image tensDigitImage;
	public bool Decrementing;
	public Sprite[] numSprites;


	public string eventName;

	public int number;
	private int firstDigit;
	private int tensDigit;

	// Use this for initialization
	void Start () {
		firstDigit = number % 10;
		tensDigit = number / 10;
		firstDigitImage.sprite = numSprites [firstDigit];
		tensDigitImage.sprite = numSprites [tensDigit];

		if (Decrementing) {
			if (eventName != "") {
				EventManager.StartListening (eventName, DecrementNumber);
			}
		} else{

			if (eventName != "") {
			EventManager.StartListening (eventName, IncrementNumber);
		    }
		}



	}
	
	// Update is called once per frame
	void Update () {
		firstDigit = number % 10;
		tensDigit = number / 10;
		firstDigitImage.sprite = numSprites [firstDigit];
		tensDigitImage.sprite = numSprites [tensDigit];

	}


	public void IncrementNumber(){
		Debug.Log (gameObject.name);
		number++;
	}


	public void DecrementNumber(){
		number--;
	}
}
