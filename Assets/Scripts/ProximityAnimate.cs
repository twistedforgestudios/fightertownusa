﻿using UnityEngine;
using System.Collections;

public class ProximityAnimate : MonoBehaviour {


	Animation anim;
	public string animName;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animation> ();
	}
	



	 void OnTriggerEnter2D(Collider2D col){
		anim[animName].speed = 1;
		anim.Play (animName);
	}

	 void OnTriggerExit2D(Collider2D col){

		anim[animName].speed = -1;
		anim [animName].time = anim [animName].length;
		anim.Play (animName);
		Debug.Log ("Reverse");
	}


}
