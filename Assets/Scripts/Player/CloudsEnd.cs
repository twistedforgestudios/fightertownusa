using UnityEngine;
using System.Collections;
public class CloudsEnd : MonoBehaviour {


	static public bool show;		
	static public bool endlevel;		
	static public bool stopped;		
    public Renderer rend;



	void Start () {
		show=false;
        rend = GetComponent<Renderer>();
		stopped=false;
		rend.enabled = false;
		endlevel=false;

	}

	void Update () {
		if (endlevel) {
			if (Fade.fadeingOut==false) {
				Debug.Log ("Start Fade In");
				if (show==false) {
					show=true;
					PlaneEnd.show=true;
		    		Fade.fadeinstart=true;
					//Debug.Log ("Fade in Start");
					endlevel=false;
				}
			}
		}
		if (show==true) {

			rend.enabled = true;
            rend.sortingLayerName = "front";

			if (transform.localPosition.x>-30) {
		 //   	transform.Translate(-(Time.deltaTime*10.5f),0,0, Space.World);
		    	transform.Translate(-(Time.deltaTime*2.5f),0,0, Space.World);
			}
			else {
				PlaneEnd.rolltimer=0;
				stopped=true;
				Exhaust.exhaust_end=true;
			}
		}
		else
			rend.enabled = false;
    }
}

