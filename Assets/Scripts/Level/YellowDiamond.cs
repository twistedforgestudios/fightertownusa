﻿using UnityEngine;
using System.Collections;

public class YellowDiamond : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){

		CollectDisplay cd = col.gameObject.GetComponent<CollectDisplay> ();


		cd.SetYellowDiamondText ();
		cd.PlayCargoAnim ();

		gameObject.SetActive (false);
		EventManager.TriggerEvent ("AcquireYellowDiamond");
	}
}
