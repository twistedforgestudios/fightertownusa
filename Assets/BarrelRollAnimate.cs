﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class BarrelRollAnimate : MonoBehaviour {

	private Animator anim;
	public Animator comtrailAnim;
	public Animator exhaustAnim;
	private int skin;


	public float startTime;
	private float startTimer;
	private bool active;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		skin = GM.skinsave;

		EventManager.StartListening ("KilledAirEnemy", BarrelRoolAnimate);
		EventManager.StartListening ("KilledGroundEnemy", BarrelRoolAnimate);

		startTimer = startTime;

	}
	
	// Update is called once per frame
	void Update () {

		if (active) {

			startTimer -= Time.deltaTime;

			if (startTimer <= 0) {
				active = false;
				startTimer = startTime;
			}
		}

		if(PlatformerCharacter2D.falling){
			anim.speed = 2;
			startTimer = 0;
			BarrelRoolAnimate();
		}

	}


	public void BarrelRoolAnimate(){

		if (!active) {

			if (this.tag == "PlayerStealthed") {

				anim.SetTrigger ("StealthRoll");

				comtrailAnim.SetTrigger ("ComRoll");

				exhaustAnim.SetTrigger ("exhaust_roll");

			} else {
				anim.SetTrigger ("Skin" + (skin + 1) + "Roll");

				comtrailAnim.SetTrigger ("ComRoll");

				exhaustAnim.SetTrigger ("exhaust_roll");
			}
			active = true;
		}
	}



	public void BarrelRoolAnimate(float speed){

		if (!active) {

			if (this.tag == "PlayerStealthed") {

				anim.SetTrigger ("StealthRoll");

				comtrailAnim.SetTrigger ("ComRoll");

				exhaustAnim.SetTrigger ("exhaust_roll");

			} else {
				anim.SetTrigger ("Skin" + (skin + 1) + "Roll");

				comtrailAnim.SetTrigger ("ComRoll");

				exhaustAnim.SetTrigger ("exhaust_roll");
			}
			active = true;
		}
	}
}
