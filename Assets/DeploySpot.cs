﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeploySpot : MonoBehaviour {

	public Health paratrooperHealth;
	public WaveDeathEvent playerSavedEvent;
	public GameObject FailObject;
	public GameObject MissionObject;

	private BoxCollider2D col;

	// Use this for initialization
	void Start () {
		col = GetComponent<BoxCollider2D> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (paratrooperHealth != null) {
			if (!paratrooperHealth.isAlive) {
				playerSavedEvent.enabled = false;
				paratrooperHealth = null;
				EventManager.TriggerEvent ("EndRescueMission");

				if (FailObject != null) {
					FailObject.SetActive (true);

				}

				if (MissionObject != null) {
					
					MissionObject.SetActive (false);
				}

				//EventManager.TriggerEvent ("ScientistDied");
			}
		}

	}




	void OnCollisionEnter2D(Collision2D other){

		if (other.gameObject.tag == "Paratrooper") {
			col.enabled = false;
			EventManager.TriggerEvent ("StartRescueMission");
			paratrooperHealth = other.gameObject.GetComponent<Health> ();

		}


	}
}
