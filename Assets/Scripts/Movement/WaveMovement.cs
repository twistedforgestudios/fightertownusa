﻿using UnityEngine;
using System.Collections;

public class WaveMovement : MonoBehaviour {

	public int speed;
	public float dist;
	public float fequency;
	public float magnitude;
	private Vector3 axis;
	private Vector3 pos;

	private float time = 0;

	void OnEnable(){
		time = dist;
		pos = transform.position;
	}



	// Use this for initialization
	void Start () {
		time = dist;
		pos = transform.position;
		axis = transform.up;
	}

	// Update is called once per frame
	void Update () {
		//this.transform.Translate (direction * speed);
		time += Time.deltaTime;
		pos += transform.right * Time.deltaTime * speed;
		transform.position = pos + axis * Mathf.Sin (time * fequency) * magnitude;
	}


	void OnDisable(){
		time = 0;
		pos = transform.position;
	}


}
