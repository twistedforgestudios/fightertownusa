﻿using UnityEngine;
using System.Collections;

public class HeatGuage : MonoBehaviour {

	public Animator heatGuageAnim;


	public float maxHeat;
	public float minHeat;
	public float reductionSpeed;
	public float OverHeatTime;

	public bool overheated;

	public float curHeat;
	public float overHeatTimer;
	public float heatIncrease;
//	private float DecreaseTimer = 1.0f;
	// Use this for initialization
	void Start () {
		curHeat = 0;
		overheated = false;
		overHeatTimer = OverHeatTime;
	}
	
	// Update is called once per frame
	void Update () {

		heatGuageAnim.SetFloat ("HeatValue", curHeat);
		 //heatGuageAnim.SetBool("IsIncreasing", true);


		if (overheated) {
			overHeatTimer -= Time.deltaTime;
			if (overHeatTimer <= 0.0f) {
				LeaveOverHeatMode ();
			}
		} else {
			ReduceHeat (reductionSpeed * Time.deltaTime);
		}
	}

	public void IncreaseHeat(){

		curHeat += (heatIncrease * Time.deltaTime);
		curHeat = Mathf.Clamp (curHeat, minHeat, maxHeat);

		heatGuageAnim.SetFloat ("HeatValue", curHeat);
		heatGuageAnim.SetBool("IsIncreasing", true);

		heatGuageAnim.SetBool("MaxedOut", true);
	//	Debug.Log (heatGuageAnim.GetBehaviour<OverheatAnim> ().overheated.ToString ());;

		if (heatGuageAnim.GetBehaviour<OverheatAnim> ().overheated) {
			
			EnterOverHeatMode ();
		
		}
	}

	public void ReduceHeat(float heat){

		//if (!heatGuageAnim.GetBool ("IsDecreasing")) {
		//	curHeat -= (heat);
		//curHeat = Mathf.Clamp (curHeat, minHeat, maxHeat);
		//}

		if (curHeat >= minHeat) {
			curHeat -= (heat * Time.deltaTime);
			curHeat = Mathf.Clamp (curHeat, minHeat, maxHeat);
		}

		//if (curHeat = minHeat) {

			/*DecreaseTimer -= Time.deltaTime;

			if (DecreaseTimer <= 0.0f) {
				
				//heatGuageAnim.SetBool("IsDecreasing", true);
				DecreaseTimer = 1.0f;
				//curHeat += 1;
			}*/
		
		//} else {
			
		

	//	}


	}

	public void EnterOverHeatMode(){
		overheated = true;

	}

	public void LeaveOverHeatMode(){
		overheated = false;
		heatGuageAnim.GetBehaviour<OverheatAnim> ().overheated = false;
		heatGuageAnim.SetBool("MaxedOut", false);
		overHeatTimer = OverHeatTime;
		curHeat = 0.0f;
	}



}
