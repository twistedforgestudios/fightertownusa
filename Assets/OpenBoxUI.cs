﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenBoxUI : MonoBehaviour {

	public CargoMenu cargoMenu;
	public Button buttonSelectPower;
	// Use this for initialization
	void Start () {
		buttonSelectPower.interactable= false;
	}
		
	// Update is called once per frame
	void Update () {
		 
	}


	public void SetFullAuto(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.FullAuto));
	}

	public void SetMultiShot(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.Multishot));
	}

	public void SetLaserBeam(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.Laserbeam));
	}

	public void SetExplodingBullet(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddWeaponPowerUp(PlayerPowerState.WeaponPower.ExplodingBullets));
	}

	public void SetInvulnerability(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Invulnerability));
	}

	public void SetSpeedUp(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.SpeedUp));
	}

	public void SetInvisibility(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Invisibility));
	}


	public void SetShield(){
		buttonSelectPower.interactable= true;
		buttonSelectPower.onClick.RemoveAllListeners ();
		buttonSelectPower.onClick.AddListener( () => cargoMenu.AddAircraftPowerUp(PlayerPowerState.AircraftPower.Shield));
	}








}
