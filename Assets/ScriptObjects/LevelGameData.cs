﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Data", menuName = "Game/LevelData", order = 1)]
public class LevelGameData :ScriptableObject{

	public string Name;
	public bool Unlocked = false;
	public bool Completed = false;
	public int GroundEnemiesKilled = 0;
	public int AirEnemiesKilled = 0;
	public int PowerUpsCollected = 0;
	public int AlliesSaved = 0;
	public int SecondaryAmmoCollected = 0;
	public int SecondaryKills = 0;
	public int YellowDiamondsColleted = 0;
	public AchievementGameData[] Achievements;


	public void Initialize(LevelData ld){

		Name = ld.Name;
		Completed = ld.Completed;
		Unlocked = ld.Unlocked;
		GroundEnemiesKilled = ld.GroundEnemiesKilled;
		AirEnemiesKilled = ld.AirEnemiesKilled;
		PowerUpsCollected = ld.PowerUpsCollected;
		AlliesSaved = ld.AlliesSaved;
		YellowDiamondsColleted = ld.YellowDiamondsColleted;
	}



}
