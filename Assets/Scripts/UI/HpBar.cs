﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HpBar : MonoBehaviour {

	public Health health;
	public Image image;

	float percentHealthLeft;

	// Use this for initialization
	void Start () {
		percentHealthLeft = health.HealthPoints / health.MaxHealth;
	}
	
	// Update is called once per frame
	void Update () {
		percentHealthLeft = (float)health.HealthPoints / (float)health.MaxHealth;

		image.fillAmount = percentHealthLeft;

	}
}
