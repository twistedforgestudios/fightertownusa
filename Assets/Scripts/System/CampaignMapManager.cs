﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class CampaignMapManager : MonoBehaviour {

	public PlayerGameData playerData;
	//public RectTransform[] maps;
	public RectTransform map;
	public Image mapImage;
	int transitionSpeed = 50;
	bool TransitionLeftActive = false;
	bool TransitionRightActive = false;
	public int mapIndex = 0;

	private string currentlySelectedLevel;
	//AudioSource BGM;
	float newPosition;
//	Camera cam;


	AudioSource startSound;



	// Use this for initialization
	void Start () {
		//BGM = GetComponent<AudioSource> ();
	//	cam = Camera.main;
		//BGM.clip = MM.musicAudio [MM.musicnewsong];
	//	Application.targetFrameRate = 200;
		//BGM.volume = MM.musicvolumechangevalue;

		//BGM.Play ();
		startSound = GetComponent<AudioSource>();
			
		if(GameDataManager.instance != null){
			GameDataManager.instance.Load();
			GameDataManager.instance.LoadLevel (1);
			GameDataManager.instance.LoadLevel (2);
			GameDataManager.instance.LoadLevel (3);
			GameDataManager.instance.LoadLevel (4);
			GameDataManager.instance.LoadLevel (5);
			GameDataManager.instance.LoadLevel (6);
			GameDataManager.instance.LoadLevel (7);
			GameDataManager.instance.LoadLevel (8);
			GameDataManager.instance.LoadLevel (9);
			GameDataManager.instance.LoadLevel (10);
		}


	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}

		if (TransitionRightActive) 
		{
			//float newPosition = map.position.x + mapImage.sprite.bounds.size.x;

			map.Translate (map.right * transitionSpeed * Time.deltaTime);
			if (map.position.x >= newPosition) {
				TransitionRightActive = false;
				//mapIndex = 0;
			}
		}

		if (TransitionLeftActive) 
		{
			

			map.Translate (map.right * -transitionSpeed * Time.deltaTime);
			if (map.position.x <= newPosition) {
				TransitionLeftActive = false;
				//mapIndex = 0;
			}
		}


	}


	public void setLevel(string level){
		currentlySelectedLevel = level;
	}

	public void StartLevel(){


		//if(playerData.NumOfFuelCells > 0){
			LoadLevelAsync ();
			//Debug.Log ("Start Level");
		//}
	
		//GameDataManager.instance.Save ();
	}

	public void GotoScene(string name){

		SceneManager.LoadScene (name);
	}


	public void MoveMapLeft()
	{
		if (mapIndex <= 1)
		{
			newPosition = map.position.x - (mapImage.sprite.bounds.size.x * Screen.width/Screen.height)/2;
			TransitionLeftActive = true;
			mapIndex++;
		}
	}

	public void MoveMapRight(){
		if (mapIndex > 0) 
		{
			newPosition = map.position.x + (mapImage.sprite.bounds.size.x  * Screen.width/Screen.height)/2;

			TransitionRightActive = true;
			mapIndex--;
		}
	}

	public IEnumerator LoadLevelAsync(){
		startSound.Play ();
		playerData.DecrementFuelCell ();

		//GameDataManager.instance.Save ();
		Debug.Log ("Loading Level");
		AsyncOperation async = SceneManager.LoadSceneAsync(currentlySelectedLevel);
		//async.allowSceneActivation = false;

	  //   while(!async.isDone){
		//	Debug.Log("WAIT");
			//yield;
		// }

		yield return async;
		Debug.Log ("Loading Complete");
	}


	public void QuitGame(){
			Application.Quit();
	}

}
