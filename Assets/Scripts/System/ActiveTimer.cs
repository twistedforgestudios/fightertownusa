﻿using UnityEngine;
using System.Collections;

public class ActiveTimer : MonoBehaviour {

	public float awakeTime;
	public GameObject obj;

	public float awakeTimer;


	// Use this for initialization
	void Start () {
		awakeTimer = awakeTime;
	}

	// Update is called once per frame
	void Update () {

		if (!obj.activeInHierarchy) {

			awakeTimer -= Time.deltaTime;

			if (awakeTimer <= 0.0f) {
				obj.SetActive (true);
				awakeTimer = awakeTime;
			}

		}
	}

	void OnDisable(){
		awakeTimer = awakeTime;
	}
		
}
