﻿using UnityEngine;
using System.Collections;

public class ForwardMovement : MonoBehaviour {

	public Vector3 direction;
	public int speed;


	public bool stops;
	public float moveTime;

	private bool isMoving;
	private float moveTimer;


	void OnEnable(){
		isMoving = true;

		moveTimer = moveTime;
	}


	// Use this for initialization
	void Start () {


		isMoving = true;

		moveTimer = moveTime;
	}
	
	// Update is called once per frame
	void Update () {

		if (stops) {
			if (isMoving) {
				this.transform.Translate (direction * Time.deltaTime * speed);

				moveTimer -= Time.deltaTime;

				if (moveTimer <= 0.0f) {
					isMoving = false;
				}
			}
		} else {
			this.transform.Translate (direction * Time.deltaTime * speed);
		}



	}
}
