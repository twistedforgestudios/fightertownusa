using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Fade : MonoBehaviour {

	static public bool Exiting;
	public Color color = Color.black;
	public Texture2D blackScreen; // add a black texture here
	public float fadeTime; // how long you want it to fade?
	public float timer = 0;
	static public bool fadeIn;
	public bool fadeing;
	static public bool fadeingOut;
	static public bool fadeinstart;

	void Start () {
		fadeing=false;
	}

	private void FadeOut() {
		timer = fadeTime;
		Debug.Log ("Setting fade out Time");
	}

	public void FadeIn() {
		timer = fadeTime;
		fadeIn = true;
		fadeing=true;
		fadeinstart=false;
		Debug.Log ("Setting Fade In Time");
	}

	private void OnGUI () {
		if (fadeing) {
			if (fadeIn)
			{
				color.a = timer / fadeTime;
				GUI.color = color;

			}
			else
			{
				color.a = 1 - (timer / fadeTime);
				//Debug.Log ("Fadding IN");
			}
			GUI.color = color;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackScreen);
		}

	}


	// Update is called once per frame
	void Update () {
		//Debug.Log (fadeingOut);
		timer -= Time.deltaTime;

		if (timer <= 0)
			timer = 0;

		if (timer==0 && fadeingOut) {
			fadeingOut=false;
			Debug.Log ("scen out");
			if (GM.scene != "-1") {
				CloudsEnd.show = false;
				PlaneEnd.show = false;
				SceneManager.LoadScene (GM.scene);

			}
		}


		if (GM.Exiting==true) {
			
			GM.Exiting=false;
			fadeing=true;
			Debug.Log ("Exiting");
			fadeingOut=true;
			FadeOut();
			fadeIn=false;
			MM.musicstop=1;
		}
		if (fadeinstart) {
			fadeinstart=false;
			FadeIn();
		//	Debug.Log ("start Fade In");
		}

	}

}
