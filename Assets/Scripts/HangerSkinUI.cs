﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Purchasing;
using System.Collections.Generic;

public class HangerSkinUI : MonoBehaviour {

	public PurchaserUI store;
	public Image planeImage;
	public Dropdown skinDropDown;
	public Animator SkinHangarAnim;
	public PlayerGameData playerData;
	// Use this for initialization
	public GameObject displayPrompt;

	public bool boughtFighterBlue;
	public bool boughtCamogreen;
	public bool boughtArticLeopard;
	public bool boughtBlackTiger;
	public bool boughtDesertEagle;
	public bool boughtFlyingBeagle;

	void Start () {

		boughtFighterBlue = playerData.boughtFighterBlue;
		boughtCamogreen = playerData.boughtCamogreen;
		boughtArticLeopard = playerData.boughtArticLeopard;
		boughtBlackTiger = playerData.boughtBlackTiger;
		boughtDesertEagle = playerData.boughtDesertEagle;
		boughtFlyingBeagle = playerData.boughtFlyingBeagle;

	}
	
	// Update is called once per frame
	void Update () {






	}


	public void checkAvailableSkins(){

			if (IAPButton.IAPButtonStoreManager.Instance.CheckPreviousPurchases (PurchaserUI.FighterBlueProductID)) {
				boughtFighterBlue = true;
				
			} else {
				boughtFighterBlue = false;
				
			}

			if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.CamoGreenProductID)) {
				boughtCamogreen = true;
				
			} else {
				boughtCamogreen = false;
				
			}

			if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.ArticLeopardProductID)) {
				boughtArticLeopard = true;
				
			} else {
				boughtArticLeopard = false;
				
			}

			if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.BlackTigerProductID)) {
				boughtBlackTiger = true;
				
			} else {
				boughtBlackTiger = false;
				
			}

			if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.DesertEagleProductID)) {
				boughtDesertEagle = true;
				
			} else {
				boughtDesertEagle = false;
				
			} 

			if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.FlyingBeagleProductID)) {
				boughtFlyingBeagle = true;
				

			} else {
				boughtFlyingBeagle = false;
				
			}




	}

	public void SelectSkin(){
		
		if (skinDropDown.value == 0) {
			playerData.CurrentSkin = skinDropDown.value;
			SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
			planeImage.color = Color.white;
			displayPrompt.SetActive (false);

		}
		else if (skinDropDown.value == 1) {
			if (boughtFighterBlue) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}

		} 
		else if (skinDropDown.value == 2) {
			if (boughtCamogreen) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive(true);
			}
		}
		else if (skinDropDown.value == 3) {
			if (boughtArticLeopard) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
		else if (skinDropDown.value == 4) {
			if (boughtBlackTiger) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
		else if (skinDropDown.value == 5) {
			if (boughtDesertEagle) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}	


		}
		else if (skinDropDown.value == 6) {
			if (boughtFlyingBeagle) {
				playerData.CurrentSkin = skinDropDown.value;
				SkinHangarAnim.SetInteger ("Skin", playerData.CurrentSkin);
				planeImage.color = Color.white;
				displayPrompt.SetActive (false);
			} else {
				//show that the skins is not available
				SkinHangarAnim.SetInteger ("Skin", skinDropDown.value);
				planeImage.color = Color.gray;
				displayPrompt.SetActive (true);
			}


		}
	}



}
