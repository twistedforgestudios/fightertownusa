﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityStandardAssets._2D;

public class PlayerUpgrades : MonoBehaviour {


	public PlayerGameData playerData;
	public HeatGuage heat;
	public Health health;
	public PlatformerCharacter2D player;

	// Use this for initialization
	void Start () {


		health.MaxHealth += playerData.ArmorLevel;

		health.HealthPoints = health.MaxHealth;

		GM.secondary_weapon_uses += (playerData.AmmoLevel * 2);

		player.rotationSpeed += (playerData.WingLevel * 10f);


		heat.OverHeatTime -= (playerData.HeatLevel * 0.30f);

		heat.heatIncrease -= (playerData.HeatLevel* 0.30f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
