﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets._2D;

public class PlayerPowerState : MonoBehaviour {

	public int PowerUpsAcquired;

	[System.Serializable]
	public enum WeaponPower { Normal, FullAuto, Multishot, Laserbeam, ExplodingBullets };

	[System.Serializable]
	public enum AircraftPower { Normal, Invulnerability, SpeedUp, Invisibility, Shield}

	[SerializeField]
	static public WeaponPower curWeaponPower;
	public AircraftPower curAircraftPower;

	public float weaponPowerTime;
	public float aircraftPowerTime;

	public bool weaponPowerActive;
	public bool aircraftPowerActive;

	public Weapon weapon;
	public Health health;
	public GameObject sheild;

	public float playerSpeedMulti = 1;
	public float playerSpeedBoost;
	private float oriFireSpeed;


	public  Sprite invisiSprite;

	private Sprite defSprite;

	private Animator playerAnimator;


//	private SpriteRenderer sprite;

	public WeaponPowerUpStateUI powerstateUI;


	// Use this for initialization
	void Start () {
		curWeaponPower = WeaponPower.Normal;
		curAircraftPower = AircraftPower.Normal;
		PowerUpsAcquired = 0;
		health = GetComponent<Health> ();
	//	anim = GetComponent<Animator> ();
		//sprite = GetComponent<SpriteRenderer> ();

		playerAnimator = GetComponent<Animator> ();


	}
	
	// Update is called once per frame
	void Update () {

		if (weaponPowerActive) {

			weaponPowerTime -= Time.deltaTime;
			if (weaponPowerTime <= 0) {
				EndWeaponPower(curWeaponPower);
			}
		}

		if (aircraftPowerActive) {
			
			aircraftPowerTime -= Time.deltaTime;
			if (aircraftPowerTime <= 0) {
				EndAircraftPower (curAircraftPower);
			}
		}
	
	}

	void StartWeaponPower(){
		weaponPowerActive = true;
		PowerUpsAcquired++;

		if (powerstateUI != null && powerstateUI.image != null) {
			//Debug.Log(curWeaponPower);
			powerstateUI.SetWeaponUI (curWeaponPower);
		}
	
	}

	void StartAircraftPower(){
		aircraftPowerActive = true;
		PowerUpsAcquired++;
	
	}

	void EndWeaponPower(WeaponPower wp){
		weaponPowerActive = false;
		weaponPowerTime = 0;
		curWeaponPower = WeaponPower.Normal;


		if (powerstateUI != null && powerstateUI.image != null) {
			powerstateUI.SetWeaponUI (curWeaponPower);
		}


		if (wp == WeaponPower.FullAuto) {
			weapon.fireRate = oriFireSpeed;
			weapon.heatEnabled = true;
		} else if (wp == WeaponPower.Multishot) {
			weapon.tripleShootMode = false;
		} else if (wp == WeaponPower.Laserbeam) {
			weapon.laserShootMode = false;
			weapon.lazerBullet.SetActive (false);
		} else if (wp == WeaponPower.ExplodingBullets) {
			weapon.explodeShootMode = false;
		}	

	}

	void EndAircraftPower(AircraftPower ap){
		aircraftPowerActive = false;
		aircraftPowerTime = 0;
		curAircraftPower = AircraftPower.Normal;


		if (ap == AircraftPower.Invulnerability)
		{
			health.invulnerble = false;
		}
		else if (ap == AircraftPower.SpeedUp)
		{
			playerSpeedMulti = 1;
		} 
		else if (ap == AircraftPower.Invisibility)
		{
			gameObject.tag= "Player";
			//anim.SetBool ("stealth", false);
			playerAnimator.SetTrigger("unStealth");
			//sprite.sprite = defSprite;
		
		}


	}

	public void NormalWeapon(){
		curWeaponPower = WeaponPower.Normal;
	}

	public void FullAuto(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.FullAuto;
	//	Debug.Log(curWeaponPower.ToString());
		weaponPowerTime += time;

		oriFireSpeed = weapon.fireRate;
		weapon.fireRate = 10;
		weapon.heatEnabled = false;
		StartWeaponPower ();
	}

	public void Multishot(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.Multishot;
		weaponPowerTime += time;
		weapon.tripleShootMode = true;
		StartWeaponPower ();

	}

	public void LaserBeam(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.Laserbeam;
		weapon.lazerBullet.SetActive (true);
		weapon.laserShootMode = true;
		weaponPowerTime += time;
		StartWeaponPower ();
	}
		
	public void ExplodingBullets(float time){
		EndWeaponPower (curWeaponPower);
		curWeaponPower = WeaponPower.ExplodingBullets;
		weaponPowerTime += time;
		weapon.explodeShootMode = true;
		StartWeaponPower ();
	}

	public void NormalAircraft(float time){
		curAircraftPower = AircraftPower.Normal;
	}

	public void Invulnerabiliy(float time){

		if (health == null)
			return;
		
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Invulnerability;
		aircraftPowerTime += time;
		health.invulnerble = true;
		StartAircraftPower ();
	}

	public void Speedup(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.SpeedUp;
		aircraftPowerTime += time;
		playerSpeedMulti = playerSpeedBoost;
		StartAircraftPower ();
	}

	public void Invisibility(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Invisibility;
		aircraftPowerTime += time;
		gameObject.tag= "PlayerStealthed";
		//PlatformerCharacter2D.m_Anim.speed=1;
		//anim.SetBool ("stealth", true);
		//defSprite = sprite.sprite;
		//sprite.sprite = invisiSprite;
		playerAnimator.SetTrigger("Stealth");
		StartAircraftPower ();
	}

	public void Sheild(float time){
		EndAircraftPower (curAircraftPower);
		curAircraftPower = AircraftPower.Shield;
		sheild.SetActive (true);
		sheild.GetComponent<Health> ().isAlive = true;
		sheild.GetComponent<SpriteRenderer> ().enabled = true;
		aircraftPowerTime += time;
		StartAircraftPower ();
	}




	public void DisplayPowerUpActivated(PowerUp.PowerUps power){


		switch (power) {
		case PowerUp.PowerUps.FullAuto:
			
			break;
		case PowerUp.PowerUps.Multishot:
	
			break;
		case PowerUp.PowerUps.Laserbeam:
			
			break;
		case PowerUp.PowerUps.ExplodingBullets:
			
			break;
		case PowerUp.PowerUps.Invulnerability:

			break;
		case PowerUp.PowerUps.SpeedUp:

			break;
		case PowerUp.PowerUps.Invisibility:
			
			break;
		case PowerUp.PowerUps.Sheild:

			break;
		default:
			break;
		}





	}





}
