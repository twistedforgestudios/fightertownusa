﻿using UnityEngine;
using System.Collections;

public class SheildEmitter : MonoBehaviour {


	SpriteRenderer sprite;
	CircleCollider2D cirCol;
	public Health health;
	AudioSource sound;
	public float shieldTime;
	float shieldTimer;
	bool shieldActive;

	// Use this for initialization
	void Start () {
		sprite = GetComponent<SpriteRenderer> ();
		cirCol = GetComponent<CircleCollider2D> ();
		sound = GetComponent<AudioSource> ();
		shieldActive = false;
		shieldTimer = shieldTime;
	}
	
	// Update is called once per frame
	void Update () {

		if (shieldActive) {
			shieldTimer -= Time.deltaTime;

			if (shieldTimer <= 0.0f) {
				Deactivate ();
				shieldActive = false;
				shieldTimer = shieldTime;
			}
		}
	
	}

	public void Shoot(){
		Activate ();
	}


	public void Activate(){
		sprite.enabled = true;
		cirCol.enabled = true;
		ActivateSheildTimer ();
		sound.Play ();
		if(health !=null)
			health.invulnerble = true;
	}

	public void Deactivate(){
		sprite.enabled = false;
		cirCol.enabled = false;
		if(health !=null)
			health.invulnerble = false;
	}

	public void ActivateSheildTimer(){
		shieldActive = true;
	}
}
