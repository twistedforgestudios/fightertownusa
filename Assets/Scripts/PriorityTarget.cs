﻿using UnityEngine;
using System.Collections;

public class PriorityTarget : MonoBehaviour {


	void OnTriggerEnter2D(Collider2D col){

		FaceTowardObject targeter = col.GetComponent<FaceTowardObject> ();

		if (targeter != null) 
			targeter.AcquireTarget (transform);
	}




}
