using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlaneEnd : MonoBehaviour {


	public Animator m_Anim;          
    public Renderer rend;
	static public bool show;   
	static public bool kill;  
	static public bool rolling;  
	static public bool rollehaust;  

	static public int rolltimer;

	static public int mission;		


	public GUIStyle customButton;
	public GUIStyle customButton0;
	public int screenwoffset;
	public int screenhoffset;
	public int heightoffset;
	public int widthoffset;
	//private Vector3 coords;


	static public int ypos;		
	static public int yposoff;		

	public bool dorolldemostarted;



	void Start () {
        rend = GetComponent<Renderer>();
		rend.enabled = false;
		kill=false;
		m_Anim.speed=0;
		rolltimer=0;
		rolling=true;

		//coords = Camera.main.WorldToScreenPoint(transform.position);
		screenwoffset=Screen.width;
		screenhoffset=Screen.height;
		heightoffset=387;
		widthoffset=765;


		ypos=290;
		yposoff=30;

	}

private void OnGUI () {
}


	void Update () {
		/*

		if (Input.GetKey(KeyCode.C)) {
			SceneManager.LoadScene ("achievement");
			print("here");
		}  

		if (Input.GetKey(KeyCode.S)) {
			GM.skinsave++;
			if (GM.skinsave==2)
				GM.skinsave=0;
		}  
*/

 //   	if (Input.GetKey(KeyCode.K)) {
 //   		SceneManager.LoadScene ("main");
 //   		print("main");
 //   	}  



		if (kill) {
			print("kill");
			rend.enabled = false;
			kill=false;
			show=false;
		}

		if (show==true) {

			m_Anim.speed=2;


			if (rolltimer!=0) {
				if (rolling) {
					m_Anim.Play("plane_end", -1, .5f*GM.skinsave);
					Exhaust.roll=true;
					rolling=false;
					dorolldemostarted=true;

					m_Anim.speed=2;


				}
			}
	    	rend.enabled = true;

			if (dorolldemostarted) {

                int animationIndex = ((int)(m_Anim.GetCurrentAnimatorStateInfo(0).normalizedTime * (48))) % 48;
				if (animationIndex>(22*(GM.skinsave+1))) {
					rolling=true;
		        	dorolldemostarted=false;
				}
			}

	//    	if (CloudsEnd.stopped==true)
	//        	m_Anim.SetBool("stopped", true);
		}
    }
}


