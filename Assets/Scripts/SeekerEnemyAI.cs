﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class SeekerEnemyAI : MonoBehaviour {


	private AudioSource deathsound;
	private Collider2D col;
	private ParticleSystem ps;

	private Health health;
	private bool dead;

	public string targetTag;
	public string EventName;
//	private FollowTarget moveScript;

	// Use this for initialization
	void Start () {
		health = GetComponent<Health> ();
		deathsound = GetComponent<AudioSource> ();
		col = GetComponent<Collider2D> ();
		ps = GetComponent<ParticleSystem> ();
		//moveScript = GetComponent<FollowTarget> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (!health.isAlive && !dead) {
			StartCoroutine ("DeathAnimateCo");
			dead = true;
			col.enabled = false;
		}
	}

	IEnumerator DeathAnimateCo(){

		ps.Play ();
		if (!deathsound.isPlaying) {
			deathsound.Play ();
		}
		yield return new WaitForSeconds(deathsound.clip.length);
	}



}
