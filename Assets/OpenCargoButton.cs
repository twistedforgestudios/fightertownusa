﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OpenCargoButton : MonoBehaviour {

	public Button openBoxBtn;
	private Button openMenuBtn;

	private CargoTimer cargoTime;

	// Use this for initialization
	void Start () {
		openMenuBtn = GetComponent<Button> ();
		cargoTime = GameDataManager.instance.cargoTime;

		//menu.cargoBoxesUI.number 
		if (cargoTime.CargoReady && cargoTime.playerData.CargoBoxes > 0) {
			openMenuBtn.interactable = true;
		} else {
			openMenuBtn.interactable = false;
		}

		openBoxBtn.onClick.AddListener( () => OpenCargoBox());
	}
	
	// Update is called once per frame
	void Update () {
		if (cargoTime.CargoReady & cargoTime.playerData.CargoBoxes > 0 ) {
			openMenuBtn.interactable = true;
		} else {
			openMenuBtn.interactable = false;
		}

	}


	public void OpenCargoBox(){
		
		cargoTime.UseCargoBox ();
	}
}
