﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorTintInterval : MonoBehaviour {
	public Color colorTint;
	public float intervalTime;
	private SpriteRenderer SprRender;
	public Material tintedMat;
	public Material oriMat;

	// Use this for initialization
	void Start () {
		SprRender = GetComponent<SpriteRenderer> ();



		Invoke ("TintColor", intervalTime);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TintColor(){
		SprRender.color = colorTint;
		SprRender.material = tintedMat;
		Invoke ("UntintColor", intervalTime);
	}
		
	public void UntintColor(){
		SprRender.color = Color.white;
		SprRender.material = oriMat;
		Invoke ("TintColor", intervalTime);
	}
}
