using UnityEngine;
using System.Collections;

public class AchievementMap : MonoBehaviour {

	public int screenwoffset;
	public int screenhoffset;
	public int heightoffset;
	public int widthoffset;

	static public int enemyunits_Air;
	static public int secondary_kills;
	static public int enemyunits_Terrain;
	static public int enemyunits_Infantry;
	static public int powerups_count;
	static public int diamonds_count;
	static public int completed;
	static public int bonuscompleted;
	static public int secondaryreloads;
	static public int allies_protected;
	static public int achievements;
	static public int stages;

	private Vector3 coords;

	public GUIStyle customButton;
	public GUIStyle customButton0;
	public GUIStyle customButton1;

	public LevelGameData levelgamedata1;
	public LevelGameData levelgamedata2;
	public LevelGameData levelgamedata3;
	public LevelGameData levelgamedata4;
	public LevelGameData levelgamedata5;
	public LevelGameData levelgamedata6;
	public LevelGameData levelgamedata7;
	public LevelGameData levelgamedata8;
	public LevelGameData levelgamedata9;
	public LevelGameData levelgamedata10;


	public float size;
	public bool update;

	// Use this for initialization
	void Start () {


		screenwoffset=Screen.width;
		screenhoffset=Screen.height;

		heightoffset=387;
		widthoffset=765;
		coords = Camera.main.WorldToScreenPoint(transform.position);

		size=37.5f;
		
		Fade.fadeinstart=true;

		enemyunits_Air=levelgamedata1.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata2.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata3.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata4.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata5.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata6.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata7.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata8.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata9.AirEnemiesKilled;
		enemyunits_Air+=levelgamedata10.AirEnemiesKilled;

		enemyunits_Terrain=levelgamedata1.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata2.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata3.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata4.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata5.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata6.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata7.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata8.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata9.GroundEnemiesKilled;
		enemyunits_Terrain+=levelgamedata10.GroundEnemiesKilled;

		powerups_count=levelgamedata1.PowerUpsCollected;
		powerups_count+=levelgamedata2.PowerUpsCollected;
		powerups_count+=levelgamedata3.PowerUpsCollected;
		powerups_count+=levelgamedata4.PowerUpsCollected;
		powerups_count+=levelgamedata5.PowerUpsCollected;
		powerups_count+=levelgamedata6.PowerUpsCollected;
		powerups_count+=levelgamedata7.PowerUpsCollected;
		powerups_count+=levelgamedata8.PowerUpsCollected;
		powerups_count+=levelgamedata9.PowerUpsCollected;
		powerups_count+=levelgamedata10.PowerUpsCollected;

		diamonds_count=levelgamedata1.YellowDiamondsColleted;
		diamonds_count+=levelgamedata2.YellowDiamondsColleted;
		diamonds_count+=levelgamedata3.YellowDiamondsColleted;
		diamonds_count+=levelgamedata4.YellowDiamondsColleted;
		diamonds_count+=levelgamedata5.YellowDiamondsColleted;
		diamonds_count+=levelgamedata6.YellowDiamondsColleted;
		diamonds_count+=levelgamedata7.YellowDiamondsColleted;
		diamonds_count+=levelgamedata8.YellowDiamondsColleted;
		diamonds_count+=levelgamedata9.YellowDiamondsColleted;
		diamonds_count+=levelgamedata10.YellowDiamondsColleted;

		secondaryreloads=levelgamedata1.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata2.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata3.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata4.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata5.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata6.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata7.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata8.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata9.SecondaryAmmoCollected;
		secondaryreloads+=levelgamedata10.SecondaryAmmoCollected;

		allies_protected=levelgamedata1.AlliesSaved;
		allies_protected+=levelgamedata2.AlliesSaved;
		allies_protected+=levelgamedata3.AlliesSaved;
		allies_protected+=levelgamedata4.AlliesSaved;
		allies_protected+=levelgamedata5.AlliesSaved;
		allies_protected+=levelgamedata6.AlliesSaved;
		allies_protected+=levelgamedata7.AlliesSaved;
		allies_protected+=levelgamedata8.AlliesSaved;
		allies_protected+=levelgamedata9.AlliesSaved;
		allies_protected+=levelgamedata10.AlliesSaved;

		completed=0;
		if (levelgamedata1.Completed)
			completed++;
		if (levelgamedata2.Completed)
			completed++;
		if (levelgamedata3.Completed)
			completed++;
		if (levelgamedata4.Completed)
			completed++;
		if (levelgamedata5.Completed)
			completed++;
		if (levelgamedata6.Completed)
			completed++;
		if (levelgamedata7.Completed)
			completed++;
		if (levelgamedata8.Completed)
			completed++;
		if (levelgamedata9.Completed)
			completed++;
		if (levelgamedata10.Completed)
			completed++;
		
		secondary_kills = levelgamedata1.SecondaryKills;
		secondary_kills+=levelgamedata2.SecondaryKills;
		secondary_kills+=levelgamedata3.SecondaryKills;
		secondary_kills+=levelgamedata4.SecondaryKills;
		secondary_kills+=levelgamedata5.SecondaryKills;
		secondary_kills+=levelgamedata6.SecondaryKills;
		secondary_kills+=levelgamedata7.SecondaryKills;
		secondary_kills+=levelgamedata8.SecondaryKills;
		secondary_kills+=levelgamedata9.SecondaryKills;
		secondary_kills+=levelgamedata10.SecondaryKills;




		achievements=0;
		bonuscompleted=0;
		stages=0;

/*
		public string Name;
	public bool Unlocked = false;
	public bool Completed = false;
	public int GroundEnemiesKilled = 0;
	public int AirEnemiesKilled = 0;
	public int PowerUpsCollected = 0;
	public int AlliesSaved = 0;
	public int SecondaryAmmoCollected = 0;

	public int YellowDiamondsColleted = 0;
	public AchievementGameData[] Achievements;
 */
    	update=true;

	}


private void OnGUI () {

	customButton.fontSize = Mathf.RoundToInt (20f * Screen.height / (heightoffset * 1.0f));
	customButton0.fontSize = Mathf.RoundToInt (12f * Screen.height / (heightoffset * 1.0f));
	customButton1.fontSize = Mathf.RoundToInt (10f * Screen.height / (heightoffset * 1.0f));

//	GUI.Label(new Rect(coords.x-100*screenhoffset / (heightoffset * 1.0f), screenhoffset-coords.y-(180*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), "Achievements Page", customButton);

	for (int i=0;i!=9;i++) {  
		GUI.Label(new Rect(coords.x-180*screenhoffset / (heightoffset * 1.0f), coords.y-((160-(i*size))*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), "\u0022"+ names[i]+"\u0022", customButton0);
		GUI.Label(new Rect(coords.x-180*screenhoffset / (heightoffset * 1.0f), coords.y-((145-(i*size))*screenhoffset / (heightoffset * 1.0f)), 1, 300*screenhoffset / (heightoffset * 1.0f)), desc[i], customButton1);
	}
}
string[] names = new string[9] {
	"Ace", 
	"Death from Above", 
	"Campaign Commander",
	"Extra Mile", 
	"Always Loaded",
	"Over Powered", 
	"Earth Mover", 
	"Guardian Angel",
	"Mr. Topgun"
	};

string[] desc = new string[9] {
	"Destroy enemy air units", 
	"Kills with secondary weapons", 
	"Complete all levels",
	"Complete all bonus missions", 
	"Collect secondary reloads",
	"Collect primary power ups",
	"Destroy enemy terrain units", 
	"Protect allied units", 
	"Complete all achievements"
	};


	/*
string[] levels = new string[5] {
	"Air", 
	"Ground", 
	"Infantry", 
	"Power Ups",
	"Diamonds" 
	};

*/
	void Update () {
		if (update) {
	    	Wings.update=true;
			update=false;
		}
	}
}
