﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

	public int HealthPoints;
	public int MaxHealth = 5;
	public bool isAlive;
	public bool invulnerble;
	public string OnHitFunctionName;
	public string OnDeathFuncName;
	public int deathDurTime;

	public AudioSource DamageSound;
	public AudioSource DeathSound;
	private SpriteRenderer sprite;

	public  bool haveInvincibilityFrames = false;
	private bool invincibiltyFramesActive = false;
	public float invincibilityTime = 0;
	private float invincibilityTimer = 0;


	void OnEnable(){

		HealthPoints = MaxHealth;
		isAlive = true;

		invincibilityTimer = invincibilityTime;

		if (sprite != null) {
			sprite.enabled = true;
		}

	}
		
	// Use this for initialization
	void Start () {


		invincibilityTimer = invincibilityTime;

		sprite = GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (invincibiltyFramesActive) {

			invincibilityTimer -= Time.deltaTime;

			if (invincibilityTimer <= 0f) {
				invincibiltyFramesActive = false;
				invulnerble = false;
				invincibilityTimer = invincibilityTime;

			}
				
		}

	}

	//Reduce health by 1
	public void Damage(){

		if (invulnerble) {
			return;
		}

		SetInvulnerability ();

		if (OnHitFunctionName != "") {
			this.SendMessage (OnHitFunctionName);
		}


		HealthPoints--;
		HealthPoints = Mathf.Clamp (HealthPoints, 0, MaxHealth);

		if (HealthPoints == 0 && isAlive) {
			//Debug.Log ("Am Dying");
			Dying();
		}
	}

	//Reduce Health by damage amount
	public void Damage(int damage){

		if (invulnerble) {
			return;
		}

		if (haveInvincibilityFrames) {
			invulnerble = true;
			invincibiltyFramesActive = true;
		}

		if (OnHitFunctionName != "") {
			this.SendMessage (OnHitFunctionName);
		}

		if(DamageSound!=null)
			DamageSound.Play ();


		HealthPoints -= damage;

		HealthPoints = Mathf.Clamp (HealthPoints, 0, MaxHealth);




		if (HealthPoints == 0 && isAlive) {
			Dying ();
			//Debug.Log ("Am Dying");
		}
			
	}


	public void Die(){
			HealthPoints = 0;
			this.gameObject.SetActive (false);
	//Debug.Log("Enemykilled");
	}

	public void PlayerDeath(){
		EventManager.TriggerEvent ("PlayerDeath");
	}

	public void Dying(){

		    HealthPoints = 0;
			isAlive = false;

		if(OnDeathFuncName != "")
		    this.SendMessage (OnDeathFuncName);

		Invoke("Die",deathDurTime);
	
		if (DeathSound != null) {
			DeathSound.Play ();
		}

		if (sprite != null) {
			sprite.enabled = false;
		}

			if (transform.childCount >= 0) {
				foreach (Transform t in transform.GetComponentInChildren<Transform>()) {
					t.gameObject.SetActive (false);
				}
			}
		}



	public void SetInvulnerability(){

		if (haveInvincibilityFrames) {
			invulnerble = true;
			invincibiltyFramesActive = true;
		}
	}
}
