﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Facebook.Unity;
using Facebook.MiniJSON;

public class FacebookManager : MonoBehaviour {

	private static FacebookManager _instance;

	public Text usernameText;
	public string get_data;
	public string fbname;


	public FacebookManager instance
	{
		get{
			if (_instance == null) {
				GameObject fbm = new GameObject ("FBMananger");
				fbm.AddComponent<FacebookManager> ();
			}

			return _instance;
		}
	}

	public bool isLoggedin{ get; set;}
		
	

	void Awake(){
		DontDestroyOnLoad(this.gameObject);

		_instance = this;
	

		if (!FB.IsInitialized) {
			FB.Init (SetInit, OnHideUnity);
		} else {
			isLoggedin = FB.IsLoggedIn;
			FB.ActivateApp ();
		}


	}

	public void Login(){
		FB.LogInWithReadPermissions ();
	}

	public void OnLogin(){
		//FB.
	}



	public void Share(){
		FB.ShareLink (
			contentTitle: "FighterTownUSA page",
			contentURL: new System.Uri ("https://www.google.com"),
			contentDescription: "Heres a link to google",
			callback: OnShare);
	}


	private void OnShare(IShareResult result){

		if (result.Cancelled || !string.IsNullOrEmpty (result.Error)) {
			Debug.Log ("ShareLink error: " + result.Error);
		} else if (!string.IsNullOrEmpty (result.PostId)) {
			Debug.Log (result.PostId);
		} else {

		}
	}

	void SetInit(){
		
		if (FB.IsLoggedIn) {
			//Debug.Log ("FB is Logged in");
			//Debug.Log (AccessToken.CurrentAccessToken.UserId);
			//string url = "https" + "://graph.facebook.com/"+ AccessToken.CurrentAccessToken.UserId +"/picture";
			//url += "?access_token=" + AccessToken.CurrentAccessToken.TokenString;
			//WWW www = new WWW(url);
			//usernameText.text = "Logged in";
			//yield return www;


		} else {
			//Login ();
			//Debug.Log ("FB is not Logged in");
			usernameText.text = "not Logged in";
		}

		isLoggedin = FB.IsLoggedIn;

	}

	void OnHideUnity(bool isGameShown){
		if (!isGameShown) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
	}


	public void GetUsername(){
		
	
		FB.API("me?fields=name", HttpMethod.GET, UserCallBack);
		//usernameText.text = AccessToken.CurrentAccessToken.UserId ;


	}

	void UserCallBack(IGraphResult result) {
	
		if (result.Error != null)
		{                                                                      
			get_data = result.RawResult;
		}
		else
		{
			get_data = result.RawResult;
		}


		IDictionary dict = Facebook.MiniJSON.Json.Deserialize(get_data) as IDictionary;
		fbname = dict["name"].ToString();

	}

	void Update(){

		usernameText.text = fbname;

	}
}
