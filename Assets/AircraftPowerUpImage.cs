﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AircraftPowerUpImage : MonoBehaviour {
	public PlayerGameData playerdata;

	public Sprite[] airPowerupSprites;
	private Image airPowerImage;

	// Use this for initialization


	void Start () {
		airPowerImage = GetComponent<Image> ();

		switch (playerdata.StartingAirPower) {

		case PlayerPowerState.AircraftPower.Invulnerability:
			airPowerImage.sprite = airPowerupSprites [0];
			break;

		case PlayerPowerState.AircraftPower.SpeedUp:
			airPowerImage.sprite = airPowerupSprites [1];
			break;

		case PlayerPowerState.AircraftPower.Invisibility:
			airPowerImage.sprite = airPowerupSprites [2];
			break;

		case PlayerPowerState.AircraftPower.Shield:
			airPowerImage.sprite = airPowerupSprites [3];
			break;

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public  void SetCurAircraftImage(){

		switch (playerdata.StartingAirPower) {

		case PlayerPowerState.AircraftPower.Invulnerability:
			airPowerImage.sprite = airPowerupSprites [0];
			break;

		case PlayerPowerState.AircraftPower.SpeedUp:
			airPowerImage.sprite = airPowerupSprites [1];
			break;

		case PlayerPowerState.AircraftPower.Invisibility:
			airPowerImage.sprite = airPowerupSprites [2];
			break;

		case PlayerPowerState.AircraftPower.Shield:
			airPowerImage.sprite = airPowerupSprites [3];
			break;

		}

	}


}
