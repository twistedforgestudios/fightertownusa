﻿using UnityEngine;
using System.Collections;

public class MotherShipDeathAnim : MonoBehaviour {

	public ParticleGroup[] expParticles;
	public AudioSource[] expSounds;


	void PlayDeathAnimation(){
		foreach( ParticleGroup p in expParticles){
			p.Play ();
		}
	}
}
