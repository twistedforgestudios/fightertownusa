﻿using UnityEngine;
using System.Collections;

public class AnimatorSetter : MonoBehaviour {

	public Animator animator;

	public string triggerName;

	public string AnimName;

	public void SetTrigger(){
		animator.SetTrigger(triggerName);
	}


	public void PlayAnim(){
		animator.Play (AnimName);
	}



}
