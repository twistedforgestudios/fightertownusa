﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {


	public Explosion explosion;
	public float lifetime;
	public float detonateTime;
	public bool isDetonating;
	public bool isTraveling;
	public float detonateTimer;
	private SpriteRenderer sprite;
	private Rigidbody2D rgd;
	//public LayerMask whatTohit;
	//public LayerMask whatTohit2;
	private Collider2D collider2d;
	void OnEnable(){
		sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();


		collider2d = GetComponent<Collider2D> ();


		collider2d.enabled = true;

		sprite.enabled = true;
		if (isTraveling)
			ApplyVelocity (transform.right);


		detonateTimer = detonateTime;

		if (detonateTime > 0.0f) {
			isDetonating = true;
		} 

		Invoke ("Destroy", lifetime);
		Refresh ();


	}


	// Use this for initialization
	void Start () {
		detonateTimer = detonateTime;
		sprite = GetComponent<SpriteRenderer> ();
		rgd = GetComponent<Rigidbody2D> ();
		collider2d = GetComponent<Collider2D> ();


		collider2d.enabled = true;
		if (isTraveling)
			ApplyVelocity(transform.right);
		
		if (detonateTime > 0.0f) {
			isDetonating = true;
		} 

		Invoke ("Destroy", lifetime);
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isDetonating) {
			detonateTimer -= Time.deltaTime;
			//Debug.Log (detonateTimer.ToString());
			if (detonateTimer <= 0.0f) {
				isDetonating = false;

				Detonate ();

				if(explosion !=null)
				explosion.BeginExplosion ();

				//isDetonating = false;
				//Debug.Log ("StartExplosion");
			}

		}

	}

	void Dissappear(){
		if(sprite != null)
			sprite.enabled = false;
	}

	void Stop(){

		if(rgd != null)
			rgd.isKinematic = true;
	}

	void Detonate(){
		Dissappear ();
		Stop ();

	}

	void Refresh(){
		sprite.enabled = true;
		rgd.isKinematic = false;
	}

	void OnTriggerEnter2D(Collider2D other){

		//if (other.gameObject.layer == whatTohit || other.gameObject.layer == whatTohit2) {
		  Stop();

		//isDetonating = true;
		rgd.velocity = Vector2.zero;
		if (detonateTime <= 0.0f) {
			Dissappear ();
			explosion.BeginExplosion ();
		} else {
			isDetonating = true;
		}
		//}


		collider2d.enabled = false;
	//	Debug.Log (other.gameObject.name);
	}

	void OnCollisionEnter2D(Collision2D other){
		//if (other.gameObject.layer == whatTohit || other.gameObject.layer == whatTohit2) {
		Stop();

		//isDetonating = true;
		rgd.velocity = Vector2.zero;
		if (detonateTime <= 0.0f) {
			Dissappear ();
			explosion.BeginExplosion ();

		} else {
			isDetonating = true;
		}
		//}
		collider2d.enabled = false;
		//Debug.Log (other.gameObject.name);
	}

	void Destroy(){

		if(explosion != null)
			explosion.EndExplosion ();

		gameObject.SetActive (false);

	}

	void OnDisable(){
		CancelInvoke ();
		sprite.enabled = true;
	}


	public void ApplyVelocity(Vector3 direction)
	{
		rgd.velocity = (direction * 20.0f);
	}

}
