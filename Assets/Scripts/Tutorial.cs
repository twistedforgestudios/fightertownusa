﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

	public GM levelMananger;
	public RectTransform[] tips;
	public float TutorialAppearTime;
	public float TutorialAppearDelay;

	public bool tutorialStarted;
	public bool stoppedTutorial;
	private float tutorialTimer;
	private float delayTimer;
	private int currentTutorialTip = 0;

	// Use this for initialization
	void Start () {
		tutorialTimer = TutorialAppearTime;
		delayTimer = TutorialAppearDelay;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (!tutorialStarted && !stoppedTutorial) {

			tutorialTimer -= Time.deltaTime;

			if (tutorialTimer <= 0.0f) {
				tutorialStarted = true;
	
				if (currentTutorialTip >= 1) {
					tips[currentTutorialTip-1].gameObject.SetActive (false);
				}


				if (currentTutorialTip <= tips.Length-1) {

					tips [currentTutorialTip].gameObject.SetActive (true);
	
					currentTutorialTip++;
				}


				if (tips.Length+1 <= currentTutorialTip) {
					stoppedTutorial = true;
				}

			}
		}


		if (tutorialStarted && !stoppedTutorial) {

			delayTimer -= Time.deltaTime;

			if (delayTimer <= 0.0f) {
		     	tutorialStarted = false;
				delayTimer = TutorialAppearDelay;
			}
		}

	}
}
