﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AircraftPowerActivator : MonoBehaviour {
	
	public PlayerPowerState playerstate;
	public PlayerGameData playerdata;
	public Button button;

	public int powerUses = 1;


	void Start(){


		if (playerdata.StartingAirPower== PlayerPowerState.AircraftPower.Normal) {
			if (button != null)
				button.interactable = false;
		}

	}

	void Update(){
		

	}



	public void ActivateAircraftPower(){

		if (powerUses > 0) {

			powerUses--;

			if (button != null)
				button.interactable = false;
			
			switch (playerdata.StartingAirPower) {

			case PlayerPowerState.AircraftPower.Invulnerability:
				playerstate.Invulnerabiliy (30f);
				break;
		
			case PlayerPowerState.AircraftPower.SpeedUp:
				playerstate.Speedup (30f);
				break;

			case PlayerPowerState.AircraftPower.Invisibility:
				playerstate.Invisibility (30f);
				break;

			case PlayerPowerState.AircraftPower.Shield:
				playerstate.Sheild (30f);
				break;

			default:
				break;
			}

			playerdata.StartingAirPower = PlayerPowerState.AircraftPower.Normal;

		}
	}


	public  void EnablAircraftPowerBtn(){


	}


}
