﻿using UnityEngine;
using System.Collections;

public class WaveDeathEvent : MonoBehaviour {

	public Health[] enemies;
	public string eventName;
	public string message;
	//public ItemSpawner itemSpawner;


	bool eventTriggered;
	bool allEnemeisDead;
	// Use this for initialization
	void Start () {
		eventTriggered = false;

	}
	
	// Update is called once per frame
	void Update () {
		allEnemeisDead = true;


		foreach (Health h in enemies) {
			if (h.isAlive )
				allEnemeisDead = false;
		}


		if (allEnemeisDead && !eventTriggered) {
		//	Debug.Log("Event");
			if (eventName != "" && enabled) 
				EventManager.TriggerEvent (eventName);
			
				eventTriggered = true;
			  

			//if(itemSpawner !=null)
			//	itemSpawner.SpawnItem ();

			if (message != "") {
				this.SendMessage (message);
			}
			
		}


	}
	public void DisableWaveEvent(){
		this.enabled = false;
	}
}
