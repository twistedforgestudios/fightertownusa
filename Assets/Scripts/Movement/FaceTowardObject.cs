﻿using UnityEngine;
using System.Collections;

public class FaceTowardObject : MonoBehaviour {

	public float speed;
	public Transform target;

	public string tagName;

	public bool useTag;

	void OnEnable(){
		
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		float step = speed * Time.deltaTime;

		if (target != null && target.gameObject.activeInHierarchy) {
			Vector3 dir = target.position - transform.position;
			float angle = Mathf.Atan2 (dir.y, dir.x) * Mathf.Rad2Deg;
			Quaternion newRot = Quaternion.AngleAxis (angle, Vector3.forward);
			Quaternion newRot2 = Quaternion.Slerp (transform.rotation, newRot, step);
			transform.rotation = newRot2;
		}
	}


	public void AcquireTarget(Transform t ){
		target = t;
		//Debug.Log("Acauire target: " + t.name);
	}

	void OnTriggerEnter2D(Collider2D other){

		if (useTag) {
			if (other.tag == tagName) {
				if (target == null) {
					AcquireTarget (other.transform);
				}
			}
		} else {
			if (target == null) {
				AcquireTarget (other.transform);
			}
		}

	}

	void OnTriggerStay2D(Collider2D other){

		if (useTag) {
			if (other.tag == tagName) {
				if (target == null) {
					AcquireTarget (other.transform);
				}
			}
		} else {
			if (target == null) {
				AcquireTarget (other.transform);
			}
		}

	}

}
