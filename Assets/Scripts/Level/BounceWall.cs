﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets._2D;

public class BounceWall : MonoBehaviour {

	public float disableTime;
	public float BounceAngle;
	public Health health;
	public bool isGround = true;
	private bool controlsDisabled = false;
	private float disableTimer;
	private PlatformerCharacter2D pltfrmr;

	private Collider2D col;

	// Use this for initialization
	void Start () {
		disableTimer = disableTime;
		col = GetComponent<Collider2D> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (controlsDisabled) {

			disableTimer -= Time.deltaTime;


			if (disableTimer <= 0.0f) {

				disableTimer = disableTime;
				controlsDisabled = false;
				col.enabled = true;
				if (pltfrmr != null) {
					pltfrmr.bouncing = false;
					pltfrmr.GetComponent<Collider2D> ().enabled = true;
				}

			}
		}
	


	}


	void OnTriggerEnter2D(Collider2D othercol){

		//if (GM.hitground) {
			//GM.hitground = false;
		//if (!GM.intunnel && isGround) {
				
			Rigidbody2D rgd = othercol.GetComponent<Rigidbody2D> ();
			othercol.enabled = false;

		if (rgd != null)
			rgd.MoveRotation (BounceAngle);

			pltfrmr = othercol.GetComponent<PlatformerCharacter2D> ();
			pltfrmr.bouncing = true;
			controlsDisabled = true;
		//} else {

		//	if (!isGround) {
		//		Rigidbody2D rgd = col.GetComponent<Rigidbody2D> ();
		//		col.enabled = false;
				//rgd.MoveRotation (BounceAngle);
		//		rgd.rotation = BounceAngle;
		//		pltfrmr = col.GetComponent<PlatformerCharacter2D> ();
		//		pltfrmr.bouncing = true;
		//		controlsDisabled = true;
		//	}

		//}
		}
	//}

}
