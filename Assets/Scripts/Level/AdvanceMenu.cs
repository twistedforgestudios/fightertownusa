using UnityEngine;
using System.Collections;
public class AdvanceMenu : MonoBehaviour {

	static public bool show;		
	public GameObject displayText;
	Vector3 temp;

	void Start () {
    	temp = transform.localPosition; 
    	temp.y-=1000;
    	transform.localPosition=temp;
		show=false;
	}

	void Update () {
		if (CloudsEnd.show==true && show==false) {

			if (displayText != null) {
				displayText.SetActive (true);
			}

    		temp.y+=1000;
    		transform.localPosition=temp;
			show=true;
		}
    }
}

