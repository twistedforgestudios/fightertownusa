﻿using UnityEngine;
using System.Collections;

public class TimerUI : MonoBehaviour {

	public NumberUI secondsUI;
	public NumberUI minutesUI;


	public float TimeLimit;
	public bool timesUp;
	public string eventName;

	float timer;
	int seconds;
	int  minutes;
	// Use this for initialization
	void Start () {
		timer = TimeLimit;

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;

		EventManager.StartListening("LevelFinished", DisableTimer);
	}
	
	// Update is called once per frame
	void Update () {
	
		if(!timesUp){
			timer -= Time.deltaTime;

			if(timer <=0.0f){
				timesUp = true;

				if(eventName != "")
					EventManager.TriggerEvent(eventName);

			}
		}

		seconds = (int)timer % 60; 
		minutes = (int)timer / 60;

		secondsUI.number = seconds;
		minutesUI.number = minutes;



	}

	public float GetTimeLeft(){
		return timer;
	}

	void DisableTimer(){
		timesUp = true;
	}


	void OnEnable(){
		timesUp = false;

	}

	void OnDisable(){
		timesUp = true;
		timer = TimeLimit;
	}


}
