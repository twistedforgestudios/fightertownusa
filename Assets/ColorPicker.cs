﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorPicker : MonoBehaviour {

	public PlayerGameData playerData;


	public Scrollbar RedColorSlider;

	public Scrollbar GreenColorSlider;

	public Scrollbar BlueColorSlider;


	public Image coloredImage;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	

		coloredImage.color = new Color (RedColorSlider.value , GreenColorSlider.value, BlueColorSlider.value);
	
		playerData.UiColorRed = RedColorSlider.value;
		playerData.UiColorGreen = GreenColorSlider.value;
		playerData.UiColorBlue = BlueColorSlider.value;
	}





}
