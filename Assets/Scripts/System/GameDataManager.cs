﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameDataManager : MonoBehaviour {

	//bool isPaused = false;
	public Sprite[] Skins;
	public SystemTime systemTime;
	public CargoTimer cargoTime;
	public FuelCellTimer fuelCellTime;
	public static GameDataManager instance;
	//private GUIStyle guiStyle = new GUIStyle(); //create a new variable



	public PlayerGameData loadedPlayerData;


	public LevelGameData[] loadedLevelData;





	public string DebugString;


	void Awake()
	{
		if (instance == null) {
			DontDestroyOnLoad (gameObject);
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}
			
	}


	void Start(){
		systemTime = GetComponent<SystemTime> ();
	}


	public static T LoadScriptableObject<T>(string path) where T : ScriptableObject
	{
		return Resources.Load<T>(path);
	}


	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");

		//DebugString = "Data Saved";
		//PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");

		PlayerGameData playerData = loadedPlayerData;
			
		PlayerData pd = new PlayerData ();

		playerData.SavedTime = DateTime.Now;


		pd.NumOfFuelCells = playerData.NumOfFuelCells;
		pd.maxEnemiesKilled = playerData.maxEnemiesKilled;
		pd.HighScore  = playerData.HighScore;


		pd.CurrentSkin = playerData.CurrentSkin;
		pd.boughtFighterBlue = playerData.boughtFighterBlue;
		pd.boughtCamogreen = playerData.boughtCamogreen;
		pd.boughtArticLeopard = playerData.boughtArticLeopard;
		pd.boughtBlackTiger = playerData.boughtBlackTiger;
		pd.boughtDesertEagle = playerData.boughtDesertEagle;
		pd.boughtFlyingBeagle = playerData.boughtFlyingBeagle;



		pd.CargoBoxes = playerData.CargoBoxes;
		pd.UiColorRed = playerData.UiColorRed;
		pd.UiColorGreen = playerData.UiColorGreen;
		pd.UiColorBlue = playerData.UiColorBlue;


		pd.FullAutoPowerUps  = playerData.FullAutoPowerUps;
		pd.MultishotPowerUps = playerData.MultishotPowerUps;
		pd.LaserbeamPowerUps = playerData.LaserbeamPowerUps;
		pd.ExplodingBulletsPowerUps = playerData.ExplodingBulletsPowerUps;

		pd.InvulnerabilityPowerUps = playerData.InvulnerabilityPowerUps;
		pd.SpeedUpPowerUps = playerData.SpeedUpPowerUps;
		pd.InvisibilityPowerUps = playerData.InvisibilityPowerUps;
		pd.ShieldPowerUps = playerData.ShieldPowerUps;

		pd.SavedTime = playerData.SavedTime;
		//Debug.Log ("Time that was saved to  file is : " + pd.SavedTime.ToString ());

		pd.TimeUntilCargoReplenish = cargoTime.GetTimeLeft ();
		pd.TimeUntilFuelCellRepleinish = fuelCellTime.GetTimeLeft ();

		Debug.Log (" The saved file:" + fuelCellTime.GetTimeLeft ().ToString ());

		pd.shipUpgrades = playerData.shipUpgrades;
		pd.curArmorLevel = playerData.ArmorLevel;
		pd.curHeatLevel = playerData.HeatLevel;
		pd.curAmmoLevel = playerData.AmmoLevel;
		pd.curWingLevel = playerData.WingLevel;




		bf.Serialize (file, pd);
		file.Close ();
	}


	public void SaveLevel(int i)
	{
		BinaryFormatter bf = new BinaryFormatter ();
	//	Debug.Log (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat");
		FileStream file = File.Create(Application.persistentDataPath + "/Level" + i.ToString("D2") + "Info.dat");

		//Debug.Log (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat Saved");
	
		//LevelGameData levelData = LoadScriptableObject<LevelGameData> ("Level" + i.ToString("D2") + "/Level" + i.ToString("D2") + "Data");
		LevelGameData levelData = loadedLevelData[i-1];

		LevelData ld = new LevelData ();

		ld.AirEnemiesKilled = levelData.AirEnemiesKilled;
		ld.GroundEnemiesKilled = levelData.GroundEnemiesKilled;
		ld.PowerUpsCollected = levelData.PowerUpsCollected;
		ld.Completed = levelData.Completed;
		ld.Unlocked = levelData.Unlocked;
		ld.YellowDiamondsColleted = levelData.YellowDiamondsColleted;

		ld.AlliesSaved = levelData.AlliesSaved ;
		ld.SecondaryAmmoCollected = levelData.SecondaryAmmoCollected;
		ld.SecondaryKills = levelData.SecondaryKills;


		bf.Serialize (file, ld);
		file.Close ();



		if(i == 1)
		DebugString += Application.persistentDataPath.ToString ();
		
	}
		
	public void Load()
	{
	//	DebugString = Application.persistentDataPath.ToString ();
		//DebugString = "Data Loaded";
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat",FileMode.Open);
			PlayerData newplayerData = (PlayerData)bf.Deserialize (file);
			file.Close ();

			//PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");
			Debug.Log("The loaded File : " + newplayerData.TimeUntilFuelCellRepleinish.ToString());
			PlayerGameData playerData = loadedPlayerData;

			if (playerData != null) {
				//DebugString = playerData.ToString ();


				playerData.Initialize (newplayerData);
				//Debug.Log ("Time that was loaded from file is : " + newplayerData.SavedTime.ToString ());
				//Debug.Log ("Time is now                       : " + DateTime.Now.ToString ());

				cargoTime.TimeLimit = newplayerData.TimeUntilCargoReplenish;
				cargoTime.ReduceTime(GetTimeBetweenNowAndPastSave ());
				cargoTime.RestartTimer ();

		
				fuelCellTime.TimeLimit = newplayerData.TimeUntilFuelCellRepleinish;
				fuelCellTime.ReduceTime(GetTimeBetweenNowAndPastSave());
				fuelCellTime.RestartTimer ();

			

			} else {
				//DebugString = "Data Not Loaded";
			}
				
		}
		
	}






	public void LoadLevel( int i)
	{


	

		if (File.Exists (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat", FileMode.Open);
			LevelData newLevelData = (LevelData)bf.Deserialize (file);
			file.Close ();

			//LevelGameData levelData = LoadScriptableObject<LevelGameData> ("Level" + i.ToString ("D2") + "/Level" + i.ToString ("D2") + "Data");
		
		LevelGameData levelData = loadedLevelData [i - 1];


			//Debug.Log ("Loaded");
		
			if(i == 1)
				DebugString += newLevelData.Unlocked.ToString ();



			if (levelData != null) {
				DebugString = levelData.ToString ();

				levelData.Initialize (newLevelData);

			} else {
				DebugString = "Data Not Loaded";
			}




		
		} else {
			//create new file and intialize a a bunch of fuel cells 
			LevelGameData levelData =  loadedLevelData [i - 1];
			LevelData newLevelData = new LevelData ();

			levelData.Initialize (newLevelData);

		}

	}



	public void ErasePlayerData(){

	//	DebugString = "Data erased";
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");


		//PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");


		PlayerGameData playerData = loadedPlayerData;

	
		PlayerData pd = new PlayerData ();

		pd.NumOfFuelCells = 0;
		pd.maxEnemiesKilled = 0;

		pd.FullAutoPowerUps =  0;
		pd.MultishotPowerUps = 0;
		pd.LaserbeamPowerUps = 0;
		pd.ExplodingBulletsPowerUps = 0;

		pd.InvulnerabilityPowerUps = 0;
		pd.SpeedUpPowerUps = 0;
		pd.InvisibilityPowerUps = 0;
		pd.ShieldPowerUps = 0;

		pd.CargoBoxes = 0;

		pd.CurrentSkin = 0;


		pd.curArmorLevel = 1;
		pd.curAmmoLevel = 1;
		pd.curHeatLevel = 1;
		pd.curWingLevel = 1;


		if (playerData != null) {

			//DebugString = playerData.ToString ();
			playerData.ErasePlayerData ();

		} else {
			//DebugString += "Data Not Loaded";
		}


		bf.Serialize (file, pd);
		file.Close ();
	}


	public void EraseLevelData(int i){
	//	DebugString = "Data Erased";
		BinaryFormatter bf = new BinaryFormatter ();
		//	Debug.Log (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat");
		FileStream file = File.Create(Application.persistentDataPath + "/Level" + i.ToString("D2") + "Info.dat");

		//Debug.Log (Application.persistentDataPath + "/Level" + i.ToString ("D2") + "Info.dat Saved");

		//LevelGameData levelData = LoadScriptableObject<LevelGameData> ("Level" + i.ToString("D2") + "/Level" + i.ToString("D2") + "Data");

		LevelGameData levelData = loadedLevelData [i - 1];


	//	DebugString = levelData.ToString ();
		LevelData ld = new LevelData ();

		ld.AirEnemiesKilled = 0;
		ld.GroundEnemiesKilled = 0;
		ld.PowerUpsCollected = 0;
		ld.Completed = false;
		ld.Unlocked = false;
		ld.YellowDiamondsColleted = 0;

		ld.AlliesSaved =0 ;
		ld.SecondaryAmmoCollected = 0;
		ld.SecondaryKills = 0 ;


		levelData.AirEnemiesKilled = ld.AirEnemiesKilled;
		levelData.GroundEnemiesKilled = ld.GroundEnemiesKilled;
		levelData.PowerUpsCollected = ld.PowerUpsCollected;
		levelData.Completed = ld.Completed;
		levelData.Unlocked = ld.Unlocked;
		levelData.YellowDiamondsColleted = ld.YellowDiamondsColleted;

		levelData.AlliesSaved = ld.YellowDiamondsColleted;
		levelData.SecondaryAmmoCollected = ld.SecondaryAmmoCollected;
		levelData.SecondaryKills = ld.SecondaryKills;


		bf.Serialize (file, ld);
		file.Close ();

		//DebugString = "Data Erased";
	}

	void OnApplicationQuit() {
//		Debug.Log ("Saved");
		Save ();
		SaveLevel (1);
		SaveLevel (2);
			
		SaveLevel (3);
		SaveLevel (4);
		SaveLevel (5);
		SaveLevel (6);

		SaveLevel (7);
		SaveLevel (8);
		SaveLevel (9);
		SaveLevel (10);



	}

	//bool isPaused = false;



	void OnApplicationFocus( bool hasFocus )
	{
		//isPaused = !hasFocus;
	}

	void OnApplicationPause( bool pauseStatus )
	{
		//isPaused = pauseStatus;
	}



	void OnGUI( )
	{

		//guiStyle.fontSize = 40;
		//GUI.Label( new Rect( 300, 200, 5000, 3000 ), DebugString,guiStyle );
	






		#if UNITY_ANDROID
	//	guiStyle.fontSize = 40;
		//GUI.Label( new Rect( 300, 200, 5000, 3000 ), DebugString,guiStyle );

		//if (GUI.Button (new Rect (70, 480 - 124, 128, 64), "rate"))
		//	Application.OpenURL ("market://details?id=com.trollugames.caverun3d");
		//if( isPaused )
		//	guiStyle.fontSize = 40;
		  //  GUI.Label( new Rect( 100, 100, 5000, 3000 ), "Game paused",guiStyle );
		#endif

		#if UNITY_IOS
		//if (GUI.Button (new Rect (70, 480 - 124, 128, 64), "rate"))
		//	Application.OpenURL ("http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&pageNumber=0&sortOrdering=1&type=Purple+Software&mt=8");

		#endif
	}


	public void DisplayTimeBetweenNowAndPastSave(){
		//PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");

		//Debug.Log (( systemTime.now - playerData.SavedTime).Seconds.ToString ());

		//return (systemTime.now - playerData.SavedTime).Seconds;

	}


	public float GetTimeBetweenNowAndPastSave(){
		PlayerGameData playerData = LoadScriptableObject<PlayerGameData> ("PlayerData");

		//Debug.Log (DateTime.Now - playerData.SavedTime).Seconds.ToString ());



		Debug.Log ("Total Time Passed in seconds: " + ( DateTime.Now - playerData.SavedTime).TotalSeconds.ToString ());

		return Mathf.Clamp(Mathf.Abs((float)(DateTime.Now - playerData.SavedTime).TotalSeconds), 0, 3600);

	}






	public void GainCargoBox(){

	}	



	public void UnlockAllLevels(){


		foreach (LevelGameData leveldata in loadedLevelData) {

			leveldata.Unlocked = true;
			leveldata.Completed = true;
			leveldata.YellowDiamondsColleted = 3;
		}







	}




}
[Serializable]
public class PlayerData
{
	public float TimeUntilFuelCellRepleinish;
	public int NumOfFuelCells;


	public int HighScore;

	public int CurrentSkin;
	public bool boughtFighterBlue;
	public bool boughtCamogreen;
	public bool boughtArticLeopard;
	public bool boughtBlackTiger;
	public bool boughtDesertEagle;
	public bool boughtFlyingBeagle;

	public int maxEnemiesKilled;
	public int CargoBoxes;
	public float UiColorRed;
	public float UiColorGreen;
	public float UiColorBlue;
	public float TimeUntilCargoReplenish;
	public DateTime SavedTime;

	public PlayerPowerState.AircraftPower StartingAirPower;
	public PlayerPowerState.WeaponPower StartingWeaponPower;



	public int FullAutoPowerUps;
	public int MultishotPowerUps;
	public int LaserbeamPowerUps;
	public int ExplodingBulletsPowerUps;

	public int InvulnerabilityPowerUps;
	public int SpeedUpPowerUps;
	public int InvisibilityPowerUps;
	public int ShieldPowerUps;



	public int shipUpgrades;
	public int curArmorLevel;
	public int curHeatLevel;
	public int curAmmoLevel;
	public int curWingLevel;



}
[Serializable]
public class LevelData
{
	public string Name = "";
	public bool Completed = false;
	public bool Unlocked = false;
	public int GroundEnemiesKilled = 0;
	public int AirEnemiesKilled = 0;
	public int PowerUpsCollected = 0;
	public int AlliesSaved = 0;
	public int SecondaryAmmoCollected = 0;
	public int SecondaryKills = 0;
	public int YellowDiamondsColleted = 0;

}

[Serializable]
public class Achievement
{
	
}