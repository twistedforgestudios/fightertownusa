﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthLowEvent : MonoBehaviour {

	public Health health;
	public int valuetoTrigger;
	public string EventToTrigger;
	public bool activated;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (health.HealthPoints <= valuetoTrigger && !activated) {
			activated = true;
			EventManager.TriggerEvent (EventToTrigger);
		}
			
	}
}
