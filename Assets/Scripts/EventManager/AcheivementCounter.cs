﻿using UnityEngine;
using System.Collections;

public class AcheivementCounter : MonoBehaviour {


	public LevelGameData levelData;

	public GM manager;

	int enemyAir;
	int enemyground;
	int allysaved;
	int powerUpCollected;
	int secondaryAmmoCollected;
	int secondaryKill;
	int yellowDiamondsCollected;


	// Use this for initialization
	void Start () {
	
		EventManager.StartListening ("KilledAirEnemy", AddAirEnemyCounter);
		EventManager.StartListening ("KilledGroundEnemy", AddGroundEnemyCounter);
		EventManager.StartListening ("AllySaved", AddAllyProtected);
		EventManager.StartListening ("PowerUpCollected", AddPowerUpCounter);
		EventManager.StartListening ("AcquireSecondaryAmmo", AddSecondaryAmmoCounter);
		EventManager.StartListening ("SecondaryKill", AddSecondaryKill);
		EventManager.StartListening ("TallyScore", TallyAcheivements);
		EventManager.StartListening("AcquireYellowDiamond", AddYellowDiamond);
	}
	
	void AddYellowDiamond(){

		yellowDiamondsCollected++;
		yellowDiamondsCollected = Mathf.Clamp (yellowDiamondsCollected, 0, 3);
		Debug.Log(yellowDiamondsCollected.ToString() + " Diamonds Collected");
	}

	void AddAirEnemyCounter(){
		enemyAir++;
		//Debug.Log("Killed");
	}

	void AddGroundEnemyCounter(){
		enemyground++;
	}

	void AddPowerUpCounter(){
		powerUpCollected++;
	}


	void AddAllyProtected(){
		allysaved++;

	}

	void AddSecondaryAmmoCounter(){
		secondaryAmmoCollected++;
	}

	void AddSecondaryKill(){
		secondaryKill++;
		Debug.Log ("Secondary Kill");
	}

	void TallyAcheivements(){

		if (levelData.YellowDiamondsColleted <= yellowDiamondsCollected) {
			Debug.Log (yellowDiamondsCollected.ToString ());
			levelData.YellowDiamondsColleted = yellowDiamondsCollected;
		}
		//if(levelData.AirEnemiesKilled < enemyAir)
		levelData.AirEnemiesKilled = GM.enemyunits_Air;

		if(levelData.GroundEnemiesKilled < enemyground)
			levelData.GroundEnemiesKilled = enemyground;

		if(levelData.AlliesSaved < allysaved)
			levelData.AlliesSaved = allysaved;

		if(levelData.PowerUpsCollected < powerUpCollected)
			levelData.PowerUpsCollected = powerUpCollected;

		if(levelData.SecondaryAmmoCollected < secondaryAmmoCollected)
			levelData.SecondaryAmmoCollected = secondaryAmmoCollected;
	
		if(levelData.SecondaryKills < secondaryKill)
			levelData.SecondaryKills = secondaryKill;


		GameDataManager.instance.SaveLevel (manager.LevelId);
	}
}
