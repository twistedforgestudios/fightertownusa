﻿using UnityEngine;
using System.Collections;

public class DamageScreenUI : MonoBehaviour {

	public Animator damageScreenAnim;

	public void ScreenDamage(){
		damageScreenAnim.SetTrigger ("Damage");
	}
}
