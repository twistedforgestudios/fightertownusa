﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;

public class ProductUpdater : MonoBehaviour {

	//public PurchaserUI store;
	public Button FuelCellBtn;
	public Button FighterBlueBtn;
	public Button CamoGreenBtn;
	public Button ArticLeopardBtn;
	public Button BlackTigerBtn;
	public Button DesertEagleBtn;
	public Button FlyingBeagleBtn;

	public bool boughtFighterBlue;
	public bool boughtCamogreen;
	public bool boughtArticLeopard;
	public bool boughtBlackTiger;
	public bool boughtDesertEagle;
	public bool boughtFlyingBeagle;


	//IAPButton buttin;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


	public void checkAvailableSkins(){


		if (IAPButton.IAPButtonStoreManager.Instance.CheckPreviousPurchases (PurchaserUI.FighterBlueProductID)) {
			boughtFighterBlue = true;
			FighterBlueBtn.interactable = false;
		} else {
			boughtFighterBlue = false;
			FighterBlueBtn.interactable = true;
		}

		if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.CamoGreenProductID)) {
			boughtCamogreen = true;
			CamoGreenBtn.interactable = false;
		} else {
			boughtCamogreen = false;
			CamoGreenBtn.interactable = true;
		}

		if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.ArticLeopardProductID)) {
			boughtArticLeopard = true;
			ArticLeopardBtn.interactable = false;
		} else {
			boughtArticLeopard = false;
			ArticLeopardBtn.interactable = true;
		}

		if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.BlackTigerProductID)) {
			boughtBlackTiger = true;
			BlackTigerBtn.interactable = false;
		} else {
			boughtBlackTiger = false;
			BlackTigerBtn.interactable = true;
		}

		if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.DesertEagleProductID)) {
			boughtDesertEagle = true;
			DesertEagleBtn.interactable = false;
		} else {
			boughtDesertEagle = false;
			DesertEagleBtn.interactable = true;
		} 

		if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.FlyingBeagleProductID)) {
			boughtFlyingBeagle = true;
			FlyingBeagleBtn.interactable = false;

		} else {
			boughtFlyingBeagle = false;
			FlyingBeagleBtn.interactable = true;
		}


		//if (IAPButton.IAPButtonStoreManager.Instance .CheckPreviousPurchases (PurchaserUI.fuelCellConsumableProductID)) {
			
			//FuelCellBtn.interactable = false;

		//} else {
			
		//	FuelCellBtn.interactable = true;
		//}


	}



}
