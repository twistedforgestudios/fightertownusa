﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TimerTextUI : MonoBehaviour {


	public Text timeText;
	public TimedEvent timeEvent;

	int secs;
	int millisecs;
	// Use this for initialization
	void Start () {
	     
	}
	
	// Update is called once per frame
	void Update () {

		secs = (int)timeEvent.timeLeft;


		millisecs = (int)(timeEvent.timeLeft * 100) % 100;

		timeText.text = string.Format("{0:00}:{1:00}",secs,millisecs);

	}
}
