﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WeaponPowerUpStateUI : MonoBehaviour {


	public Sprite[] weaponSpritesUI;

	public Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
	
	}

	// Update is called once per frame
	void Update () {
		//Debug.Log(image);
	}


	public void SetWeaponUI( PlayerPowerState.WeaponPower power){

		switch (power) {

		case PlayerPowerState.WeaponPower.Normal:
			image.sprite = weaponSpritesUI [0];
			break;
		case PlayerPowerState.WeaponPower.Multishot:
			image.sprite = weaponSpritesUI [1];
			break;
		case PlayerPowerState.WeaponPower.FullAuto:
			image.sprite = weaponSpritesUI [2];
			Debug.Log(image);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			image.sprite = weaponSpritesUI [3];
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			image.sprite = weaponSpritesUI [4];
			break;
		default:
			break;
		}


	}
		





}
