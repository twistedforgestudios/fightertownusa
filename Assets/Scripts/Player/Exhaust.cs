using UnityEngine;
using System.Collections;

public class Exhaust : MonoBehaviour {

	static public bool roll;
	static public bool exhaust_end;
	static public bool exhaust_start;
	static public int presstime;
	public int type;

	public Animator m_Anim; 
    public Renderer rend;


	void Start () {
        rend = GetComponent<Renderer>();
	}

	void Update () {
		/*
		if (presstime==0) {
			if (Input.GetKey(KeyCode.B)) {
				exhaust_start=true;
				presstime=10;
			}	
			if (Input.GetKey(KeyCode.E)) {
				exhaust_end=true;
				presstime=10;
			}				   
		}
		*/
		if (exhaust_end==true) {
	    	m_Anim.SetTrigger("exhaust_end");
			exhaust_end=false;
		}

		if (exhaust_start==true) {
	    	m_Anim.SetTrigger("exhaust_start");
			exhaust_start=false;
		}

		if (presstime!=0)
			presstime--;

		if (type==1) {
			if (!PlaneEnd.show) {
				rend.enabled = false;
			}
			else
				rend.enabled = true;
		}

		if (roll) {
			m_Anim.SetTrigger("exhaust_roll");
			roll=false;
		}
    }
}


