﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

[RequireComponent (typeof(CircleCollider2D))]
public class PathNode : MonoBehaviour {

	public PathNode nextNode;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnTriggerEnter2D(Collider2D col){

		FollowTarget followScript = col.GetComponent<FollowTarget> ();


		if (followScript != null) {

			followScript.target = nextNode.transform;

		}
	}




}
