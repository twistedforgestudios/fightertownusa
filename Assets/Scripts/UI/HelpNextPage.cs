using UnityEngine;
using System.Collections;

public class HelpNextPage : MonoBehaviour {

    public Renderer rend;
	public bool pressed;
	public bool active;

	void OnMouseDown () {
		if (active) {
			Help.newi=Help.newisave[Help.page];
			Help.page++;
			Help.scrollpad=0;
			pressed=true;
		}
	}


	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		pressed=false;
		rend.enabled = true;
		active=true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Help.active==true) {

			rend.enabled = true;
			active=true;

			if (pressed) {
				if (Help.oldlength > Help.newisave[Help.page]) {
					rend.enabled = true;
					active=true;
				}
				else {
					rend.enabled = false;
					active=false;
				}
			}
		}
		else {
    		rend.enabled = false;
			active=false;

		}
	}
}
