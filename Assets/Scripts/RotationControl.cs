﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class RotationControl : MonoBehaviour {

	public float rotationMin;
	public float rotationMax;

	private float z = 0.0f;


	// Use this for initialization
	void Start () {
	
		z = transform.eulerAngles.z;
	}
	
	// Update is called once per frame
	void Update () {
	
		#if UNITY_STANDALONE_WIN

		if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.Rotate (Vector3.forward, 1f);
		}


		if (Input.GetKey (KeyCode.RightArrow)) {
			transform.Rotate (Vector3.forward, -1f);
		}

		#endif 	


		#if UNITY_ANDROID

		z += -CrossPlatformInputManager.GetAxis ("Horizontal");

		z = ClampAngle(z, -rotationMin,  rotationMax);


		Quaternion rotation =  Quaternion.Euler(0,0,z);


	

		transform.rotation = rotation;

	
		#endif 


	}

	static float ClampAngle ( float angle,float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}



}
