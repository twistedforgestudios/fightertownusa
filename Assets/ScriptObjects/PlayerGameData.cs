﻿using System;
using UnityEngine;
using System.Collections;


[CreateAssetMenu(fileName = "Data", menuName = "Game/PlayerData", order = 1)]
public class PlayerGameData : ScriptableObject
{

	//Fuel Cell System
	public float TimeUntilFuelCellReplenish;
	public int NumOfFuelCells;


	public int HighScore;
	public int maxEnemiesKilled;
	public int CargoBoxes;



	/// <summary>
	/// Skin Data
	/// </summary>
	public int CurrentSkin;
	public bool boughtFighterBlue;
	public bool boughtCamogreen;
	public bool boughtArticLeopard;
	public bool boughtBlackTiger;
	public bool boughtDesertEagle;
	public bool boughtFlyingBeagle;





	public PlayerPowerState.AircraftPower StartingAirPower;
	public PlayerPowerState.WeaponPower StartingWeaponPower;


	public DateTime SavedTime;

	public float UiColorRed;
	public float UiColorGreen;
	public float UiColorBlue;

	//weapon powerups
	public int FullAutoPowerUps;
	public int MultishotPowerUps;
	public int LaserbeamPowerUps;
	public int ExplodingBulletsPowerUps;

	//aircraft power ups
	public int InvulnerabilityPowerUps;
	public int SpeedUpPowerUps;
	public int InvisibilityPowerUps;
	public int ShieldPowerUps;

	//cargo box system
	public float TimeUntilCargoReplenish;
	public int CargoBoxUses;

	//Ship Upgrade System;
	public int shipUpgrades;
	public int curArmorLevel = 1;
	public int curHeatLevel = 1;
	public int curAmmoLevel = 1;
	public int curWingLevel = 1;

	private int maxLevel = 5;

	public int ArmorLevel {
		get { return Mathf.Clamp (curArmorLevel, 1, maxLevel); }
		set { curArmorLevel = value; }
	}

	public int HeatLevel {
		get { return Mathf.Clamp (curHeatLevel, 1, maxLevel); }
		set { curHeatLevel = value; }
	}

	public int AmmoLevel {
		get { return Mathf.Clamp (curAmmoLevel, 1, maxLevel); }
		set { curAmmoLevel = value; }
	}

	public int WingLevel {
		get { return Mathf.Clamp (curWingLevel, 1, maxLevel);}
		set { curWingLevel = value; }
	}



	public void Initialize(PlayerData pd){

		TimeUntilFuelCellReplenish = pd.TimeUntilFuelCellRepleinish;

		NumOfFuelCells = pd.NumOfFuelCells;

		HighScore = pd.HighScore;

		CurrentSkin = pd.CurrentSkin;
		boughtFighterBlue = pd.boughtFighterBlue;
		boughtCamogreen = pd.boughtCamogreen;
		boughtArticLeopard = pd.boughtArticLeopard;
		boughtBlackTiger = pd.boughtBlackTiger;
		boughtDesertEagle = pd.boughtDesertEagle;
		boughtFlyingBeagle = pd.boughtFlyingBeagle;


		maxEnemiesKilled = pd.maxEnemiesKilled;
		CargoBoxes = pd.CargoBoxes;

		SavedTime = pd.SavedTime;


		FullAutoPowerUps  = pd.FullAutoPowerUps;
		MultishotPowerUps = pd.MultishotPowerUps;
		LaserbeamPowerUps = pd.LaserbeamPowerUps;
		ExplodingBulletsPowerUps = pd.ExplodingBulletsPowerUps;

	    InvulnerabilityPowerUps = pd.InvulnerabilityPowerUps;
		SpeedUpPowerUps = pd.SpeedUpPowerUps;
		InvisibilityPowerUps = pd.InvisibilityPowerUps;
		ShieldPowerUps = pd.ShieldPowerUps;



		//Debug.Log ("Time that was read from file is : " + SavedTime.ToString ());

		UiColorRed = pd.UiColorRed;
	    UiColorGreen = pd.UiColorGreen;
		UiColorBlue = pd.UiColorBlue;

		//Debug.Log (TimeUntilCargoReplenish);
		TimeUntilCargoReplenish = pd.TimeUntilCargoReplenish;

		shipUpgrades = pd.shipUpgrades;
		ArmorLevel = pd.curArmorLevel;
		AmmoLevel = pd.curAmmoLevel;
		WingLevel = pd.curWingLevel;
		HeatLevel = pd.curHeatLevel;




	}



	public void ErasePlayerData(){

		NumOfFuelCells = 0;
		HighScore = 0;
		CurrentSkin = 0;
		maxEnemiesKilled = 0;
		CargoBoxes = 0;

	//	SavedTime = 0f;


		FullAutoPowerUps  = 0;
		MultishotPowerUps = 0;
		LaserbeamPowerUps = 0;
		ExplodingBulletsPowerUps = 0;

		InvulnerabilityPowerUps = 0;
		SpeedUpPowerUps = 0;
		InvisibilityPowerUps = 0;
		ShieldPowerUps = 0;


		ArmorLevel = 1;
		AmmoLevel = 1;
		HeatLevel = 1;
		WingLevel = 1;




	}


	public void DecrementFuelCell(){

		NumOfFuelCells--;

		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0,8);
	}

	public void IncrementFuelCell(){

		NumOfFuelCells++;

		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0, 8);
	}

	public void IncrementFuelCell( int cells){

		NumOfFuelCells += cells;
		Debug.Log ("Acquired " + cells.ToString() + " Cells");
		NumOfFuelCells = Mathf.Clamp (NumOfFuelCells, 0, 8);
	}


	public Color GetColor(){

		return new Color (UiColorRed, UiColorGreen, UiColorBlue);
	}


	public void GainCargoBox(){
		CargoBoxes++;
		CargoBoxes = Mathf.Clamp (CargoBoxes, 0, 20);

	}
}
