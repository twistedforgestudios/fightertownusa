﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PushPosition : MonoBehaviour {

	public Transform pusher;
	public Vector3 offset;
	//public Vector3 PushDirection;
	private Transform myTransform;
	public bool pushing;



	// Use this for initialization
	void Start () {
		myTransform = transform;
	}
	
	// Update is called once per frame
	void Update () {

		//Debug.Log ((myTransform.position.x - pusher.position.x));

		if ((myTransform.position.x - pusher.position.x) < offset.x) {
			myTransform.position = new Vector3 (pusher.position.x + offset.x, myTransform.position.y, myTransform.position.z + offset.z);
			pushing = true;
		} else {
			pushing = false;
		}


	}
}
