using UnityEngine;
using System.Collections;

public class HelpScroll : MonoBehaviour {

    public Renderer rend;
	public int type;

	public bool active;
    static public float offset;


	void OnMouseUp () {
		if (active) {
			if (type==0)
				Help.scrollpad+=.10f;
			else
				Help.scrollpad-=.10f;
		}
	}

    void OnMouseDrag () {
		if (active) {
			if (type==0)
				Help.scrollpad+=.05f;
			else
				Help.scrollpad-=.05f;
		}
    }


	// Use this for initialization
	void Start () {
		rend = GetComponent<Renderer>();
		active=true;

		offset=(Screen.height / 447f)/1f;

	}
	
	// Update is called once per frame
	void Update () {
		if (Help.active==true)
			rend.enabled = true;
		else
			rend.enabled = false;

		if (type==1) {
			if (Help.scrollpad < .05f)
				active=false;
			else
				active=true;
		}
		else {
			if (Help.scrollpad > 12.0f/offset)
				active=false;
			else
				active=true;
		}
	}
}
