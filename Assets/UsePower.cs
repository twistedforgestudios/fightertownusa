﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UsePower : MonoBehaviour {

	public CargoMenu cargoMenu;
	public PlayerGameData playerData;
	public Button buttonUsePower;
	public Text text;


	public RectTransform InfoWeaponWarningUI;
	public RectTransform InfoAirplaneWarningUI;

	// Use this for initialization
	void Start () {
		buttonUsePower.interactable= false;
		InfoWeaponWarningUI.gameObject.SetActive (false);

		InfoAirplaneWarningUI.gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void SetFullAuto(){


		if (playerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);




		if (playerData.FullAutoPowerUps > 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.FullAuto));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.FullAuto));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}

	}

	public void SetMultiShot(){



		if (playerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);




		if (playerData.MultishotPowerUps > 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.Multishot));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.Multishot));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}

	}

	public void SetLaserBeam(){



		if (playerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);



		if (playerData.LaserbeamPowerUps > 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.Laserbeam));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.Laserbeam));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetExplodingBullet(){




		if (playerData.StartingWeaponPower != PlayerPowerState.WeaponPower.Normal) {
			InfoWeaponWarningUI.gameObject.SetActive (true);
		} else {
			InfoWeaponWarningUI.gameObject.SetActive (false);
		}

		InfoAirplaneWarningUI.gameObject.SetActive (false);




		if (playerData.ExplodingBulletsPowerUps > 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveWeaponPowerUp (PlayerPowerState.WeaponPower.ExplodingBullets));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetWeaponPower (PlayerPowerState.WeaponPower.ExplodingBullets));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetInvulnerability(){



		if (playerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);



		if (playerData.InvulnerabilityPowerUps> 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Invulnerability));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Invulnerability));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetSpeedUp(){




		if (playerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);




		if (playerData.SpeedUpPowerUps> 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.SpeedUp));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.SpeedUp));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}

	public void SetInvisibility(){



		if (playerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);



		if (playerData.InvisibilityPowerUps> 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Invisibility));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Invisibility));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}


	public void SetShield(){



		if (playerData.StartingAirPower != PlayerPowerState.AircraftPower.Normal) {
			InfoAirplaneWarningUI.gameObject.SetActive (true);
		} else {
			InfoAirplaneWarningUI.gameObject.SetActive (false);
		}

		InfoWeaponWarningUI.gameObject.SetActive (false);


		if (playerData.ShieldPowerUps> 0) {
			buttonUsePower.interactable = true;
			buttonUsePower.onClick.RemoveAllListeners ();
			buttonUsePower.onClick.AddListener (() => cargoMenu.RemoveAircraftPowerUp (PlayerPowerState.AircraftPower.Shield));
			buttonUsePower.onClick.AddListener (() => cargoMenu.SetAircraftPower (PlayerPowerState.AircraftPower.Shield));
			text.text = "Use Power Up";
		} else {
			buttonUsePower.interactable = false;
			buttonUsePower.onClick.RemoveAllListeners ();
			text.text = "No Power Ups Left";
		}
	}




}
