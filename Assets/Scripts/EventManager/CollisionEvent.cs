﻿using UnityEngine;
using System.Collections;

public class CollisionEvent : MonoBehaviour {

	public string eventName;

	public LayerMask targetLayerMask;

	void OnTriggerEnter2D(Collider2D col){

		if ( targetLayerMask  == (targetLayerMask | (1 << col.gameObject.layer)) ) {
			EventManager.TriggerEvent (eventName);
			Debug.Log ("Collsion");
		}


	}
}

