using UnityEngine;
using System.Collections;

namespace UnityStandardAssets._2D
{
public class ComTrail : MonoBehaviour {


public static ParticleSystem ps;
	void Start () {
    	ps = GetComponent<ParticleSystem>();
		ps.GetComponent<Renderer>().sortingLayerName = "front";
	}

	void Update () {
		if (PlatformerCharacter2D.falling || GM.explodeflag) {
			ps.Stop();
		}
    }
}
}

