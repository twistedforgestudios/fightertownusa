using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets._2D;


public class GM : MonoBehaviour {

	public enum MissionType { TagnBagAir,TagnBagGround,TagnBag, AsBirdFlies, PaveTheWay, DemolitionMan, Seeker, Geronimo, GroundnPound, BossFight}
	public MissionType currentMission;
	public int NumOfGoalsRequired;
	public int numOfGoalsCompleted;
	public PlayerPowerState powerState;
	public bool RetryCostEnabled;
	public static int enemyunits_Air;
	public static int enemyunits_Terrain;
	public static int enemyunits_Infantry;
	public static int powerups_count;
	public static int strucutures_destroyed;
	public static int primary_weapon_current;
	public static int sec_weapon_current;
	public static int hiscore;
	public static int button_pressed;
	public static int button_over;
	public static int button_pressed_type;
	public static int totalkills;
	public RectTransform MenuDialog;
	public RectTransform EndDialog;

	public PlayerGameData playerData;
	public LevelGameData levelData;

	public LevelGameData nextLevel;

	static public int yellowDiamondsCollected;
	static public int alliesKilled;
	static public int alliesSaved;
	public static int secondary_weapon_uses;

	static public bool explodeflag = false;
	static public bool smokeflag = false;
	static public bool completed = false;

	static public Vector3 planepos;

	public AudioSource PlayerdeathSound;
	public int LevelId;

	public int numOfAirKillsNeeded;
	public int numOfGroundKillsNeeded;
	public int NumOfTotalKillsNeeded;
	public int numOfStructuresDestroyedNeeded;
	public int numOfParaTroopersNeeded;
	public int numOfAlliesKilledFail;
	public int numOfAlliesSavedWin;
	public int SecondaryAmmo;
	//public bool bossDestroyed;
	//public int AlliesLeft;

	int paratroopersDropped;

	static public int explodetype;

	static public bool Exiting;
	static public string scene;
	public int Exittimer;
	static public bool intunnel = false;
	static public bool hitground = false;


	private int skin=0;
	static public int skinsave=0;
	//public int numskins=2;

	//public bool[] WinConditions;

	public bool ObjectiveMet;

	public bool FinishOnlyOnTimeUp;


	// Use this for initialization
	void Awake () {
		enemyunits_Air=0;
		enemyunits_Terrain=0;
		enemyunits_Infantry=0;
		totalkills = 0;
		powerups_count=0;
		yellowDiamondsCollected = 0;
		sec_weapon_current=1;
		secondary_weapon_uses=SecondaryAmmo;
		alliesSaved = 0;
		alliesKilled = 0;
		strucutures_destroyed = 0;
		skin = playerData.CurrentSkin;
		skinsave=skin;

	}

	void Start () {


	



		Exiting = false;



	

		intunnel=false;
		hitground=false;

		//CloudsEnd.endlevel=false;
		//SceneManager.LoadScene ();
		PlaneEnd.show = false;
		EventManager.StartListening ("PlayerDeath", OpenRetryDialog);
		EventManager.StartListening ("PlayerCrash", PlayerDeath);

		EventManager.StartListening ("LevelFinished", OpenAdvanceDialog);
		EventManager.StartListening ("AcquireSecondaryAmmo", ReloadSecondaryWeapon);

		EventManager.StartListening("TimerUp", TimerUp);
		EventManager.StartListening("SubGoalCompleted", AddNumOfGoals);

		switch (currentMission) {
			
			case MissionType.TagnBag:
				EventManager.StartListening ("KilledAirEnemy", AddAirEnemyKilled);
				EventManager.StartListening ("KilledGroundEnemy", AddGroundEnemyKilled);


				break;
			case MissionType.TagnBagAir:
				EventManager.StartListening ("KilledAirEnemy", AddAirEnemyKilled);
				break;
			case MissionType.TagnBagGround:
				EventManager.StartListening ("KilledGroundEnemy", AddGroundEnemyKilled);
				break;
			case MissionType.AsBirdFlies:
				EventManager.StartListening ("GoalLineReached", GoalLineReached);
				break;
			case MissionType.PaveTheWay:
				EventManager.StartListening ("InfantryWayPaved", InfantryWayPaved);
				EventManager.StartListening ("AllyDestroyed", AllyDestroyed);
				break;
			case MissionType.DemolitionMan:
		        EventManager.StartListening ("StructureDestroyed", AddStructuresDestroyed);
				break;
			case MissionType.Seeker:
				EventManager.StartListening ("GoalLineReached", GoalLineReached);
				EventManager.StartListening ("TargetSeekerDestoyed", TargetSeekerDestroyed);
				break;
			case MissionType.Geronimo:
				EventManager.StartListening ("ParatrooperDeployed", AddParatroopersDropped);
				break;
			case MissionType.GroundnPound:
			    EventManager.StartListening("AllySaved", AllySaved);
				EventManager.StartListening ("GoalLineReached", GoalLineReached);
				break;
			case MissionType.BossFight:
				EventManager.StartListening ("BossDestroyed", BossDestroyed);
				break;
		}
			
		EventManager.StartListening ("PowerUpCollected", AddPowerUpsCollected);

		if (GameDataManager.instance != null)
			GameDataManager.instance.Save ();

		sec_weapon_current=0;
		primary_weapon_current=0;


		//SetPlayerInitialPowerUP ();

	}



	// Update is called once per frame
	void Update () {

		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		}

		if (Input.GetKeyDown (KeyCode.F)) {
			EventManager.TriggerEvent ("SubGoalCompleted");
		}

	}


	public void CompleteLevel(){
			EventManager.TriggerEvent ("SubGoalCompleted");
	}



	public void RestartLevel(){


		if (RetryCostEnabled) {
			if (playerData.NumOfFuelCells > 0) {
				playerData.DecrementFuelCell ();
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			} 
		} else {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}


	public void GotoScene(string s){
		GM.completed=false;
		Exiting=true;
		scene=s;
	}


	public void OpenRetryDialog()
	{

		EventManager.StopListening ("SubGoalCompleted", AddNumOfGoals);
	

		if(MenuDialog != null)
			MenuDialog.gameObject.SetActive (true);
	}

	public void CloseRetryDialog()
	{
		if(MenuDialog != null)
			MenuDialog.gameObject.SetActive (false);
		EventManager.StopListening ("LevelFinished", OpenAdvanceDialog);
	}

	public void OpenAdvanceDialog()
	{
		if (EndDialog != null)
			EndDialog.gameObject.SetActive (true);

		EventManager.StopListening ("PlayerDeath",OpenRetryDialog);
		EventManager.StopListening ("PlayerCrash", PlayerDeath);
	//	CrossinputManager.UnRegisterVirtualAxis(string name)
		levelData.Completed = true;
		completed=true;

		if (GetComponent<LevelUnlocker> () != null) {
			Debug.Log ("Unlock Next Level");
			this.SendMessage ("UnlockNextLevel");
		}

		//GameDataManager.instance.Save ();
		EventManager.TriggerEvent("TallyScore");
	}



	public void AddAirEnemyKilled(){
		enemyunits_Air++;
	
		if (enemyunits_Air >= numOfAirKillsNeeded) {
			EventManager.StopListening ("KilledAirEnemy",AddAirEnemyKilled);
			EventManager.TriggerEvent("SubGoalCompleted");
		}

		if (enemyunits_Air + enemyunits_Terrain >= NumOfTotalKillsNeeded && currentMission == MissionType.TagnBag ) {
			EventManager.TriggerEvent ("SubGoalCompleted");
			EventManager.StopListening ("KilledGroundEnemy",AddGroundEnemyKilled);
			EventManager.StopListening ("KilledAirEnemy",AddAirEnemyKilled);
		}

	}

	public void AddGroundEnemyKilled(){
		enemyunits_Terrain++;

		if (enemyunits_Terrain >= numOfGroundKillsNeeded ) {

			if (FinishOnlyOnTimeUp) {
				ObjectiveMet = true;
			} else {
				EventManager.TriggerEvent ("SubGoalCompleted");
				EventManager.StopListening ("KilledGroundEnemy",AddGroundEnemyKilled);
			}

		}

		if (enemyunits_Air + enemyunits_Terrain >= NumOfTotalKillsNeeded && currentMission == MissionType.TagnBag) {
			EventManager.TriggerEvent ("SubGoalCompleted");
			EventManager.StopListening ("KilledGroundEnemy",AddGroundEnemyKilled);
			EventManager.StopListening ("KilledAirEnemy",AddAirEnemyKilled);
		}
	}

	public void AddPowerUpsCollected(){
		powerups_count++;
	}


	public void AddParatroopersDropped(){
		paratroopersDropped++;
		if (paratroopersDropped >= numOfParaTroopersNeeded) 
			EventManager.TriggerEvent("SubGoalCompleted");
			//EventManager.TriggerEvent ("LevelFinished");
	}

	public void AddStructuresDestroyed(){
		strucutures_destroyed++;

		if (strucutures_destroyed >= numOfStructuresDestroyedNeeded) {
			EventManager.TriggerEvent ("SubGoalCompleted");
			EventManager.StopListening ("StructureDestroyed",AddStructuresDestroyed);
		}

	}

	public void GoalLineReached(){
		//EventManager.TriggerEvent ("LevelFinished");
		EventManager.TriggerEvent("SubGoalCompleted");
	}

	public void InfantryWayPaved(){
		EventManager.TriggerEvent("SubGoalCompleted");
	
	}

	public void TargetSeekerDestroyed(){
		EventManager.TriggerEvent ("PlayerDeath");
	}

	public void AllyDestroyed(){

		alliesKilled++;

		if(alliesKilled >= numOfAlliesKilledFail)
			EventManager.TriggerEvent ("PlayerDeath");
	}

	public void AllySaved(){

		alliesSaved++;

		if (alliesSaved >= numOfAlliesSavedWin) {
			EventManager.TriggerEvent ("SubGoalCompleted");
		
			EventManager.StopListening ("AllySaved", AllySaved);
		}
		

	}

	public void  TimerUp(){
				
		if(FinishOnlyOnTimeUp){
			//check win conditions is met
			if(ObjectiveMet)
				EventManager.TriggerEvent("SubGoalCompleted");
				else
				EventManager.TriggerEvent("PlayerDeath");
		}
		else{
			EventManager.TriggerEvent ("PlayerDeath");
		}
	}



	public void BossDestroyed(){
		EventManager.TriggerEvent("SubGoalCompleted");
	}

	public void ReloadSecondaryWeapon(){
		secondary_weapon_uses++;
	}

	public void Pause(){
		Time.timeScale = 0.0f;
	}

	public void Unpause(){
		Time.timeScale = 1.0f;
	}


	public void PlayerDeath(){
    	PlayerdeathSound.Play ();
		Debug.Log ("Playsound");
	}



	public void AddNumOfGoals(){
		numOfGoalsCompleted++;

		if(numOfGoalsCompleted >= NumOfGoalsRequired){
			EventManager.TriggerEvent("LevelFinished");
			EventManager.StopListening ("PlayerDeath", OpenRetryDialog);
			EventManager.StopListening ("PlayerCrash", PlayerDeath);
		}
	}

	public void SetPlayerInitialPowerUP(){

		PlayerPowerState.WeaponPower wp;
		PlayerPowerState.AircraftPower ap;

		wp = playerData.StartingWeaponPower;
		ap = playerData.StartingAirPower;

		switch (wp) {

		case PlayerPowerState.WeaponPower.Multishot:
			powerState.Multishot (30.0f);
			break;
		case PlayerPowerState.WeaponPower.FullAuto:
			powerState.FullAuto (30.0f);
			break;
		case PlayerPowerState.WeaponPower.Laserbeam:
			powerState.LaserBeam (30.0f);
			break;
		case PlayerPowerState.WeaponPower.ExplodingBullets:
			powerState.ExplodingBullets (30.0f);
			break;
		default:
			break;
		}

		switch (ap) {
		case PlayerPowerState.AircraftPower.SpeedUp:
			powerState.Speedup (30.0f);
			break;
		case PlayerPowerState.AircraftPower.Shield:
			powerState.Sheild (30.0f);
			break;
		case PlayerPowerState.AircraftPower.Invulnerability:
			powerState.Invulnerabiliy (30.0f);
			break;
		case PlayerPowerState.AircraftPower.Invisibility:
			powerState.Invisibility (30.0f);
			break;
		default:
			break;
		}
	}
}
