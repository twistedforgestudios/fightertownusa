﻿using UnityEngine;
using System.Collections;

public class AllyPlane : MonoBehaviour {

	public ParticleSystem explosion;

	public void OnAllyDestroy(){
		explosion.Play();
		EventManager.TriggerEvent ("AllyDestroyed");
	}
}
