﻿using UnityEngine;
using System.Collections;

public class DisableTimer : MonoBehaviour {

	public float lifeTime;

	bool isActive;

	public float lifeTimer;

	public bool permanent = false;

	void OnEnable(){
		isActive = true;
		lifeTimer = lifeTime;
	}


	// Use this for initialization
	void Start () {
		isActive = true;
		lifeTimer = lifeTime;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (isActive && !permanent) {

			lifeTimer -= Time.deltaTime;

			if (lifeTimer <= 0.0f) {
				Disable ();
				isActive = false;
			}
			
		}
	}


	public void Disable(){
		gameObject.SetActive (false);
	}
}
