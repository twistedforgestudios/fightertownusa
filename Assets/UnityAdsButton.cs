﻿
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using System.Collections;

[RequireComponent(typeof(Button))]
public class UnityAdsButton : MonoBehaviour
{
	public string EventAdFinished;
	public string EventAdFailed;
	public string EventAdSkipped;


	public string zoneId;
	private Button _button;
	void Start ()
	{
		_button = GetComponent<Button>();
		if (_button) _button.onClick.AddListener (delegate() { ShowAdPlacement(); });
	}
	void Update ()
	{
		if (_button) {
			if (string.IsNullOrEmpty (zoneId)) zoneId = null;
			_button.interactable = Advertisement.IsReady (zoneId);
		}
	}
	void ShowAdPlacement ()
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;
		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;




		Advertisement.Show (zoneId, options);
	}
	private void HandleShowResult (ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			
			if(EventAdFinished != "")
				EventManager.TriggerEvent (EventAdFinished);

			break;
		case ShowResult.Skipped:
			
			if(EventAdFailed != "")
				EventManager.TriggerEvent (EventAdFailed);

			break;
		case ShowResult.Failed:
			
			if(EventAdSkipped != "")
				EventManager.TriggerEvent (EventAdSkipped);

			break;
		}
	}

}