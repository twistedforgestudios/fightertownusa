using UnityEngine;
using System.Collections;

namespace UnityStandardAssets._2D
{
public class Shadow : MonoBehaviour {

    public Renderer rend;
	private Vector3 temp;
	public Color mycolor;
	public float scalevalue;
	public float scaleold;
	public float planey;
	public float planeyscale;
	public float planeyfirst;

	Vector3 scale;
	Quaternion rotation; 



	void Start () {
        rend = GetComponent<Renderer>();
		rend.enabled = true;
        rotation = transform.rotation; 
		transform.Rotate(-30, 0, 0);
        rotation = transform.rotation; 

        float speed = .51f;
		Color color = rend.material.color;
		color.a -= speed;
		rend.material.color = color;

		scaleold =transform.localScale.x;
		planeyfirst=0;
		planeyscale=0;
		planey=0;
	}

	void Update () {
		if (GM.Exiting || GM.intunnel) {
			rend.enabled = false;
		}
		else {
			rend.enabled = true;
			if (planeyfirst==0) {
				planey=GM.planepos.y;
				planeyfirst++;
			}
		//	planeyscale=((GM.planepos.y-planey)*.03f)+1;
			planeyscale=((GM.planepos.y-planey)*.05f)+1;
	  //  	scale=new Vector3( (scaleold*planeyscale*(1-scalevalue)),scaleold*planeyscale, 1f);
			scale=new Vector3( scaleold*planeyscale,scaleold*planeyscale, 1f);
			transform.localScale=scale;
		//	scalevalue=(Mathf.Abs(PlatformerCharacter2D.mv))*40;
			scalevalue=PlatformerCharacter2D.acRotationvalue;
			transform.rotation = rotation;
		    transform.Rotate(0, scalevalue, 0);
	    	temp = GM.planepos; // copy to an auxiliary variable...
	    	temp.y=-20f;
	    	transform.position=temp;
		}
    }
}
}


