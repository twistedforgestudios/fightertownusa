﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventMessagePersistent : MonoBehaviour {

	public string Message;
	public string EventToListen;
	public string EventToTrigger;


	// Use this for initialization
	void OnEnable() {
		SceneManager.sceneLoaded += OnSceneLoaded;

	}


	// Use this for initialization
	void OnDisable() {
		SceneManager.sceneLoaded -= OnSceneLoaded;

	}

	// Update is called once per frame
	void Update () {

	}



	void OnSceneLoaded(Scene scene, LoadSceneMode mode){
		if(EventToListen != "")
			EventManager.StartListening (EventToListen,TriggerMessage);
	}



	public void TriggerMessage(){

		if(Message != "")
			this.SendMessage (Message);

		if (EventToTrigger != "") 
			EventManager.TriggerEvent (EventToTrigger);


	}
}
