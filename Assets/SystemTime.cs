﻿using System;
using UnityEngine;
using System.Collections;

public class SystemTime : MonoBehaviour {

	public NumberUI secondsUI;
	public NumberUI minutesUI;

	public DateTime now;
	public float TimeLimit;
	public bool timesUp;
	public string eventName;

	float timer;
	int seconds;
	int  minutes;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

		now = DateTime.Now;



		seconds = now.Second;
		minutes = now.Minute;
	
		secondsUI.number = seconds;
		minutesUI.number = minutes;

	}

	public float GetTimeLeft(){
		return timer;
	}

	void DisableTimer(){
		timesUp = true;
	}


	void OnEnable(){
		timesUp = false;

	}

	void OnDisable(){
		timesUp = true;
		timer = TimeLimit;
	}


}
