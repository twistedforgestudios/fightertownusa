using UnityEngine;
using System.Collections;
public class EmpSprite : MonoBehaviour {


	static public bool show;		
	static public bool start;		
	public float count;		
    public Renderer rend;

	Vector3 scale;


	void Start () {
		show=false;
        rend = GetComponent<Renderer>();
		rend.enabled = false;
	}

	void Update () {
		if (start==true) {
			print("test");
			show=true;
			transform.position = GM.planepos;
			Vector3 temp = GM.planepos; // copy to an auxiliary variable...
			transform.position=temp;
			rend.enabled = true;
			count=0;
			start=false;
		}
		if (show) {
			if (count<60) {
				count++;
				Vector3 temp1 = transform.position; // copy to an auxiliary variable...
				temp1.x+=.8f;
				transform.position=temp1;

		    	scale=new Vector3( .05f*count,.05f*count, 1f );
		    	transform.localScale=scale;

			}
			else {
				show=false;
				rend.enabled = false;
			}
		}
    }
}

